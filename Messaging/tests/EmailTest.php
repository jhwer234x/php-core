<?php
use MerchPlatform\Messaging\Email;

class EmailTest extends PHPUnit_Framework_TestCase {

    private $email;

    /**
     * Initialize and setup the tests
     */
    public function setUp() {
        $this->email = new Email(); 
    }

    /**
     * testSendBuyerCampaignChargeFail
     * @return void
     */
    public function testSendBuyerCampaignChargeFail() {

        $campaignOrderId = 11532;
        $result = $this->email
            ->sendBuyerCampaignChargeFail($campaignOrderId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyersCampaignEndedFail
     * @return void
     */
    public function testSendBuyersCampaignEndedFail() {

        $campaignId = 67;
        $result = $this->email
            ->sendBuyersCampaignEndedFail($campaignId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyerCampaignEndedSuccess
     * @return void
     */
    public function testSendBuyerCampaignEndedSuccess() {

        $campaignOrderId = 11532;
        $result = $this->email
            ->sendBuyerCampaignEndedSuccess($campaignOrderId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyerCampaignMilestoneHit
     * @return void
     */
    public function testSendBuyerCampaignMilestoneHit() {

        $campaignOrderId = 11532;
        $result = $this->email
            ->sendBuyerCampaignMilestoneHit($campaignOrderId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyerCampaignNotGuaranteed
     * @return void
     */
    public function testSendBuyerCampaignNotGuaranteed() {

        $campaignOrderId = 11532;
        $result = $this->email
            ->sendBuyerCampaignNotGuaranteed($campaignOrderId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyerCampaignOrderReceipt
     * @return void
     */
    public function testSendBuyerCampaignOrderReceipt() {

        $campaignOrderId = 11532;
        $result = $this->email
            ->sendBuyerCampaignOrderReceipt($campaignOrderId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyersCampaignRelaunched
     * @return void
     */
    public function testSendBuyersCampaignRelaunched() {

        $campaignId = 67;
        $result = $this->email
            ->sendBuyersCampaignRelaunched($campaignId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendCampaignsToBulkOrders
     * @return void
     */
    public function testSendCampaignsToBulkOrders() {

        $campaignIds = array(1040, 1041, 1042);
        $result = $this->email
            ->sendCampaignsToBulkOrders($campaignIds);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendPrinterLabelsDoneGenerating
     * @return void
     */
    public function testSendPrinterLabelsDoneGenerating() {

        $jobId = 586;
        $result = $this->email
            ->sendPrinterLabelsDoneGenerating($jobId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerConfirmPayout
     * @return void
     */
    public function testSendSellerConfirmPayout() {

        $payoutId = 68;
        $result = $this->email
            ->sendSellerConfirmPayout($payoutId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerConfirmSignup
     * @return void
     */
    public function testSendSellerConfirmSignup() {

        $customerId = 32615;
        $result = $this->email
            ->sendSellerConfirmSignup($customerId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerCampaignConfirm
     * @return void
     */
    public function testSendSellerCampaignConfirm() {

        $campaignId = 1040;
        $result = $this->email
            ->sendSellerCampaignConfirm($campaignId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerCampaignEndedFail
     * @return void
     */
    public function testSendSellerCampaignEndedFail() {

        $campaignId = 1040;
        $result = $this->email
            ->sendSellerCampaignEndedFail($campaignId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerCampaignEndedSuccess
     * @return void
     */
    public function testSendSellerCampaignEndedSuccess() {

        $campaignId = 1040;
        $result = $this->email
            ->sendSellerCampaignEndedSuccess($campaignId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerCampaignEndingSoon
     * @return void
     */
    public function testSendSellerCampaignEndingSoon() {

        $campaignId = 67;
        $result = $this->email
            ->sendSellerCampaignEndingSoon($campaignId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendSellerResetPassword
     * @return void
     */
    public function testSendSellerResetPassword() {

        $customerId = 32615;
        $result = $this->email
            ->sendSellerResetPassword($customerId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendBuyerTrackerUrl
     * @return void
     */
    public function testSendBuyerTrackerUrl() {

        $customerId = 32615;
        $result = $this->email
            ->sendBuyerTrackerUrl($customerId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

    /**
     * testSendTrackingEmail
     * @return void
     */
    public function testSendBuyerTrackingEmail() {

        $packageId = 69584;
        $result = $this->email
            ->sendBuyerTrackingEmail($packageId);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

}
