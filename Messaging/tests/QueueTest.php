<?php
use MerchPlatform\Messaging\Queues;
use Predis\Client;
use MerchPlatform\Messaging\Queue;

class QueueTest extends PHPUnit_Framework_TestCase {

    private $queue;
    private $queueNames;

    /**
     * setUp unit test
     * @return void
     */
    public function setUp() {
        $this->queues = new Queues();
        $this->queuesNames = $this->queues->getList();
        $this->queue = new Queue();
    }

    /**
     * Test the add function
     * @return void
     */
    public function testAdd() {

        $message = array(
            'id' => 1,
            'type' => 'test'
        );

        $result = $this->queue->add($message, $this->queueNames);

        $this->assertObjectHasAttribute('data', $result);
        $this->assertInstanceOf('Guzzle\Service\Resource\Model', $result);
    }

}
