<?php
namespace MerchPlatform\Messaging;

use MerchPlatform\Messaging\Queue;
use MerchPlatform\Messaging\Queues;

/**
 * Common Email Functions
 */
class Messages
{

    private $queue;
    private $queues;
    private $queuesList;

    /**
     * Setup the Queues
     */
    public function __construct()
    {
        $this->queue = new Queue();
        $this->queues = new Queues();
        $this->queuesList = $this->queues->getList();
    }

    public function sendMarketingEmails($marketingEmailHistories)
    {
        $marketingEmailHistoriesBatches = array_chunk($marketingEmailHistories, 10); // SUPRISE. Amazon limits number of entities in request to 10. This is never mentioned in docs.
        foreach ($marketingEmailHistoriesBatches as $marketingEmailHistory) {
            $rq = array_map(function ($it) {
                return array(
                    'marketingEmailHistoryId' => $it['id'],
                    'customerId' => $it['customer_id']
                );
            }, $marketingEmailHistory);
            $this->queue->batchAddToQueue(
                $rq,
                $this->queues->getList()['marketing-email']
            );
        }
    }

    public function sendCampaignFulfillment($campaignId)
    {
        return $this->queue->add(
            array(
                'campaignId' => $campaignId
            ),
            $this->queuesList['shopify-fulfillment']
        );
    }

    public function sendCampaignItemFulfillment($campaignItemId)
    {
        return $this->queue->add(
            array(
                'campaignItemId' => $campaignItemId
            ),
            $this->queuesList['shopify-fulfillment']
        );
    }

    /**
     * Send job message to `fulfill_created_job` queue
     * @param  int  $jobId            A job id
     * @param  boolean $garmentsProvided - flag to specify
     * whether (true) or not (false) we provided garment ordering
     * for this job
     * @param  boolean $ignoreLabels     - flag to specify
     * whether (true) or not (false) albion should request labels
     * @return mixed    MerchPlatform/Messaging/Queue class instance
     */
    public function sendAlbionJob($jobId, $garmentsProvided = true, $ignoreLabels = true)
    {
        return $this->queue->add(
            array(
                'job' => $jobId,
                'garmentsProvided' => $garmentsProvided,
                'ignoreLabels' => $ignoreLabels
            ),
            $this->queuesList['created-jobs']
        );
    }

}
