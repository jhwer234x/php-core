<?php
namespace MerchPlatform\Messaging;

use MerchPlatform\Messaging\Queue;
use MerchPlatform\Messaging\Queues;
use Carbon\Carbon as Carbon;

/**
 * Common Email Functions
 */
class Email {

    private $queue;
    private $queues;

    /**
     * Setup the Queues
     */
    public function __construct() {
        $this->queue = new Queue();
        $queues = new Queues();
        $this->queues = $queues->getList();
    }

    /**
     * This email is sent to a campaign buyer when their card was
     * charged and the charge failed.
     * @param  int $campaignOrderId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendBuyerCampaignChargeFail($campaignOrderId) {

        return $this->queue->add(
            array(
                'campaignOrderId' => $campaignOrderId,
                'type' => 'buyerCampaignChargeFail'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to all campaign buyers when a campaign they bought from
     * unsuccessfully ends.
     * @param  int $campaignId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendBuyersCampaignEndedFail($campaignId) {

        return $this->queue->add(
            array(
                'campaignId' => $campaignId,
                'type' => 'buyersCampaignEndedFail'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to a campaign buyer when a campaign they bought from
     * successfully ends.
     * @param  int $campaignId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendBuyerCampaignEndedSuccess($campaignOrderId) {

        return $this->queue->add(
            array(
                'campaignOrderId' => $campaignOrderId,
                'type' => 'buyerCampaignEndedSuccess'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to a campaign buyer when a campaign they bought from
     * becomes successful, ONLY if they purchased before the campaign became
     * successful.
     * @param  int $campaignOrderId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendBuyerCampaignMilestoneHit($campaignOrderId) {

        return $this->queue->add(
            array(
                'campaignOrderId' => $campaignOrderId,
                'type' => 'buyerCampaignMilestoneHit'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to inform the customer that their order is dependent
     * upon the movmement hitting it's milestone.
     * @param  int  $packageId
     * @return Guzzle\Service\Resource\Model 
     */
    public function sendBuyerCampaignNotGuaranteed($campaignOrderId) {

        return $this->queue->add(
            array(
                'campaignOrderId' => $campaignOrderId,
                'type' => 'buyerCampaignNotGuaranteed'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to a campaign buyer upon ordering.
     * @param  int $campaignOrderId
     * @param  int $customerId
     */
    public function sendBuyerCampaignOrderReceipt($campaignOrderId) {

        return $this->queue->add(
            array(
                'campaignOrderId' => $campaignOrderId,
                'type' => 'buyerCampaignOrderReceipt'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to campaign buyers when a campaign they bought from
     * is relaunched, ONLY if the campaign previously failed to tip.
     * @param  int $campaignId
     */
    public function sendBuyersCampaignRelaunched($campaignId) {

        return $this->queue->add(
            array(
                'campaignId' => $campaignId,
                'type' => 'buyersCampaignRelaunched'
            ),
            $this->queues['buyer-campaign']
        );
    }

    /**
     * This email is sent to admin when the campaign to bulk orders
     * script runs.
     * @param  Array $campaignIds
     */
    public function sendCampaignsToBulkOrders($campaignIds) {

        return $this->queue->add(
            array(
                'campaignIds' => $campaignIds,
                'type' => 'campaignsToBulkOrders'
            ),
            $this->queues['admin']
        );
    }

    /**
     * This email is sent to admin and buyer when the bulk_orders launch
     * script runs.
     * @param  array $campaignOrderIds
     */
    public function sendConfirmationBulkOrdersForAdmin($campaignOrderIds) {

        return $this->queue->add(
            array(
                'campaignOrderIds' => $campaignOrderIds,
                'type' => 'confirmationBulkOrders'
            ),
            $this->queues['admin']
        );
    }

    /**
     * This email is sent to buyer when the bulk_orders launch
     * script runs.
     * @param  array $campaignOrderIds
     */
    public function sendConfirmationBulkOrdersForBuyer($campaignOrderIds) {

        return $this->queue->add(
            array(
                'campaignOrderIds' => $campaignOrderIds,
                'type' => 'sellerConfirmationBulkOrders'
            ),
            $this->queues['seller-campaign']
        );
    }

    /**
     * This email is sent to buyer when the bulk_orders launch
     * script runs.
     * @param  array $campaignOrderIds
     * @param string $error
     */
    public function sendErrorBulkOrdersForBuyer($campaignOrderIds, $error) {

        return $this->queue->add(
            array(
                'campaignOrderIds' => $campaignOrderIds,
                'type' => 'sellerErrorBulkOrders',
                'error' => $error
            ),
            $this->queues['seller-campaign']
        );
    }

    /**
     * This email is sent to buyer when the bulk_orders launch
     * script runs.
     * @param  array $campaignOrderIds
     * @param string $error
     */
    public function sendErrorBulkOrdersForAdmin($campaignOrderIds, $error) {

        return $this->queue->add(
            array(
                'campaignOrderIds' => $campaignOrderIds,
                'type' => 'errorBulkOrders',
                'error' => $error
            ),
            $this->queues['admin']
        );
    }

    /**
     * This email is sent to printers when labels are done generating.
     * @param  int $jobId
     */
    public function sendPrinterLabelsDoneGenerating($jobId) {

        return $this->queue->add(
            array(
                'jobId' => $jobId,
                'type' => 'printerLabelsDoneGenerating'
            ),
            $this->queues['printer']
        );
    }

    /**
     * This email is sent to a seller confirming their pending payout
     * was approved.
     * @param  int  $payoutId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendSellerConfirmPayout($payoutId) {

        return $this->queue->add(
            array(
                'payoutId' => $payoutId,
                'type' => 'sellerConfirmPayout'
            ),
            $this->queues['seller-payout']
        );
    }

    /**
     * This email is sent to a seller when they sign up.
     * @param  int  $customerId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendSellerConfirmSignup($customerId) {

        return $this->queue->add(
            array(
                'customerId' => $customerId,
                'type' => 'sellerConfirmSignup'
            ),
            $this->queues['seller-account']
        );
    }

    /**
     * This email is sent to a seller when they create a campaign.
     * @param  $campaignId
     * @return Guzzle\Service\Resource\Model 
     */
    public function sendSellerCampaignConfirm($campaignId) {

        return $this->queue->add(
            array(
                'campaignId' => $campaignId,
                'type' => 'sellerCampaignConfirm'
            ),
            $this->queues['seller-campaign']
        );
    }

    /**
     * This email is sent to a campaign seller when a campaign they ran
     * unsuccessfully ends.
     * @param  int $campaignId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendSellerCampaignEndedFail($campaignId) {

        return $this->queue->add(
            array(
                'campaignId' => $campaignId,
                'type' => 'sellerCampaignEndedFail'
            ),
            $this->queues['seller-campaign']
        );
    }

    /**
     * This email is sent to a campaign seller when a campaign they ran
     * successfully ends.
     * @param  int $campaignId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendSellerCampaignEndedSuccess($campaignId) {

        return $this->queue->add(
            array(
                'campaignId' => $campaignId,
                'type' => 'sellerCampaignEndedSuccess'
            ),
            $this->queues['seller-campaign']
        );
    }

    /**
     * This email is sent to a seller when their campaign is ending soon
     * and it hasn't reached the tipping point.
     * @param  int $campaignId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendSellerCampaignEndingSoon($campaignId) {

        return $this->queue->add(
            array(
                'campaignId' => $campaignId,
                'type' => 'sellerCampaignEndingSoon'
            ),
            $this->queues['seller-campaign']
        );
    }

    /**
     * This email is sent when a customer requests a password reset.
     * @param  int  $customerId
     * @return Guzzle\Service\Resource\Model 
     */
    public function sendSellerResetPassword($customerId) {

        return $this->queue->add(
            array(
                'customerId' => $customerId,
                'type' => 'sellerResetPassword'
            ),
            $this->queues['seller-password']
        );
    }

    /**
     * This email is sent to a buyer to show their tracking url.
     * @param  int  $customerId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendBuyerTrackerUrl($customerId) {

        return $this->queue->add(
            array(
                'customerId' => $customerId,
                'type' => 'buyerTrackerUrl'
            ),
            $this->queues['buyer-tracking-url']
        );
    }

    /**
     * This email is sent to a campaign buyer to track their order.
     * @param  int  $packageId
     * @return Guzzle\Service\Resource\Model
     */
    public function sendBuyerTrackingEmail($packageId) {

        return $this->queue->add(
            array(
                'packageId' => $packageId,
                'type' => 'buyerTrackingEmail'
            ),
            $this->queues['buyer-tracking']
        );
    }

    /**
     * sendExternalOrderBilling 
     * 
     * @param mixed $campaign 
     * @access public
     * @return Guzzle\Service\Resource\Model
     */
    public function sendExternalOrderBilling($bulkOrders, $totalCost) {
        $subject = "Charge for Production Orders";
        $sellerEmail = $bulkOrders->first()->customer->email;
        $sellerName = $bulkOrders->first()->customer->full_name;
        $price = money_format('%i', $totalCost);
        $items = array();
        foreach ($bulkOrders as $bulkOrder) {
            $items[] = array(
                'title' => 'Production Order ' . $bulkOrder->id,
                'amount' => $bulkOrder->price_external
            );
        }
        $options = array(
            'to' =>             array($sellerEmail, 'admin@example.com'),
            'subject' =>        $subject,
            'template' =>       'billing',
            'data' =>           array(
                'customerName' =>       $sellerName,
                'billingTitle' =>       $subject,
                'date' =>               Carbon::now()->toDateString(),
                'grandTotal' =>         $price . ' Paid',
                'total' =>              $price,
                'tax' =>                0,
                'items' =>              $items
            )
        );
        return $this->queue->add(
            $options,
            $this->queues['mail-billing']
        );
    }

    public function sendExternalOrderBillingFail($bulkOrders, $totalCost, $issue = 'none') {
        $subject = "Charge Failed for Production Orders";
        $sellerEmail = $bulkOrders->first()->customer->email;
        $sellerName = $bulkOrders->first()->customer->full_name;
        if ($issue == 'fail') {
            $message = 'A charge was attempted, but your payment method has been declined.';
        } else {
            $message = 'You must add a payment method to your account or increase your maximum auto-charge limit.';
        }
        return $this->sendAdminEmail(array(
            'to' =>             array($sellerEmail, 'admin@example.com'),
            'subject' =>        $subject,
            'template' =>       'alert',
            'data' =>           array(
                'title' =>          $subject,
                'bodyElements' =>   array($message),
                'alert' =>          $subject,
                'alertColor' =>     '#E21019',
                'link' =>           'https://www.example.com/production_orders',
                'linkCTA' =>        'Pay Now',
                'linkColor' =>      '#8BD169'
            )
        ));
    }

    public function sendProductMarketingToSellers($emails, $products, $subject, $body, $title, $logo) {
        $brandName = \MerchPlatform\Config\Main::get('brand')['name'];
        $path = \MerchPlatform\Config\Main::get('brand')['urls']['userUploads'] . '/';
        $emails[] = \MerchPlatform\Config\Main::get('brand')['contacts']['tech'];

        $options = array(
            'to' =>             $emails,
            'subject' =>        $subject,
            'template' =>       'product-marketing',
            'data' =>           array(
                'logo' =>               $logo,
                'path' =>               $path,
                'products' =>           $products,
                'brand' =>              $brandName,
                'body' =>               $body,
                'title' =>              $title,
                'date' =>               Carbon::now()->toDateString()
            )
        );
        return $this->queue->add(
            $options,
            $this->queues['mail-product-marketing']
        );
    }

    public function sendToReprint($bulkOrders, $paymentDetails, $logo) {
        $subject = "Order " . $bulkOrders->id . " Confirmed";
        $customerName = $bulkOrders->buyer->first_name . ' ' . $bulkOrders->buyer->last_name;
        $sellerEmail = $bulkOrders->seller->email;
        $sellerName = $bulkOrders->seller->first_name;
        $price = money_format('%i', $bulkOrders->buyer_cost);
        $items = array();
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $key = $protocol . \MerchPlatform\Config\Main::get('brand')['domain'] . '/tracker/order?key=' . $bulkOrders->url_key;
        $tax = 0;
        $shipping = 0;
        $subTotal = 0;
        foreach ($bulkOrders->campaignOrders as $bulkOrder) {
            foreach ($bulkOrder->campaignItems as $item){
                $items[] = array(
                    'title' => $item->campaign->title,
                    'image' => \MerchPlatform\Config\Main::get('brand')['urls']['userUploads'] . '/' . $item->previewFrontImage(),
                    'size' => $item->variant->size->name_long,
                    'amount' => money_format('%i', $item->price_items)
                );   
                $subTotal += $item->price_items;                 
                $tax += $bulkOrder->price_tax;
            }
            $shipping += $bulkOrder->price_shipping;
        }
        $tax = money_format('%i', $tax);
        $shipping = money_format('%i', $shipping);
        $subTotal = money_format('%i', $subTotal);
        $options = array(
            'to' =>             array($sellerEmail, \MerchPlatform\Config\Main::get('brand')['contacts']['tech']),
            'subject' =>        $subject,
            'template' =>       'reprint',
            'data' =>           array(
                'orderId' =>            $bulkOrders->id,
                'logo' =>               $logo,
                'sellerName' =>         $sellerName,
                'customerName' =>       $customerName,
                'key' =>                $key,
                'date' =>               Carbon::now()->toDateString(),
                'subTotal' =>           $subTotal,
                'total' =>              $price,
                'shipping' =>           $shipping,
                'tax' =>                $tax,
                'items' =>              $items,
                'shopName' =>           $paymentDetails->shopName,
                'paymentType' =>        $paymentDetails->paymentType,
                'paymentLogo' =>        $paymentDetails->paymentLogo,
                'shippingMethod' =>     $paymentDetails->shippingMethod,
                'street1' =>            $bulkOrders->address->street_1,
                'street2' =>            $bulkOrders->address->street_2,
                'locality' =>           $bulkOrders->address->locality,
                'province' =>           $bulkOrders->address->province,
                'postalCode' =>         $bulkOrders->address->postal_code,
                'country' =>            $bulkOrders->address->country->name
            )
        );
        return $this->queue->add(
            $options,
            $this->queues['mail-reprint']
        );
    }

    public function sendToRefund($bulkOrders, $refund, $paymentDetails, $logo) {
        $subject = "Order " . $bulkOrders->id . " Confirmed";
        $customerName = $bulkOrders->buyer->first_name . ' ' . $bulkOrders->buyer->last_name;
        $sellerEmail = $bulkOrders->seller->email;
        $sellerName = $bulkOrders->seller->first_name;
        $price = money_format('%i', $bulkOrders->buyer_cost);
        $items = array();
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $key = $protocol . \MerchPlatform\Config\Main::get('brand')['domain'] . '/tracker/order?key=' . $bulkOrders->url_key;
        $tax = 0;
        $shipping = 0;
        $subTotal = 0;
        foreach ($bulkOrders->campaignOrders as $bulkOrder) {
            foreach ($bulkOrder->campaignItems as $item){
                $items[] = array(
                    'title' => $item->campaign->title,
                    'image' => \MerchPlatform\Config\Main::get('brand')['urls']['userUploads'] . '/' . $item->previewFrontImage(),
                    'size' => $item->variant->size->name_long,
                    'amount' => money_format('%i', $item->price_items)
                );   
                $subTotal += $item->price_items;                 
                $tax += $bulkOrder->price_tax;
            }
            $shipping += $bulkOrder->price_shipping;
        }
        $tax = money_format('%i', $tax);
        $shipping = money_format('%i', $shipping);
        $subTotal = money_format('%i', $subTotal);
        $options = array(
            'to' =>             array($sellerEmail, \MerchPlatform\Config\Main::get('brand')['contacts']['tech']),
            'subject' =>        $subject,
            'template' =>       'refund',
            'data' =>           array(
                'orderId' =>            $bulkOrders->id,
                'logo' =>               $logo,
                'sellerName' =>         $sellerName,
                'customerName' =>       $customerName,
                'key' =>                $key,
                'date' =>               Carbon::now()->toDateString(),
                'subTotal' =>           $subTotal,
                'refund' =>             $refund,
                'total' =>              $price,
                'shipping' =>           $shipping,
                'tax' =>                $tax,
                'items' =>              $items,
                'shopName' =>           $paymentDetails->shopName,
                'paymentType' =>        $paymentDetails->paymentType,
                'paymentLogo' =>        $paymentDetails->paymentLogo,
                'shippingMethod' =>     $paymentDetails->shippingMethod,
                'street1' =>            $bulkOrders->address->street_1,
                'street2' =>            $bulkOrders->address->street_2,
                'locality' =>           $bulkOrders->address->locality,
                'province' =>           $bulkOrders->address->province,
                'postalCode' =>         $bulkOrders->address->postal_code,
                'country' =>            $bulkOrders->address->country->name
            )
        );
        return $this->queue->add(
            $options,
            $this->queues['mail-refund']
        );
    }

    public function sendToDenyRequest($bulkOrders, $paymentDetails, $logo) {
        $subject = "Order " . $bulkOrders->id . " Confirmed";
        $customerName = $bulkOrders->buyer->first_name . ' ' . $bulkOrders->buyer->last_name;
        $sellerEmail = $bulkOrders->seller->email;
        $sellerName = $bulkOrders->seller->first_name;
        $price = money_format('%i', $bulkOrders->buyer_cost);
        $items = array();
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $key = $protocol . \MerchPlatform\Config\Main::get('brand')['domain'] . '/tracker/order?key=' . $bulkOrders->url_key;
        $tax = 0;
        $shipping = 0;
        $subTotal = 0;
        foreach ($bulkOrders->campaignOrders as $bulkOrder) {
            foreach ($bulkOrder->campaignItems as $item){
                $items[] = array(
                    'title' => $item->campaign->title,
                    'image' => \MerchPlatform\Config\Main::get('brand')['urls']['userUploads'] . '/' . $item->previewFrontImage(),
                    'size' => $item->variant->size->name_long,
                    'amount' => money_format('%i', $item->price_items)
                );   
                $subTotal += $item->price_items;                 
                $tax += $bulkOrder->price_tax;
            }
            $shipping += $bulkOrder->price_shipping;
        }
        $tax = money_format('%i', $tax);
        $shipping = money_format('%i', $shipping);
        $subTotal = money_format('%i', $subTotal);
        $options = array(
            'to' =>             array($sellerEmail, \MerchPlatform\Config\Main::get('brand')['contacts']['tech']),
            'subject' =>        $subject,
            'template' =>       'deny-request',
            'data' =>           array(
                'orderId' =>            $bulkOrders->id,
                'logo' =>               $logo,
                'sellerName' =>         $sellerName,
                'customerName' =>       $customerName,
                'key' =>                $key,
                'date' =>               Carbon::now()->toDateString(),
                'subTotal' =>           $subTotal,
                'total' =>              $price,
                'shipping' =>           $shipping,
                'tax' =>                $tax,
                'items' =>              $items,
                'shopName' =>           $paymentDetails->shopName,
                'paymentType' =>        $paymentDetails->paymentType,
                'paymentLogo' =>        $paymentDetails->paymentLogo,
                'shippingMethod' =>     $paymentDetails->shippingMethod,
                'street1' =>            $bulkOrders->address->street_1,
                'street2' =>            $bulkOrders->address->street_2,
                'locality' =>           $bulkOrders->address->locality,
                'province' =>           $bulkOrders->address->province,
                'postalCode' =>         $bulkOrders->address->postal_code,
                'country' =>            $bulkOrders->address->country->name
            )
        );
        return $this->queue->add(
            $options,
            $this->queues['mail-deny-request']
        );
    }

    /**
     * This is a catch-all method for sending arbitrary message JSON
     * @param  Array $options
     * @return Guzzle\Service\Resource\Model
     */
    public function sendAdminEmail($options) {

        return $this->queue->add(
            $options,
            $this->queues['admin']
        );
    }
}
