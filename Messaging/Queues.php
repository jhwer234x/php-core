<?php
namespace MerchPlatform\Messaging;

class Queues {

    private $list;

    public function __construct() {
        $this->configureQueues();
    }

    /**
     * Configure the queues names
     * @return void
     */
    public function configureQueues() {
        $this->list = [
            'admin' => 'mail',
            'mail-billing' => 'mail_billing',
            'buyer-campaign' => 'mail_buyer_order_receipt',
            'mass-launch' => 'mass_launch',
            'printer' => 'mail_printer_label_completed',
            'seller-account' => 'mail_signup_confirm',
            'seller-campaign' => 'mail_seller_confirm_campaign',
            'buyer-tracking' => 'mail_buyer_order_tracking',
            'shopify-fulfillment' => 'fulfill_shopify_fulfillment',
            'marketing-email' => 'mail_seller_marketing',
            'created-jobs' => 'fulfill_created_job',
            'seller-password' => 'mail_password_reset',
            'buyer-tracking-url' => 'mail_buyer_tracking_url',
            'seller-payout' => 'mail_seller_confirm_payout',
            'mail-reprint' => 'mail_reprint',
            'mail-refund' => 'mail_refund',
            'mail-deny-request' => 'mail_deny_request',
            'mail-product-marketing' => 'mail_product_marketing'
        ];

    }

    /**
     * Expose the queue list
     * @return Array
     */
    public function getList() {
        return $this->list;
    }
}
