<?php
namespace MerchPlatform\Postal;

use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Formatter\LineFormatter;
use MerchPlatform\Entities\PackageBatch;
use MerchPlatform\Entities\SupplierOrderItem;
use MerchPlatform\Entities\Setting;
use MerchPlatform\Entities\Country;
use MerchPlatform\Entities\Package;
use League\Flysystem\Exception;

/**
 * Purchase labels for shipping packages, get tracking status updates, and more
 */
class Mailman
{
    protected $logger;
    /**
     * Construct the class, specify environment
     * @param string $environment [description]
     */
    public function __construct($environment = 'development')
    {
        $this->logger = new Logger('controller');
        $handler = new ErrorLogHandler();
        $handler->setFormatter(new LineFormatter(null, null, true));
        $this->logger->pushHandler($handler);
        $key = \MerchPlatform\Config\Main::get('easypost')['key'];
        \EasyPost\EasyPost::setApiKey($key);
    }

    /**
     * When an admin cancels a/n order(s), we need to refund labels
     * @param string $campaign_order_id
     * @return string result of refund request: refund status of order
     */
    public function refundLabels($campaign_order_id)  {
        //get shipment id
        $easy_post_shipment_ids = Package::whereCampaignOrderId($campaign_order_id)->whereNotNull('thirdparty_id')->lists('thirdparty_id');
        try {
            //easypost get shipment
            foreach ($easy_post_shipment_ids as $easy_post_shipment_id) {
                $shipment = \EasyPost\Shipment::retrieve($easy_post_shipment_id);
            }
            //esasypost refund
            $shipment->refund();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $shipment->refund_status;
    }

    /**
     * Purchase labels for one or more packages
     * @param  object $packages    Eloquent collection of Package models
     * @param  object $fromAddress Eloquent Address model for From address
     * @param  object $toAddress   Eloquent Address model for To address
     * @param  object $returnAddress Eloquent Address model for Return address
     * @return mixed The collection of packages if successful, else false
     */
    public function purchaseLabels($packages, $fromAddress, $toAddress, $returnAddress, $force = false)
    {
        $costCenter = Setting::where('key', 'cost_center')->pluck('value');
        $epFromAddress = $this->createAddress($fromAddress, $useCarrierFacility = true);
        $epReturnAddress = $this->createAddress($returnAddress, $useCarrierFacility = false);
        foreach ($packages as $package) {
            $epParcel = $this->createParcel($package);
            $epToAddress = $this->createAddress($toAddress, $useCarrierFacility = false);
            $options = array(
                'cost_center' => $costCenter,
                'print_custom_1' => $package->label_line_1,
                'print_custom_2' => $package->label_line_2,
                'print_custom_3' => $package->label_line_3
            );
            if ($force === 'true') {
                $options['address_validation_level'] = 0;
            }
            list($international, $epShipment) = $this->generateLabelInfo($epToAddress, $package, $epFromAddress, $epReturnAddress, $epParcel, $options);
            $package->address_id = $toAddress->id;
            try {
                $this->buyLabel($epShipment, $international);
                $package->carrier = $epShipment->selected_rate->carrier;
                $package->thirdparty_id = $epShipment->id;
                $package->cost = $epShipment->selected_rate->rate;
                $package->label_url = $epShipment->postage_label->label_url;
                $package->tracking_num = $epShipment->tracking_code;
                $package->status = 'paid';
            } catch (\Exception $e) {
                $this->logError($e);
            }
            $package->save();
        }
        return $packages;
    }

    /**
     *
     * Create an EasyPost Domestic Address object from a address object
     * @param  object $address Eloquent Address model
     * @param boolean $useCarrierFacility sent  if address is a fromAddress
     * @return mixed EasyPost Address object if succeeded, else false
     */
    public function createAddress($address, $useCarrierFacility)
    {
        $phoneNum = $address->phone_num;
        if (is_null($address->phone_num) || (empty($address->phone_num))) {
            $phoneNum = '773-688-4485';
        }
        $addressToBeCreated =[
            'name' => $address->full_name,
            'company' => $address->company,
            'street1' => $address->street_1,
            'street2' => $address->street_2,
            'city' => $address->locality,
            'state' => $address->province,
            'zip' => $address->postal_code,
            'phone' => $phoneNum,
            'country' => $address->country->code
        ];
        if ($useCarrierFacility) {
            $carrierFacility = Setting::where('key', 'carrier_facility')->pluck('value');
            $addressToBeCreated['carrier_facility'] = $carrierFacility;
        }
        try {
            $epAddress = \EasyPost\Address::create($addressToBeCreated);
            return $epAddress;
        } catch (\Exception $e) {
            $this->logError($e);
            return false;
        }
    }

    /**
     * Create and get an EasyPost Parcel object from a package object
     * @param  object $package Eloquent Package model
     * @return mixed EasyPost Parcel object if succeeded, else false
     */
    public function createParcel($package)
    {
        try {
            $epParcel = \EasyPost\Parcel::create(array(
                'length' => $package->length,
                'width' => $package->width,
                'height' => $package->height,
                'weight' => $package->weight_oz
            ));
            return $epParcel;
        } catch (\Exception $e) {
            $this->logError($e);
            return false;
        }
    }

    /**
     * Get the tracking status of a package
     * @param  object $package Eloquent Package model
     * @return string The tracking status
     */
    public function getTrackingStatus($package)
    {
        $shipment = \EasyPost\Shipment::retrieve($package->thirdparty_id);
        $tracker = \EasyPost\Tracker::retrieve($shipment->tracker->id);
        return $tracker->status;
    }

    private function logError($e)
    {
        $msg = 'Exception details: ' . $e->getMessage() . "\n";
        $msg .= 'File: ' . $e->getFile() . ' (' . $e->getLine() . ')' . "\n";
        $msg .= $e->getTraceAsString();
        $this->logger->addInfo($msg);
    }

    /**
     * based on the provice of the package destination address, generates address and customs item objects for labels
     * @param $epToAddress
     * @param $package
     * @param $epFromAddress
     * @param $epReturnAddress
     * @param $epParcel
     * @param $options
     * @return array, boolean
     */
    public function generateLabelInfo($epToAddress, $package, $epFromAddress, $epReturnAddress, $epParcel, $options)
    {
        $international = $this->isInternational($epToAddress);
        list($customsInfo, $jobId) = $this->createCustomsItem($package);
        $options['print_custom_3'] = $options['print_custom_3'] . ' job Id: ' . $jobId;
        $epShipment = $this->createShipment(
            $international,
            $epFromAddress,
            $epToAddress,
            $epReturnAddress,
            $epParcel,
            $customsInfo,
            $options
        );
        return array($international, $epShipment);
    }

    /**
     * Returns true if the To address for a package is NOT a US state
     * @param object $address to-address for the package
     * @return boolean true if to-address for the package is international address
     */
    public function isInternational($address)
    {
        $countries = Country::where('is_domestic', 1)->lists('code');
        foreach ($countries as $country) {
            if ($address->country == $country) {
                return false;
            }
        }
        return true;
    }

    /**
     * Create an EasyPost Custom info object containing an EasyPost Customs Item object for each unique item in package. See Documentation for more information https://www.easypost.com/customs-guide?lang=php#code-language-select
     * @param  object $package Eloquent Package model
     * @return  mixed EasyPost Customs Items objects if succeeded, else false
     */
    public function createCustomsItem($package)
    {
        $supplierOrderItems = SupplierOrderItem::where('package_id', '=', $package->id)
            ->with('variant', 'variant.product', 'campaignItem', 'variant.size', 'variant.color')->get();
        $groupedSupplierOrderItems = $supplierOrderItems->groupBy('variant_id');
        foreach ($groupedSupplierOrderItems as $groupedSupplierOrderItem) {
            $campaign_item_order_cost = $groupedSupplierOrderItem[0]->campaignItem;
            $jobId = $groupedSupplierOrderItem[0]->job_id;
            $count = count($groupedSupplierOrderItem);
            $variant = $groupedSupplierOrderItem[0]->variant;
            $weight = ((float)$variant->weight * 16) * $count;
            if ($weight < 1) {
                $weight = 1;
            }
            $cost = ($campaign_item_order_cost->price_items / $campaign_item_order_cost->qty) * $count;
            try {
                $customsItem = \EasyPost\CustomsItem::create(array(
                    "description" => $variant->size->name . ' ' . $variant->color->name . ' ' . $variant->product->name,
                    "quantity" => $count,
                    "hs_tariff_number" => $variant->product->hs_tariff,
                    "weight" => $weight,
                    "value" => $cost,
                    "origin_country" => 'US'
                ));
                $customsItems[] = $customsItem;
            } catch (\Exception $e) {
                $this->logError($e);
                return false;
            }
        }
        $customsInfo = $this->createCustomsInfo($customsItems);
        return array($customsInfo, $jobId);
    }

    /**
     * Create an EasyPost Customs Info object using custom Items array and customs information
     * @param  object $customsItems Easy Post Custom Items array
     * @return mixed EasyPost Parcel object if succeeded, else false
     */
    public function createCustomsInfo($customsItems) {
        $customsSigner = Setting::where('key', 'customs_signer')->pluck('value');
        try {
            $customsInfo = \EasyPost\CustomsInfo::create(array(
                "eel_pfc" => 'NOEEI 30.37(a)',
                "customs_certify" => true,
                "customs_signer" => $customsSigner,
                "contents_type" => 'gift',
                "contents_explanation" => '',
                "restriction_type" => 'none',
                "restriction_comments" => '',
                "non_delivery_option" => 'return',
                "customs_items" => $customsItems
            ));
            return $customsInfo;
        } catch (\Exception $e) {
            $this->logError($e);
            return false;
        }
    }

    /**
     * Create and get an EasyPost Shipment object
     * @param  object $epFromAddress EasyPost Address object for From address
     * @param  object $epReturnAddress EasyPost Address object for From address
     * @param  object $epToAddress EasyPost Address object for To address
     * @param  object $epParcel EasyPost Parcel object
     * @param  array  $options Options for EasyPost Shipment request
     * @param boolean $international true if shipment NOT going to a US state
     * @param $customsInfo object required for international shipping
     * @return mixed EasyPost Shipment object if success, else false
     */
    public function createShipment($international, $epFromAddress, $epToAddress, $epReturnAddress, $epParcel, $customsInfo, $options = array())
    {
        $shipment = [
            'to_address' => $epToAddress,
            'from_address' => $epFromAddress,
            'return_address' => $epReturnAddress,
            'parcel' => $epParcel,
            'reference' => \MerchPlatform\Config\Main::get('easypost')['reference'],
            'options' => $options,
            'customs_info' => $customsInfo
        ];
        try {
            $epShipment = \EasyPost\Shipment::create($shipment);
            return $epShipment;
        } catch (\Exception $e) {
            $this->logError($e);
            return false;
        }
    }
    /**
     * Buy a label for an EasyPost Shipment
     * @param  object $shipment EasyPost Shipment Object
     * @param  boolean $international true if shipment not going to US state
     * @return object EasyPost Shipment object
     */
    public function buyLabel($shipment, $international)
    {
        $carrierOptions = $this->getCarrierOptions($shipment, $international);
        $rate = $this->getSpecificRate($shipment, $carrierOptions);
        try {
            $shipment->buy($rate);
            return $shipment;
        } catch (\Exception $e) {
            $this->logError($e);
            return false;
        }
    }

    /**
     * Get the carrier and service to use for a Shipment
     * @param  object $shipment EasyPost Shipment object
     * @param  boolean $international true if shipment not going to US state
     * @return array Array specifying the carrier and service to use
     */
    private function getCarrierOptions($shipment, $international)
    {
        $carrier = 'Asendia';
        $service = 'IPA';
        if ($international) {
            if ($shipment->parcel->weight >= 70.4) {
                $service = 'PMI';
            }
        }
        if (!$international) {
            $carrier = 'USPS';
            $service = 'First';
            if ($shipment->parcel->weight >= 13) {
                $service = 'Priority';
            }
        }
        return compact('carrier', 'service');
    }
    /**
     * Get a specific rate (carrier and service) for a shipment
     * @param  object $shipment     EasyPost Shipment object
     * @param  array $carrierOptions Array specifying service and carrier
     * @return mixed EasyPost Rate object if success, else false
     */
    public function getSpecificRate($shipment, $carrierOptions)
    {
        foreach($shipment->rates as $rate) {
            if ($rate->service == $carrierOptions['service'] &&
                $rate->carrier == $carrierOptions['carrier']) {
                return $rate;
            }
        }
        return false;
    }

    public function collectDailyShipments($jobIds, $contractorId)
    {
        $shipmentIdsByCarrier = [];
        // Get all the packages that are being shipped out with the given jobs
        $supplierOrderItems = SupplierOrderItem::whereIn('job_id', $jobIds)
            ->whereStatus('arrived')
            ->groupBy('package_id')
            ->with('package')
            ->get();
        // Sort packages by carrier and third party shipment id
        foreach ($supplierOrderItems as $supplierOrderItem) {
            if (!$supplierOrderItem->package->label_url) {
                throw new Exception("One or more of the packages in job ". $supplierOrderItem->job_id ." has not finished label generation. Please check the issues tab and try again.");
            }
            $carrier = $supplierOrderItem->package->carrier;
            $shipmentArray = ['id' => $supplierOrderItem->package->thirdparty_id];
            $shipmentIdsByCarrier[$carrier][] = $shipmentArray;
        }
        // Generate batch shipment for each carrier and request daily scan from from webhook
        foreach ($shipmentIdsByCarrier as $carrier => $shipmentIds) {
            if (!empty($carrier)) {
                $batchShipment = $this->generateBatchShipments($shipmentIds);
                $packageBatch = new PackageBatch();
                $packageBatch->batch_id = $batchShipment->id;
                $packageBatch->carrier = $carrier;
                $packageBatch->contractor_id = $contractorId;
                $packageBatch->save();
                Package::whereIn('thirdparty_id', $shipmentIds)
                    ->update(array('package_batch_id' => $packageBatch->id));
            }
        }
    }

    public function generateBatchShipments ($shipment_ids)
    {
        $batchShipment = \EasyPost\Batch::create();
        $batchShipment->add_shipments ([
            'shipments' => $shipment_ids
        ]);
        $batchShipment->create_scan_form();
        return $batchShipment;
    }
}
