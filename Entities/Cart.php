<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Cart extends Eloquent
{
    protected $fillable = [
        'cisession_id',
        'price',
    ];
    protected $appends = array();
    protected $hidden = array();
    
    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    // Cart's parent relationships

    public function cisession()
    {
        return $this->hasOne('MerchPlatform\Entities\CiSession', 'session_id');
    }

    public function campaignItems()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignItem');
    }

    public function scopeCampaigns($query)
    {
        return $query->with('campaignItems.campaign.customer', 'campaignItems.variant.size', 'campaignItems.variant.color');
    }

    public function campaignItemsByCampaign()
    {
        $campaignIds = array();
        $campaigns = array();

        foreach ($this->campaignItems as $campaignItem) {
            //$campaign = $campaignItem->campaign; // @altmind: WTF is THAT. we use model to store transient properties that are not to be saved?
            // well, now we save our campaign and suddenly start getting errors about properties not existing in Campaign on sql insert.
            // is it indended? maybe just use plain StdObject?
            $campaign = (object)array('id' => $campaignItem->campaign->id);
            $campaignId = $campaign->id;

            if (!in_array($campaignId, $campaignIds)) {
                array_push($campaignIds, $campaignId);
                array_push($campaigns, $campaign);
            }
        }
        $campaigns = \Illuminate\Support\Collection::make($campaigns);

        foreach ($campaigns as $campaign) {
            $campaign->price_shipping = $campaign->price_items = $campaign->price_tax = 0.00;
            $campaignItems = array();
            foreach ($this->campaignItems as $campaignItem) {
                if ($campaignItem->campaign->id == $campaign->id) {
                    $campaignItems[] = $campaignItem;
                }

                // Add to sum up the shipping and items prices for the campaign (campaign order if they place it)
                $campaign->price_shipping += $campaignItem->price_shipping;
                $campaign->price_items += $campaignItem->price_items;
                $campaign->price_tax += $campaignItem->price_tax;
                $campaign->num_pieces += $campaignItem->qty;
            }

            // Sum up total price for the campaign (this is the total price of campaign_order if they place it)
            $campaign->price_total = $campaign->price_shipping + $campaign->price_items + $campaign->price_tax;

            $campaign->campaignItems = \Illuminate\Support\Collection::make($campaignItems);
        }

        return $campaigns;
    }

    public function getCampaignCosts($totalSavings = 0)
    {
        $campaignCosts = array('shipping' => 0, 'items' => 0, 'tax' => 0, 'total' => 0);
        foreach ($this->campaignItems as $campaignItem) {
            $campaignCosts['items'] += $campaignItem->price_items;
            $campaignCosts['shipping'] += $campaignItem->price_shipping;
            $campaignCosts['tax'] += $campaignItem->price_tax;
        }
        $campaignCosts['items'] -= $totalSavings;
        $campaignCosts['savings'] = $totalSavings;
        $campaignCosts['total'] = $campaignCosts['items'] + $campaignCosts['tax'] + $campaignCosts['shipping'];

        return $campaignCosts;
    }

    public function getCampaignNumUnits()
    {
        $this->campaignNumUnits = 0;
        foreach($this->campaignItems as $campaignItem) {
            $this->campaignNumUnits += $campaignItem->qty;
        }

        return $this->campaignNumUnits;
    }

    public static function getWithItems($cartId)
    {
        $cart = array(
            'price_shipping' => 0,
            'price_items' => 0,
            'price_tax' => 0,
            'price_total' => 0
        );
        $campaignItems = DB::table('campaign_items')
            ->select(
                'campaign_items.cart_id',
                DB::raw('campaign_items.price_items / 100 AS price_items'),
                DB::raw('campaign_items.price_shipping / 100 AS price_shipping'),
                DB::raw('campaign_items.price_tax / 100 AS price_tax'),
                'campaign_items.qty',
                DB::raw('campaign_items.upsell_discount_amount / 100 AS upsell_discount_amount'),
                'campaign_items.campaign_id',
                'campaigns.ends_at',
                'campaigns.url AS campaign_url',
                'variants.product_id',
                'variants.color_id',
                'variants.size_id',
                'products.short_name AS product_name',
                'colors.name AS color_name',
                'sizes.name AS size_name',
                'campaigns.upsells_enabled'
            )
            ->join('carts', 'carts.id', '=', 'campaign_items.cart_id')
            ->join('variants', 'variants.id', '=', 'campaign_items.variant_id')
            ->join('products', 'products.id', '=', 'variants.product_id')
            ->join('colors', 'colors.id', '=', 'variants.color_id')
            ->join('sizes', 'sizes.id', '=', 'variants.size_id')
            ->join('campaigns', 'campaigns.id', '=', 'campaign_items.campaign_id')
            ->where('campaign_items.cart_id', $cartId)
            ->get();

        foreach ($campaignItems as $campaignItem) {
            $cart['id'] = $campaignItem['cart_id'];
            $cart['price_shipping'] += $campaignItem['price_shipping'];
            $cart['price_items'] += $campaignItem['price_items'];
            $cart['price_tax'] += $campaignItem['price_tax'];
            $cart['price_total'] += array_sum(array(
                $campaignItem['price_shipping'],
                $campaignItem['price_items'],
                $campaignItem['price_tax']
            ));
            $cart['items'][] = $campaignItem;
        }
        return $cart;
    }
    
}