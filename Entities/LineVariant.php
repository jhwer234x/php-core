<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LineVariant extends Eloquent
{
    protected $fillable = array('color_id', 'size_id', 'qty', 'variant_id', 'supplier_order_id', 'line_id');
    protected $appends = array();
    protected $hidden = array();

    public function getCostAttribute($value)
    {
        return money_format('%i', $value / 100);
    }

    public function setCostAttribute($value)
    {
        $this->attributes['cost'] = $value * 100;
    }

    public function line()
    {
        return $this->belongsTo('MerchPlatform\Entities\Line');
    }

    public function variant()
    {
        return $this->belongsTo('MerchPlatform\Entities\Variant');
    }

    public function supplierOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\SupplierOrder');
    }

    public function campaignItems()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignItems');
    }

    public function packageItems()
    {
        return $this->hasMany('MerchPlatform\Entities\PackageItem');
    }

    public function designPositions()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignPosition');
    }

}