<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Payment extends Eloquent
{
    protected $fillable = array(
        'seller_id',
        'buyer_id',
        'contractor_id',
        'supplier_id',
        'type',
        'status',
        'total',
        'payee',
        'payee_type',
        'admin_id',
        'contractor_id',
        'supplier_id',
        'description',
        'transaction_token',
        'transaction_service',
        'transaction_meta',
        'notes',
        'paypal_email',
        'stripe_rec_token',
        'bill_com_bill_id',
        'bill_com_vendor_id',
        'payment_source_id'
    );
    protected $appends = array(
        'status_label',
    );

    public function getTotalAttribute($value)
    {
        return $value / 100;
    }

    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = $value * 100;
    }

    public function admin()
    {
        return $this->belongsTo('MerchPlatform\Entities\Admin');
    }

    public function paymentSource()
    {
        return $this->belongsTo('MerchPlatform\Entities\PaymentSource');
    }

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function seller()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'seller_id');
    }

    public function buyer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'buyer_id');
    }

    public function contractor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Contractor');
    }

    public function supplier()
    {
        return $this->belongsTo('MerchPlatform\Entities\Supplier');
    }

    public function items()
    {
        return $this->hasMany('MerchPlatform\Entities\PaymentItem');
    }

    public function payoutItems()
    {
        return $this->hasMany('MerchPlatform\Entities\PayoutItem');
    }

    // Sets custom "status_label" attribute (bootstrap label color class name).. Based on the Payouts's 'status' value
    public function getStatusLabelAttribute()
    {
        if($this->attributes['status'] == 'Pending')
        {
            return 'default';
        }
        elseif($this->attributes['status'] == 'Rejected')
        {
            return 'danger';
        }
        elseif($this->attributes['status'] == 'Failed')
        {
            return 'warning';
        }
        elseif($this->attributes['status'] == 'Paid')
        {
            return 'success';
        }

        return 'default';
    }

    public function getItemsTypeAttribute()
    {
        if(isset($this->attributes['customer_id']))
        {
            return 'Campaign';
        }
        elseif(isset($this->attributes['supplier_id']))
        {
            return 'Purchase Order';
        }
        elseif(isset($this->attributes['contractor_id']))
        {
            return 'Job';
        }
        else
        {
            return 'Item';
        }
    }

    public function getItemsAdminLinkAttribute()
    {
        if(isset($this->attributes['customer_id']))
        {
            return '/admin/campaigns/campaign/';
        }
        elseif(isset($this->attributes['supplier_id']))
        {
            return '/admin/supply/order?id=';
        }
        elseif(isset($this->attributes['contractor_id']))
        {
            return '/admin/production/job?id=';
        }
        else
        {
            return '#';
        }
    }

    public function getUserAttribute()
    {
        if(isset($this->attributes['customer_id']))
        {
            return $this->customer;
        }
        elseif(isset($this->attributes['supplier_id']))
        {
            return $this->supplier;
        }
        elseif(isset($this->attributes['contractor_id']))
        {
            return $this->contractor;
        }
        elseif(isset($this->attribtues['admin_id']))
        {
            return $this->admin;
        }

        return null;
    }

}