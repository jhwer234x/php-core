<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();
    
    public function parent()
    {
        return $this->belongsTo('Fulfill\Tronic\Models\MerchPlatform\Category', 'parent_id');
    }


    // Category's child relationships

    public function children()
    {
        return $this->hasMany('Fulfill\Tronic\Models\MerchPlatform\Category', 'parent_id');
    }

    // Category's many-to-many relationships

    public function products()
    {
        return $this->belongsToMany('Fulfill\Tronic\Models\MerchPlatform\Product');
    }

    public function scopeItemCats($query)
    {
        return $query->whereType('item_sub')->orderBy('name', 'asc');
    }

    public function scopeAgeCats($query)
    {
        return $query->whereType('age')->orderBy('name', 'asc');
    }

    public function scopeSexCats($query)
    {
        return $query->whereType('gender')->orderBy('name', 'asc');
    }

}