<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MarketingEmailFilterItem extends Eloquent
{
    protected $fillable = array('marketing_email_filter_id', 'campaign_id', 'tag_id');

    public function tag()
    {
        return $this->belongsTo('MerchPlatform\Entities\Tag');
    }
    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function marketingEmailFilter()
    {
        return $this->belongsTo('MerchPlatform\Entities\MarketingEmailFilter');
    }

    public function campaignOrTagName(){
        if ($this->campaign_id){
            return $this->campaign->title;
        } else if ($this->tag_id){
            return $this->tag->name;
        }
    }

}