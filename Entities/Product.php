<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DBManager;

class Product extends Eloquent
{
    protected $fillable = array('brand_id', 'name', 'slug', 'details', 'default_color_id', 'code', 'bodek_code', 'ss_code', 'description', 'weight_oz', 'is_active', 'category_id_item', 'category_id_gender', 'category_id_age', 'short_name', 'item_key', 'flat_cost');
    protected $appends = array();
    protected $hidden = array();

    public function colors()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Color', 'variants', 'product_id', 'color_id')->groupBy('product_id', 'color_id');
    }

    public function activeColors()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Color', 'variants', 'product_id', 'color_id')->whereIsActive(1)->groupBy('product_id', 'color_id');
    }

    public function sizes()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Size', 'variants', 'product_id', 'size_id')->groupBy('product_id', 'size_id')->orderBy('display_order', 'asc');
    }
    
    public function brand()
    {
        return $this->belongsTo('MerchPlatform\Entities\Brand');
    }

    public function defaultColor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Color', 'default_color_id');
    }

    public function groups()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\Group', 'MerchPlatform\Entities\Line');
    }

    public function lines()
    {
        return $this->hasMany('MerchPlatform\Entities\Line');
    }

    public function variants()
    {
        return $this->hasMany('MerchPlatform\Entities\Variant');
    }

    public function views()
    {
        return $this->hasMany('MerchPlatform\Entities\View');
    }

    public function categories()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Category');
    }

    // Cycles through product's views, filters them out unless it is the default view, then firsts() on the returned collection because there should only be one default view anyway
    public function getDefaultViewAttribute()
    {
        return $this->views->filter(function($view){
                if($view->is_default) {
                    return true;
                }
            })->first();
    }

    public function setFlatCostAttribute($value)
    {
        $this->attributes['flat_cost'] = $value * 100;
    }

    public function getFlatCostAttribute($value)
    {
        return $value / 100;
    }

    // Format into an attribute the default view img name
    public function getDefaultViewImgAttribute()
    {
        return $this->id.'-'.$this->default_color->id.'-'.$this->default_view->name.'.png';
    }

    public function getDefaultImgAttribute()
    {
        return $this->id.'-Front.png';
    }

    public function getNumColorsAttribute()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Color', 'variants', 'product_id', 'color_id')->groupBy('product_id', 'color_id')->count();
    }

    public function viewFiles()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\ViewFile', 'MerchPlatform\Entities\View');
    }

    public function file()
    {
        return $this->belongsTo('MerchPlatform\Entities\File');
    }

    public function getCategoryIconAttribute()
    {
        $this->categoryIconName = 'icon-ctg-tshirts';
        $this->categories->each(function($category){
            if($category->slug == 'tees-short-sleeve' || $category->slug == 'tees-long-sleeve' || $category->slug == 'tees-sleeveless') {
                $this->categoryIconName = 'icon-ctg-tshirts';
            }
            else if($category->slug == 'sweats-hoodies' || $category->slug == 'sweats-zip-hoodies' || $category->slug == 'sweats-shorts' || $category->slug == 'sweats-sweatpants') {
                $this->categoryIconName = 'icon-ctg-sweats';
            }
            else if($category->slug == 'businesswear-polo') {
                $this->categoryIconName = 'icon-ctg-businesswear';
            }
            else if($category->slug == 'performance-long-sleeve' || $category->slug == 'performance-short-sleeve') {
                $this->categoryIconName = 'icon-ctg-performance';
            }
            else if($category->slug == 'sweats-hoodies' || $category->slug == 'sweats-zip-hoodies' || $category->slug == 'sweats-shorts' || $category->slug == 'sweats-sweatpants') {
                $this->categoryIconName = 'icon-ctg-youth';
            }
            else if($category->slug == 'headwear-beanie' || $category->slug == 'headwear-snapback' || $category->slug == 'headwear-strapped') {
                $this->categoryIconName = 'icon-ctg-headwear';
            }
        });

        return $this->categoryIconName;
    }

    // Get product data and all its data needed for studio
    public static function designerData($productId, $fromCache = true)
    {   
        $product = \MerchPlatform\Entities\Product::with(array(
            'categories',
            'views' => function($q) {
                $q->orderBy('display_order', 'asc');
            },
            'viewFiles.file',
            'sizes',
            'activeColors'
        ))->find($productId);

        if(!$product) {
            return false;
        }

        return $product;
    }

    public function setDefaultResalePriceAttribute($value)
    {
        $this->attributes['default_resale_price'] = $value * 100;
    }

    public function getDefaultResalePriceAttribute($value)
    {
        return $value / 100;
    }
}
