<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PaymentItem extends Eloquent
{
    protected $fillable = array(
        'order_id',
        'payment_id',
        'total',
        'job_id',
        'supplier_order_id'
    );
    protected $appends = array();
    protected $hidden = array();

    public function getTotalAttribute($value)
    {
        return $value / 100;
    }

    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = $value * 100;
    }

    public function job()
    {
        return $this->belongsTo('MerchPlatform\Entities\Job');
    }

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order');
    }

    public function payout()
    {
        return $this->belongsTo('MerchPlatform\Entities\Payout');
    }

    public function supplierOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\SupplierOrder');
    }

    public function getTypeIdAttribute()
    {
        if(isset($this->attributes['campaign_id']))
        {
            return $this->attributes['campaign_id'];
        }
        elseif(isset($this->attributes['order_id']))
        {
            return $this->attributes['order_id'];
        }
        elseif(isset($this->attributes['job_id']))
        {
            return $this->attributes['job_id'];
        }
        elseif(isset($this->attributes['supplier_order_id']))
        {
            return $this->attributes['supplier_order_id'];
        }
        else
        {
            return $this->attributes['id'];
        }
    }

    public function getItemTitleAttribute()
    {
        if(isset($this->attributes['campaign_id']))
        {
            return $this->campaign->title;
        }
        elseif(isset($this->attributes['order_id']))
        {
            return $this->order->order_num;
        }
        elseif(isset($this->attributes['job_id']))
        {
            return $this->job->job_num;
        }
        elseif(isset($this->attributes['supplier_order_id']))
        {
            return $this->supplierOrder->order_num;
        }
        else
        {
            return str_pad($this->attributes['id'], 4, '0', STR_PAD_LEFT);
        }
    }

}