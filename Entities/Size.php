<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Size extends Eloquent
{
    protected $fillable = array('name', 'name_long', 'display_order', 'display_order_cart', 'bodek_code', 'ss_code', 'aa_code', 'broder_code', 'sanmar_code');
    protected $appends = array();
    protected $hidden = array();

    public function variants()
    {
        return $this->hasMany('MerchPlatform\Entities\Variant');
    }

}