<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PackageType extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();

    public function packages()
    {
        return $this->hasMany('MerchPlatform\Entities\Package');
    }

}