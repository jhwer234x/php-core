<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DBManager;

class DecColor extends Eloquent
{
    protected $fillable = array('pantone', 'hex', 'dec_method', 'thread_code');
    protected $appends = array();
    protected $hidden = array();
    
    public static function designerData()
    {
        $decColors = \MerchPlatform\Entities\DecColor::where('dec_method', '=', 'SCR')
            ->get();

        return $decColors;
    }

    public function designs()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Design', 'dec_color_design', 'dec_color_id', 'design_id');
    }

    public function designVariants()
    {
        return $this->belongsToMany('MerchPlatform\Entities\DesignVariant', 'dec_color_design', 'dec_color_id', 'design_variant_id');
    }

    public function designObjects()
    {
        return $this->belongsToMany('MerchPlatform\Entities\DesignObject', 'dec_color_design', 'dec_color_id', 'design_object_id');
    }

}