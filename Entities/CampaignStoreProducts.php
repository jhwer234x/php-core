<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignStoreProducts extends Eloquent
{
    protected $fillable = array(
        'auth_integration_id',
        'campaign_id',
        'external_product_id'        
    );

    protected $appends = array();
    protected $hidden = array();

    public $timestamps = true;

    public function campaignIntegration()
    {
        return $this->hasOne('MerchPlatform\Entities\Integration','auth_integration_id','auth_integration_id');
    }
    
}
