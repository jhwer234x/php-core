<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MarketingEmailFilter extends Eloquent
{
    protected $fillable = array('customer_id', 'type');

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function marketingEmail(){
        return $this->hasOne('MerchPlatform\Entities\MarketingEmail');
    }

    public function marketingEmailFilterItems()
    {
        return $this->hasMany('MerchPlatform\Entities\MarketingEmailFilterItem');
    }
}