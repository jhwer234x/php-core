<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon as Carbon;
use Omnipay\Omnipay;
use Illuminate\Database\Capsule\Manager as DB;
use MerchPlatform\Exceptions\AccessDeniedException;
use MerchPlatform\Entities\Setting;
use MerchPlatform\Entities\CampaignCost;
use MerchPlatform\Finance\Quote;
use MerchPlatform\Entities\Upsell;

class Campaign extends Eloquent
{
    protected $fillable = array(
        'title',
        'address_id',
        'milestone',
        'url',
        'ends_at',
        'customer_fulfills',
        'description',
        'customer_id',
        'admin_status',
        'is_unlisted',
        'is_featured',
        'fake_sold',
        'real_sold',
        'fb_pixel',
        'rt_pixel',
        'rt_google_pixel',
        'old_url',
        'profit',
        'is_draft',
        'footer_script',
        'default_product_id',
        'default_view_name',
        'show_sold',
        'upsells_enabled',
        'show_timer',
        'roll_over_sold',
        'auto_relaunch',
        'fb_audience_pixel',
        'launched_at',
        'price_method'
    );

    protected $appends = array(
        'approval_deadline',
        'ended'
    );

    protected $hidden = array();

    protected static $optionFields = array(
        'show_sold',
        'show_timer',
        'roll_over_sold',
        'auto_relaunch',
        'is_unlisted',
        'fb_pixel',
        'rt_pixel',
        'rt_google_pixel',
        'fb_audience_pixel'
    );

    public static function getOptionFields()
    {
        return self::$optionFields;
    }

    public function relaunchedCampaigns()
    {
        return $this->hasMany('MerchPlatform\Entities\Campaign', 'original_id');
    }

    public function getEndedAttribute()
    {
        if (is_null($this->ends_at)) {
            return false;
        } else {
            return (new Carbon($this->ends_at))->lt(Carbon::now());
        }
        
    }

    public function getDaysActiveAttribute()
    {
        return Carbon::now()->diffInDays($this->ends_at);
    }

    public function scopeRange($query, $ranges)
    {
        return $query->whereBetween('ends_at', $ranges);
    }

    public function getSoldAttribute()
    {
        $unitsSold = DB::select('select sum(campaign_items.qty) as unitsSold from campaign_items join campaign_orders on campaign_orders.id = campaign_items.campaign_order_id where campaign_orders.campaign_id = '.intval($this->id).' and campaign_orders.status = "paid"')[0]['unitsSold'];
        return $unitsSold != null ? $unitsSold : 0;
    }

    public function getProfitAttribute($value)
    {
        return $value / 100;
    }

    public function setProfitAttribute($value)
    {
        $this->attributes['profit'] = $value * 100;
    }

    public function getPricePerAttribute($value)
    {
        return $value / 100;
    }

    public function setPricePerAttribute($value)
    {
        $this->attributes['price_per'] = $value * 100;
    }

    public function getCostPerAttribute($value)
    {
        return $value / 100;
    }

    public function setCostPerAttribute($value)
    {
        $this->attributes['cost_per'] = $value * 100;
    }

    public function getCustomerProfitAttribute($value)
    {
        return $value / 100;
    }

    public function setCustomerProfitAttribute($value)
    {
        $this->attributes['customer_profit'] = $value * 100;
    }

    public function getThumbnailImageUrl($by = 'url')
    {
        if ($by === 'url') {
            $url = $this->url ? $this->url : ($this->old_url ? $this->old_url : null);
            return $url ? ('/campaign_imgs/' . $url . '/thumbnail.png') : null;
        }
        elseif ($by === 'id') {
            return '/images/drafts/' . $this->id . '/preview.png';
        }
    }

    public function getMockupImageUrl()
    {
        $url = $this->url ? $this->url : ($this->old_url ? $this->old_url : null);
        return $url ? ('/campaign_imgs/' . $url . '/product.png') : null;
    }

    public function getDesignedMockupImageUrl()
    {
        $url = $this->url ? $this->url : ($this->old_url ? $this->old_url : null);
        return $url ? ('/campaign_imgs/' . $url . '/mockup.png') : null;
    }

    public function getSales($campaignId, $timezone = 0)
    {
        $yesterdayUserUTC = Carbon::yesterday($timezone)->setTimezone('UTC');
        $tomorrowUserUTC = Carbon::tomorrow($timezone)->setTimezone('UTC');
        $todayUserUTC = Carbon::today($timezone)->setTimezone('UTC');
        $nowUserUTC = Carbon::now($timezone)->setTimezone('UTC');
        $summary = DB::select(DB::raw('
            select
                m.id,
                sum(case when mo.created_at >= "'.$yesterdayUserUTC.'" AND mo.created_at < "'.$todayUserUTC.'" then mi.qty else 0 end) as yesterday,
                sum(case when mo.created_at >= "'.$todayUserUTC.'" AND mo.created_at < "'.$tomorrowUserUTC.'" then mi.qty else 0 end) as today,
                sum(mi.qty) as total
            from campaigns m
            join campaign_orders mo
                on m.id = mo.campaign_id
                    and mo.campaign_id = '.intval($campaignId).'
            left join campaign_items mi
                on mo.id = mi.campaign_order_id
                and m.id = '.intval($campaignId).';
        '))[0];
        return $summary;
    }

    public function admin()
    {
        return $this->belongsTo('MerchPlatform\Entities\Admin');
    }

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function tags()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Tag', 'campaign_tags');
    }

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order');
    }

    public function address()
    {
        return $this->belongsTo('MerchPlatform\Entities\Address');
    }

    public function originalCampaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign', 'original_id');
    }

    public function getOriginalCampaignId()
    {
        $campaignIdToUse = $this->id;
        if($this->original_id) {
            $campaignIdToUse = $this->original_id;
        }
        return $campaignIdToUse;
    }

    public function group()
    {
        return $this->hasOne('MerchPlatform\Entities\Group');
    }

    public function referrers()
    {
        return $this->hasMany('MerchPlatform\Entities\Referrer');
    }


    public function campaignOrders()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignOrder');
    }

    public function campaignCosts()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignCost');
    }

    public function payoutItem()
    {
        return $this->hasOne('MerchPlatform\Entities\PayoutItem');
    }

    public function packages()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\Package', 'MerchPlatform\Entities\CampaignOrder');
    }


    public function getDates()
    {
        return array('ends_at', 'created_at', 'updated_at', 'launched_at');
    }

    public function campaignStoreProducts()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignStoreProducts');
    }

    // Custom attribute. Array with two values. 'hours_left' is the number of hours since the Order was created. 'label' is the bootstrap label to use (color)
    public function getApprovalDeadlineAttribute()
    {
        $hoursSinceCreated = Carbon::createFromTimestamp(strtotime($this->attributes['created_at']))->diffInHours(Carbon::now(), false);
        $hoursLeft = 36-$hoursSinceCreated;

        if($hoursLeft >= 24)
        {
            $label = 'success';
        }
        elseif($hoursLeft >= 12)
        {
            $label = 'warning';
        }
        else
        {
            $label = 'danger';
        }

        $approvalDeadline = array('hours_left' => $hoursLeft, 'label' => $label);

        return $approvalDeadline;
    }

    public function getNumPackagesAttribute()
    {
        return $this->packages->count();
    }

    public function getIsChargeableAttribute()
    {
        if($this->real_sold >= $this->milestone) {
            return true;
        }

        return false;
    }

    public function scopeIsActive($query)
    {
        return $query->IsNotDraft()->where(function($q) {
            $q->where('ends_at', '>', Carbon::now())
                ->orWhereNull('ends_at');
            });
    }

    public function scopeIsDraft($query)
    {
        return $query->where('is_draft', 1);
    }

    public function scopeIsNotDraft($query)
    {
        return $query->where('is_draft', 0);
    }

    public function scopeIsBuyable($query)
    {
        return $query->whereAdminStatus('Approved');
    }

    public function scopeIsEnded($query)
    {
        return $query->IsNotDraft()
            ->where('ends_at', '<=', Carbon::now())
            ->whereNotNull('ends_at');
    }

    public function scopeIsVisible($query)
    {
        return $query->isActive()->where('admin_status', 'Approved');
    }

    public function scopeIsSuccessful($query)
    {
        return $query->whereRaw('real_sold >= milestone')->whereHas('campaignOrders',
            function($q) {
                $q->whereStatus('paid');
            });
    }

    public function scopeHasUnpaidOrders($query)
    {
        return $query->whereHas('campaignOrders', function($q){
            $q->whereStatus('unpaid')
                ->whereNull('parent_id');
        });
    }

    public function scopeEndedToday($query)
    {
        return $query->where('ends_at', '=', Carbon::today());
    }

    public function fillFromUserInput($properties, $controller)
    {
        if ($controller->isUser('admin') && $controller->user_data['perms']['edit_campaigns']) {
            $properties = array_except($properties, array('real_sold'));
        } elseif (isMyCampaign($this, $controller)) {
            // Only let a customer edit if it's their Campaign and it's not yet Ended/Rejected
            if ($this->admin_status == 'Approved' && $this->is_active) {
                $properties = array_only($properties,
                    array(
                        'title',
                        'description',
                        'plus_size_addition',
                        'pickup_details',
                        'is_unlisted',
                        'show_sold',
                        'show_timer',
                        'roll_over_sold',
                        'auto_relaunch',
                        'fb_pixel',
                        'rt_pixel',
                        'rt_google_pixel',
                        'fb_audience_pixel',
                        'tags',
                        'default_view_name'
                    ));
            } elseif(!$this->is_active) {
                $properties = array_only($properties, array('tags'));
            } else {
                throw new \RuntimeException("This campaign is not active or has been rejected");
            }
        } else {
            throw new AccessDeniedException("Current user cannot modify campaign #{$this->id}");
        }
        if ($properties['description']) {
            $properties['description'] = html_purify($properties['description']);
        }
        if (!empty($properties['tags'])) {
            $tags = Tag::getFromNames($properties['tags']);
            $this->tags()->detach();
            $this->tags()->saveMany($tags);
        } else {
            $this->tags()->detach();
        }
        unset($properties['tags']);
        // Start doing the actual updating at this point. Loop through for any column names
        // @altmind: could we have just set up $fillable and used $campaign->update($properties) ?
        foreach ($properties as $key => $value) {
            $this->{$key} = $value;
        }
        return $this; // fluent interface
    }

    public function scopeIsNotSuccessful($query)
    {
        return $query->whereRaw('real_sold < milestone');
    }

    public function scopeEndedSuccessfully($query)
    {
        return $query->isSuccessful()->isEnded();
    }
    public function scopeIsNotRejected($query)
    {
        return $query->where('admin_status', '!=', 'Rejected');
    }

    public function scopeNotYetPaid($query)
    {
        return $query->has('payoutItem', '=', 0);
    }

    // active, inactive, all
    public function scopeWithProducts($query, $activeStatus = 'all')
    {
        $activeValueArray = [1, 0];
        if ($activeStatus === 'active') {
            $activeValueArray = [1];
        } elseif ($activeStatus === 'inactive') {
            $activeValueArray = [0];
        }

        return $query->with(array(
            'group.lines' => function($q) use($activeValueArray) {
                $q->whereHas('lineVariants', function($q) use($activeValueArray) {
                    $q->whereHas('variant', function($q) use($activeValueArray) {
                        $q->whereIn('is_active', $activeValueArray)
                            ->with('product.sizes');
                    });
                });
            },
            'group.lines.lineVariants' => function($q) use($activeValueArray) {
                $q->whereHas('variant', function($qv) use($activeValueArray) {
                    $qv->whereIn('is_active', $activeValueArray);
                })->with(array(
                    'variant' => function($qv) use($activeValueArray) {
                        $qv->with('color')
                            ->whereIn('is_active', $activeValueArray);
                    }));
                },
            'group.designs.designVariants.mockupFile',
            'group.designs.designVariants.designFile',
            'group.lines.product.viewFiles' => function($vfq) {
                $vfq->whereHas('view', function($vq) {
                    $vq->whereIn('name', array('Front', 'Back'));
                });
            },
            'group.lines.product.viewFiles.file',
            'group.lines.product.viewFiles.view'
        ));
    }

    public function scopeWithLineVariants($query)
    {
        return $query->with(array(
            'group.lines.product',
            'group.lines.lineVariants.variant.color'
        ));
    }


    public function getCampaignIncome()
    {
        $this->load('campaignOrders');

        $this->total = 0;
        $this->campaignOrders->each(function($order){
            if($order->status == 'paid') {
                $this->total += $order->price_total;
            }
        });
        $campaignIncome = $this->total;
        unset($this->total);

        return $campaignIncome;
    }

    /**
     * getCampaignExternalPrice
     *
     * @return {Integer} Total amount charged for production and fulfillment of external campaignOrders, in cents
     */
    public function getCampaignExternalPrice()
    {
        try {
            $externalDomesticShippingCost = Setting::whereKey('external_domestic_shipping_cost_usd')
                ->first()->value;
            $externalInternationalShippingCost = Setting::whereKey('external_international_shipping_cost_usd')
                ->first()->value;
            $excluded = Setting::whereKey('no_charge_integrations')->first()->value;
            $excluded = json_decode($excluded);
        } catch (\Exception $e) {
            $excluded = array();
        }
        $this->load(
            'campaignOrders.campaignItems.line',
            'campaignOrders.order.address.country',
            'campaignOrders.campaignOrderExternals');

        $priceExternal = 0;
        $shippingCost = 0;
        foreach($this->campaignOrders as $campaignOrder) {
            if(($campaignOrder->campaignOrderExternals->count() > 0) && ($campaignOrder->status == 'paid')) {
                if (in_array($campaignOrder->campaignOrderExternals->first()->auth_integration_id, $excluded)) {
                    continue;
                }
                $country = $campaignOrder->order->address->country;
                foreach($campaignOrder->campaignItems as $idx => $campaignItem) {
                    if ($country->is_domestic) {
                        $shippingCost += $externalDomesticShippingCost * $campaignItem->qty;
                    } else {
                        $shippingCost += $externalInternationalShippingCost * $campaignItem->qty;
                    }

                    $priceExternal += $campaignItem->line->campaign_base_cost * $campaignItem->qty;
                }
            }
        }

        return $priceExternal + $shippingCost;
    }

    public function updateCustomerProfit()
    {
        Quote::getSellerQuoteByCampaignId($this->id, true);
    }

    public function chargeForExternalOrders($force = false)
    {
        $billing = new \MerchPlatform\Billing\Stripe(
            \MerchPlatform\Config\Main::get('stripe')['secret'],
            \MerchPlatform\Config\Main::get('stripe')['apiDate']
        );
        $email = new \MerchPlatform\Messaging\Email();
        $this->load('order.customer.cards');

        if ($this->order->status == 'paid') {
            return true;
        }
        if ($this->order->customer->cards->isEmpty() || !$this->order->customer->stripe_token) {
            $email->sendExternalOrderBillingFail($this);
            return false;
        }
        if (!$force && ($this->order->price_external > $this->order->customer->max_auto_charge)) {
            $email->sendExternalOrderBillingFail($this);
            return false;
        }

        $subject = "Production of campaign #".$this->id;
        try {
            $charge = $billing->createCharge(array(
                'amount' => $this->order->price_external * 100,
                'currency' => 'usd',
                'customer' => $this->order->customer->stripe_token,
                'card' => $this->order->customer->cards->first()->stripe_token,
                'description' => $subject
            ));
            // email hook: Send "your order receipt" email to campaign buyer
            $email->sendExternalOrderBilling($this);
            $this->order->stripe_charge_token = $charge->id;
            $this->order->status = 'paid';
            $this->order->save();
        } catch(\Exception $e) {
            // email hook: Send "your order charge failed" email to campaign buyer
            $email->sendExternalOrderBillingFail($this, 'fail');
            $this->order->status = 'failed';
            $this->order->save();
            return false;
        }
        return true;
    }

    public function chargeUnpaidOrders()
    {
        $billing = new \MerchPlatform\Billing\Stripe(
            \MerchPlatform\Config\Main::get('stripe')['secret'],
            \MerchPlatform\Config\Main::get('stripe')['apiDate']
        );
        $email = new \MerchPlatform\Messaging\Email();

        $paypalGateway =  Omnipay::create('PayPal_Express');
        $paypalGateway->setUsername(\MerchPlatform\Config\Main::get('paypal')['username']);
        $paypalGateway->setPassword(\MerchPlatform\Config\Main::get('paypal')['password']);
        $paypalGateway->setSignature(\MerchPlatform\Config\Main::get('paypal')['signature']);
        $paypalGateway->setTestMode(\MerchPlatform\Config\Main::get('paypal')['isSandbox']);

        $paypalGateway->setSolutionType('Sole');
        $paypalGateway->setLogoImageUrl(\MerchPlatform\Config\Main::get('paypal')['brandImgUrl']);
        $paypalGateway->setBrandName(\MerchPlatform\Config\Main::get('brand')['name']);
        foreach($this->campaignOrders as $campaignOrder)
        {
            if($campaignOrder->status != 'unpaid') {
                continue;
            }
            try {
                // Card was successfully charged
                // tris is Stripe
                if($campaignOrder->stripe_card_token){
                    // Do charge
                    $charge = $billing->createCharge(array(
                        'amount' => $campaignOrder->price_total*100, // amount in cents
                        'currency' => 'usd',
                        'customer' => $campaignOrder->order->customer->stripe_token,
                        'card' => $campaignOrder->stripe_card_token,
                        'description' => "Charge for campaign #".$this->id
                    ));

                    $campaignOrder->stripe_charge_token = $charge->id;
                } else if ($campaignOrder->paypal_transaction_token) {
                    // this is paypal
                    $paypalRequest = $paypalGateway->capture(array(
                        'transactionReference' => $campaignOrder->paypal_transaction_token,
                        'amount' => number_format(round($campaignOrder->price_total, 2, PHP_ROUND_HALF_UP), 2, '.', ''),
                        'currency' => 'USD'
                    ));
                    $paypalResponse = $paypalRequest->send();
                    $data = $paypalResponse->getData();
                    if ($paypalResponse->isSuccessful() && isset($data['PAYMENTSTATUS']) && ($data['PAYMENTSTATUS'] == 'Completed'/* || $data['PAYMENTSTATUS'] == 'Pending'*/)) {
                        // OK
                    } elseif (isset($data['PAYMENTSTATUS']) && $data['PAYMENTSTATUS'] !== 'Completed') {
                        throw new \Exception("Paypal transaction " + $campaignOrder->paypal_transaction_token + " was not completed: " + $data['PAYMENTSTATUS']);
                    } else {
                        throw new \Exception($paypalResponse->getMessage());
                    }
                }

                $campaignOrder->status = 'paid';
                $campaignOrder->charge_attempts++;
                $campaignOrder->save();

                // email hook: Send "your order is guaranteed" email to campaign buyer
                $email->sendBuyerCampaignMilestoneHit($campaignOrder->id);


            } catch(\Exception $e) {
                // The card has been declined

                // email hook: Send "your order charge failed" email to campaign buyer
                $email->sendBuyerCampaignChargeFail($campaignOrder->id);

                $campaignOrder->status = 'failed';
                $campaignOrder->charge_attempts++;
                $campaignOrder->save();
            }
        }
    }

    public function convertToOrder()
    {
        // Stop this script if the campaign has already been converted into an order
        if($this->order_id) {
            return false;
        }

        // Lazy load all relationships we will need for this script
        $this->load('campaignOrders.campaignItems', 'group.lines.lineVariants');

        // Put together the lineVarian ts that will be in this order
        $newLineVariants = array();
        foreach($this->campaignOrders as $campaignOrder)
        {
            // Only add to line variants if the campaign order is successfully paid for
            if($campaignOrder->status != 'paid') {
                continue;
            }

            foreach($campaignOrder->campaignItems as $campaignItem)
            {
                // If not already set, set the new line variant ID (key) and qty (value)
                if(!isset($newLineVariants[$campaignItem->variant_id])) {
                    $newLineVariants[$campaignItem->variant_id]['variant_id'] = $campaignItem->variant_id;
                    $newLineVariants[$campaignItem->variant_id]['product_id'] = $campaignItem->variant->product_id;
                    $newLineVariants[$campaignItem->variant_id]['qty'] = $campaignItem->qty;
                    $newLineVariants[$campaignItem->variant_id]['accounted_for'] = false;
                    continue;
                }

                // If it is already set, add to the quantity
                $newLineVariants[$campaignItem->variant_id]['qty'] += $campaignItem->qty;
            }
        }

        // Actually create the line variants for the order.
        foreach($this->group->lines as $line)
        {
            foreach($line->lineVariants as $lineVariant)
            {
                foreach($newLineVariants as $idx => $newLineVariant)
                {
                    if($lineVariant->variant_id == $newLineVariant['variant_id'])
                    {
                        $lineVariant->qty += $newLineVariant['qty'];
                        $lineVariant->is_auto_order = 1;
                        $lineVariant->save();
                        $newLineVariants[$idx]['accounted_for'] = true;
                    }
                }
            }
        }

        foreach($this->group->lines as $line)
        {
            foreach($newLineVariants as $newLineVariant)
            {
                if($newLineVariant['accounted_for'] === false && $newLineVariant['product_id'] == $line->product_id)
                {
                    \MerchPlatform\Entities\LineVariant::create(array(
                        'variant_id' => $newLineVariant['variant_id'],
                        'qty' => $newLineVariant['qty'],
                        'line_id' => $line->id
                    ));
                }
            }
        }

        $campaignIncome = $this->getCampaignIncome();
        $priceExternal = $this->getCampaignExternalPrice();
        $paidStatus = $priceExternal > 0 ? 'unpaid' : 'paid';
        $quote = Quote::getSellerQuoteByCampaignId($this->id);
        $decMethod = $quote['printMethod'];
        $order = \MerchPlatform\Entities\Order::create(array(
            'customer_id' =>    $this->customer_id,
            'price_bulk' =>     $campaignIncome,
            'price_total' =>    $campaignIncome,
            'price_external' => $priceExternal,
            'status' =>         $paidStatus,
            'payout_customer'=> $this->profit,
            'shippingdue_at' => NULL,
            'dec_method' => $decMethod
        ));

        $this->order_id = $order->id;
        $this->group->order_id = $order->id;
        $this->push();
    }

    public function getIsActiveAttribute()
    {
        if (is_null($this->ends_at)) {
            return true;
        } elseif ($this->ends_at > Carbon::now()) {
            return true;
        }

        return false;
    }

    public function relaunch($newUrl, $newEndDate = false)
    {
        $tippingPoint = Setting::whereKey('tipping_point')
            ->first()->value;
        $this->load(array('group' => function($q) {
            $q->with(array(
                'designs.designVariants.decColors',
                'lines.lineVariants'
            ));
        }));

        $campaignLengthRequirements = Setting::where('key', 'maximum_campaign_length')
            ->orWhere('key', 'minimum_campaign_length')
            ->lists('value','key');
        $max = $campaignLengthRequirements['maximum_campaign_length'];
        $min = $campaignLengthRequirements['minimum_campaign_length'];
        if (!$newEndDate) {
            // Get new end date
            $reference_date = $this->launched_at ?: $this->created_at;
            $days = $this->ends_at->startOfDay()->diffInDays(
                    $reference_date->startOfDay()
                );
            // Temp fix in case campaign length in days was less than 1
            if ($days < $min) {
                $days = $min;
            } elseif ($days > $max) {
                $days = $max;
            }
            $newEndDate = Carbon::now()
                ->addDays($days);
        } else {
            try {
                $newEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $newEndDate);
                if($newEndDate > Carbon::now()->addDays($max)){
                    throw new \InvalidArgumentException("Date cant be more than {$max} days from today");
                }elseif($newEndDate<Carbon::now()->addDays($min)->subMinutes(10)){
                    throw new \InvalidArgumentException("Date cant be less than {$min} day from today");
                }
            } catch (InvalidArgumentException $e){
                throw new \Exception('Invalid $newEndDate: ' . $newEndDate);
            }
        }

        // If the campaign does not have any paid orders, then just extend
        // the existing campaign (do not create a whole new campaign)
        if (!$this->sold) {
            // Cancel any unpaid orders in case this had a tipping point > 1
            DB::table('campaign_orders')
                ->where('campaign_id', $this->id)
                ->where('status', 'unpaid')
                ->update(array(
                    'status' => 'canceled'
                ));
            $this->launched_at = Carbon::now();
            $this->ends_at = $newEndDate;
            $this->save();
            $this->checkAndAddLineVariants($this->id);
            return $this;
        }

        // Unmap URL from old campaign
        $this->url = null;
        $this->old_url = $newUrl;
        $this->save();

        $originalCampaign = $this->getOriginalCampaign($this);

        // Get total sold for this campaign instance (the original and all its relaunches so far)
        $campaignInstances = \MerchPlatform\Entities\Campaign::where('original_id', '=', $originalCampaign->id)
            ->orWhere('id', '=', $originalCampaign->id)
            ->get();
        $originalSold = 0;
        foreach ($campaignInstances as $campaignInstance) {
            if ($campaignInstance->real_sold >= $campaignInstance->milestone) {
                $originalSold += $campaignInstance->real_sold;
            }
        }

        $newCampaign = $this->replicate(array('id', 'order_id', 'real_sold', 'fake_sold'));
        $newCampaign->url = $newUrl;
        $newCampaign->original_id = $originalCampaign->id;
        $newCampaign->original_real_sold = $originalSold;
        $newCampaign->created_at = Carbon::now();
        $newCampaign->launched_at = Carbon::now();
        $newCampaign->ends_at = $newEndDate;
        $newCampaign->labelgen_status = 'ungenerated';
        $newCampaign->charge_status = 'uncharged';
        $newCampaign->fb_pixel = $originalCampaign->fb_pixel;
        $newCampaign->rt_pixel = $originalCampaign->rt_pixel;
        $newCampaign->rt_google_pixel = $originalCampaign->rt_google_pixel;
        $newCampaign->labels_requested = 0;
        $newCampaign->profit = null;
        $newCampaign->milestone = $tippingPoint;
        $newCampaign->price_method = $originalCampaign->price_method;
        $newCampaign->save();

        $newGroup = $this->group->replicate(array('id', 'campaign_id', 'order_id'));
        $newGroup->save();
        $newGroup->campaign()->associate($newCampaign);
        $newLines = array(); $oldNewLineIds = array();
        foreach($this->group->lines as $line)
        {
            $newLine = $line->replicate(array('id', 'group_id'));
            $newLine->save();

            $oldNewLineIds[$line->id] = $newLine->id;
            $newLineVariants = array();
            foreach($line->lineVariants as $lineVariant) {
                $newLineVariant = $lineVariant->replicate(array('id', 'line_id'));
                $newLineVariant->qty = 0;
                $newLineVariants[] = $newLineVariant;
            }

            $newLine->lineVariants()->saveMany($newLineVariants);
            $newLines[] = $newLine;
        }

        $newDesigns = array();
        foreach($this->group->designs as $design)
        {
            $newDesign = $design->replicate(array('id', 'group_id'));
            $newDesign->save();

            $newDesignVariants = array();
            foreach($design->designVariants as $designVariant) {
                $newDesignVariants[] = $designVariant->replicate(array('id', 'design_id'));
            }
            // Map new line ids to design variants
            foreach($newDesignVariants as $newDesignVariant) {
                foreach($oldNewLineIds as $oldId => $newId) {
                    if($newDesignVariant->line_id == $oldId) {
                        $newDesignVariant->line_id = $newId;
                    }
                }
            }

            $newDesign->designVariants()->saveMany($newDesignVariants);
            $newDesigns[] = $newDesign;
        }

        $newGroup->lines()->saveMany($newLines);
        $newGroup->designs()->saveMany($newDesigns);
        $newGroup->push();

        $this->checkAndAddLineVariants($newCampaign->id);

        return $newCampaign;
    }

    public function end()
    {
        $this->ends_at = Carbon::now()->toDateTimeString();
        $this->save();
    }

    private function getOriginalCampaign($campaign)
    {
        if(isset($campaign->original_id)) {
            $maybeOriginalCampaign = \MerchPlatform\Entities\Campaign::with('group')
                ->find($campaign->original_id);
            return $this->getOriginalCampaign($maybeOriginalCampaign);
        } else {
            return $campaign;
        }
    }

    public function getCampaignInstanceIds()
    {
        $ids = array();
        $query = \MerchPlatform\Entities\Campaign::select('id')
            ->where('id', '=', $this->id)
            ->orWhere('original_id', '=', $this->id);
        if ($this->original_id) {
            $query->orWhere('original_id', '=', $this->original_id)
                ->orWhere('id', '=', $this->original_id);
        }
        $campaignInstances = $query->get();
        foreach ($campaignInstances as $campaignInstance) {
            $ids[] = $campaignInstance->id;
        }
        return $ids;
    }

    public function saveOptions($options)
    {
        foreach ($options as $idx => $option) {
            $options[$idx] = $option;
        }
        try {
            \MerchPlatform\Entities\Campaign::whereIn('id', $this->getCampaignInstanceIds())
                ->update($options);
            $returnValue = true;
        } catch (\Exception $e) {
            $returnValue = false;
        }

        return $returnValue;
    }

    public function saveCampaignCosts($costs, $accuracy, $jobId = null)
    {
        foreach ($costs as $cost) {
            $campaignCost = new CampaignCost;
            $campaignCost->name = $cost['name'];
            $campaignCost->type = $cost['type'];
            $campaignCost->units = $cost['units'];
            $campaignCost->cost = $cost['cost'];
            $campaignCost->meta = $cost['meta'];
            $campaignCost->accuracy = $accuracy;
            $campaignCost->job_id = $jobId;
            $this->campaignCosts()->save($campaignCost);
        }
    }

    public function getEditUrl()
    {
        return '/campaigns/' . $this->id . '/edit';
    }

    public function getViewUrl()
    {
        return '/campaigns/' . $this->url;
    }

    private function getUpsellObject($customerId, $campaignUpsellsEnabled)
    {
        $upsell = Upsell::where(array(
            'customer_id' => $customerId
        ))->first();

        if ($upsell && $upsell->is_active && !$campaignUpsellsEnabled) {
            return array(
                'isUniversalUpsell' => true,
                'discountAmount' => $upsell->discount_flat
            );
        }

        return array(
            'isUniversalUpsell' => false
        );
    }

    public function getVerboseSalesPageData($options)
    {
        $campaign = $this ?: Campaign::find($options['campaignId']);
        $campaign->load('customer');

        $upsell = $this->getUpsellObject($campaign->customer_id, $campaign->upsells_enabled);

        $salesPageData = $campaign->getSalesPageData($options, $upsell);
        $productsData = $campaign->getProductsData(['withMockups' => true], $upsell);

        $salesPageData['products'] = $productsData['products'];
        $salesPageData['variantExtraCosts'] = $productsData['variantExtraCosts'];
        return $salesPageData;
    }

    public function getSalesPageData($options, $upsell)
    {
        $campaign = $this ?: Campaign::find($options['campaignId']);
        $campaign->load('customer');

        $sold = $campaign->real_sold;
        $milestone = $campaign->milestone;

        if ($campaign->fake_sold) {
            $sold = $campaign->fake_sold;
        }

        if ($campaign->roll_over_sold) {
            $sold += $campaign->original_real_sold;
            $milestone += $campaign->original_real_sold;
        }

        $upsellsEnabled = $upsell['isUniversalUpsell'] ? 1 : $campaign->upsells_enabled;

        return array(
            'id' =>                     $campaign->id,
            'title' =>                  $campaign->title,
            'description' =>            $campaign->description,
            'sold' =>                   $sold,
            'milestone' =>              $milestone,
            'pickupDetails' =>          $campaign->pickup_details,
            'adminStatus' =>            $campaign->admin_status,
            'url' =>                    is_null($campaign->url) ? $campaign->old_url : $campaign->url,
            'createdAt' =>              $campaign->created_at ? $campaign->created_at->toDateTimeString() : null,
            'endsAt' =>                 $campaign->ends_at ? $campaign->ends_at->toRfc2822String() : null,
            'ended' =>                  $campaign->ended,
            'customerFulfills' =>       (int) $campaign->customer_fulfills,
            'profit' =>                 (float) $campaign->profit,
            'fbPixel' =>                $campaign->fb_pixel,
            'rtPixel' =>                $campaign->rt_pixel,
            'rtGooglePixel' =>          $campaign->rt_google_pixel,
            'footerScript' =>           $campaign->footer_script,
            'default_product_id' =>     (int) $campaign->default_product_id,
            'isUnlisted' =>             (int) $campaign->is_unlisted,
            'rollOverSold' =>           (int) $campaign->roll_over_sold,
            'autoRelaunch' =>           (int) $campaign->auto_relaunch,
            'thumbnail_default_image' => $campaign ? $campaign->getThumbnailImageUrl() : null,
            'mockup_default_image' =>   $campaign ? $campaign->getMockupImageUrl() : null,
            'mockup_designed_image' =>  $campaign ? $campaign->getDesignedMockupImageUrl() : null,
            'default_fbPixel' =>        $campaign->customer->default_fb_pixel,
            'default_rtPixel' =>        $campaign->customer->default_rt_pixel,
            'default_rtGooglePixel' =>  $campaign->customer->default_rt_google_pixel,
            'fbAudiencePixel' =>        $campaign->fb_audience_pixel ?: $campaign->customer->fb_audience_pixel,
            'defaultViewName' =>        $campaign->default_view_name,
            'upsellsEnabled' =>         (int) $upsellsEnabled,
            'showSold' =>               (int) $campaign->show_sold,
            'showTimer' =>              (int) $campaign->show_timer,
            'customerId' =>             $campaign->customer_id,
            'analyticsProfile' =>       $campaign->customer->analytics_profile
        );
    }

    public function getProductsData($options, $upsell)
    {
        $defaultOptions = array(
            'productActiveStatus' => 'active'
        );
        $options = array_merge($defaultOptions, $options);
        $products = array();
        $campaignId = ($this->id ?: $options['campaignId']);
        if (!$campaignId) {
            return $products;
        }

        $products = DB::table('lines')
            ->select(
                'products.id',
                'products.name',
                'products.description',
                'products.short_name AS shortName',
                'lines.campaign_price AS price',
                'lines.upsell_discount_amount',
                'lines.default_color_id',
                'lines.default_size_id',
                'lines.campaign_base_cost as baseCost'
            )
            ->join('products', 'products.id', '=', 'lines.product_id')
            ->join('groups', 'groups.id', '=', 'lines.group_id')
            ->whereCampaignId($campaignId)
            ->get();

        $variants = DB::table('variants')
            ->select(
                'colors.id AS color_id',
                'hex_prim AS hexPrim',
                'hex_sec AS hexSec',
                'colors.name AS color_name',
                'lines.product_id',
                'sizes.id AS size_id',
                'sizes.name AS size_name',
                'sizes.name_long AS size_name_long',
                'sizes.display_order AS size_display_order',
                'variants.extra_cost'
            )
            ->join('colors', 'colors.id', '=', 'variants.color_id')
            ->join('sizes', 'sizes.id', '=', 'variants.size_id')
            ->join('line_variants', 'line_variants.variant_id', '=', 'variants.id')
            ->join('lines', 'lines.id', '=', 'line_variants.line_id')
            ->join('groups', 'groups.id', '=', 'lines.group_id')
            ->whereCampaignId($campaignId);
            if ($options['productActiveStatus'] === 'active') {
                $variants->where('variants.is_active', '1');
            }
            $variants = $variants->get();
        $colors = $sizes = $variantExtraCosts = array();
        foreach ($variants as $variant) {
            $colors[$variant['product_id'].'-'.$variant['color_id']] = [
                'id' => $variant['color_id'],
                'hexPrim' => $variant['hexPrim'],
                'hexSec' => $variant['hexSec'],
                'name' => $variant['color_name'],
                'product_id' => $variant['product_id'],
            ];
            $sizes[$variant['product_id'].'-'.$variant['size_id']] = [
                'id' => $variant['size_id'],
                'name' => $variant['size_name'],
                'name_long' => $variant['size_name_long'],
                'display_order' => $variant['size_display_order'],
                'product_id' => $variant['product_id'],
                'extra_cost' => $variant['extra_cost']
            ];
            $variantExtraCosts[implode('-', array(
                $variant['product_id'],
                $variant['color_id'],
                $variant['size_id']))] = money_format('%i', $variant['extra_cost']);
        }
        if ($options['withMockups']) {
            $mockups = DB::table('design_variants')
                ->select(
                    'designs.dec_position AS position',
                    'files.location AS mockupImg',
                    'design_variants.color_id',
                    'lines.product_id'
                )
                ->join('files', 'files.id', '=', 'design_variants.mockup_file_id')
                ->join('designs', 'designs.id', '=', 'design_variants.design_id')
                ->join('groups', 'groups.id', '=', 'designs.group_id')
                ->join('lines', 'lines.id', '=', 'design_variants.line_id')
                ->whereCampaignId($campaignId)
                ->get();

            $designedViews = array_unique(array_column($mockups, 'position'));
            $missingViews = array_values(array_diff(['Front', 'Back'], $designedViews));
            if ($missingViews) {
                $viewName = $missingViews[0];
                $photos = DB::table('design_variants')
                    ->select(
                        'views.name AS position',
                        'files.location AS mockupImg',
                        'view_files.color_id',
                        'views.product_id'
                    )
                    ->join('designs', 'designs.id', '=', 'design_variants.design_id')
                    ->join('groups', 'groups.id', '=', 'designs.group_id')
                    ->join('lines', 'lines.id', '=', 'design_variants.line_id')
                    ->join('views', function($join) use($viewName) {
                        $join->on('views.product_id', '=', 'lines.product_id')
                            ->where('views.name', '=', $viewName);
                        })
                    ->join('view_files', function($join) {
                        $join->on('view_files.view_id', '=', 'views.id')
                            ->on('view_files.color_id', '=', 'design_variants.color_id');
                        })
                    ->join('files', 'files.id', '=', 'view_files.file_id')
                    ->whereCampaignId($campaignId)
                    ->get();
            }
        }

        $this->nestProductsData($products, $colors, $sizes, $variants, $mockups, $photos, $upsell);

        return compact('products', 'variantExtraCosts');
    }

    private function nestProductsData(&$products, $colors, $sizes, $variants, $mockups = null, $photos = null, $upsell = null)
    {
        if ($mockups) {
            foreach ($colors as $idx => $color) {
                foreach ($mockups as $mockup) {
                    if ($mockup['color_id'] === $color['id'] &&
                        $mockup['product_id'] === $color['product_id']) {
                        $colors[$idx]['mockups'][] = $mockup;
                    }
                }
            }
            if ($photos) {
                foreach ($colors as $idx => $color) {
                    foreach ($photos as $photo) {
                        if ($photo['color_id'] === $color['id'] &&
                            $photo['product_id'] === $color['product_id']) {
                            $colors[$idx]['mockups'][] = $photo;
                        }
                    }
                }
            }
        }

        foreach ($products as $idx => $product) {
            //replace campaign specific amount on universal upsell amount
            if ($upsell['isUniversalUpsell']) {
                $profit = $product['price'] - $product['baseCost'];
                $discountAmount = ($profit > $upsell['discountAmount'] * 100) ?
                    $upsell['discountAmount'] : 0;
            } else {
                $discountAmount = $product['upsell_discount_amount'] / 100;
            }
            unset($products[$idx]['baseCost']);

            $products[$idx]['price'] = $product['price'] / 100;
            $products[$idx]['upsell_discount_amount'] = $discountAmount;
            foreach ($sizes as $size) {
                if ($size['product_id'] === $product['id']) {
                    $products[$idx]['sizes'][] = $size;
                    $products[$idx]['extraCosts']['size_id'][$size['id']] = $size['extra_cost'] / 100;
                }
            }

            foreach ($colors as $color) {
                if ($color['product_id'] === $product['id']) {
                    $products[$idx]['colors'][] = $color;
                }
            }
        }

    }

    public function checkAndAddLineVariants($campaignId)
    {
        $lines = DB::table('lines')
            ->select(
                'lines.id AS line_id',
                DB::raw('COUNT(DISTINCT lvVariants.id) AS num_variants'),
                DB::raw('COUNT(DISTINCT variants.id) AS num_calc_variants')
            )
            ->join('line_variants', 'line_variants.line_id', '=', 'lines.id')
            ->join('products', 'products.id', '=', 'lines.product_id')
            ->join('variants', 'variants.product_id', '=', 'products.id')
            ->join('variants AS lvVariants', 'lvVariants.id', '=', 'line_variants.variant_id')
            ->join('groups', 'groups.id', '=', 'lines.group_id')
            ->join('campaigns', 'campaigns.id', '=', 'groups.campaign_id')
            ->whereRaw('lvVariants.color_id = variants.color_id')
            ->where('campaigns.ends_at', '>=', Carbon::now())
            ->groupBy('lines.id')
            ->havingRaw('num_variants < num_calc_variants')
            ->where('campaigns.id', $campaignId)
            ->get();

        foreach ($lines as $line) {
            $lineVariants = LineVariant::whereLineId($line['line_id'])
                ->with('variant')
                ->get();
            foreach ($lineVariants as $lv) {
                $variants = Variant::whereColorId($lv->variant->color_id)
                    ->whereProductId($lv->variant->product_id)
                    ->get();
                foreach ($variants as $variant) {
                    $lineVariant = LineVariant::firstOrNew(array(
                        'line_id' => $lv->line_id,
                        'variant_id' => $variant->id
                    ));
                    if (!$lineVariant->id) {
                        $lineVariant->save();
                    }
                }
            }
        }
    }

    public static function getDefaultImgUrl($campaignUrl, $type, $position = null)
    {
        if ($type === 'thumbnail') {
            $query = DB::table('design_variants')
                ->select('files.id', 'files.created_at', 'files.location', 'colors.hex_prim')
                ->join('designs', 'designs.id', '=', 'design_variants.design_id')
                ->join('files', 'files.id', '=', 'designs.file_id')
                ->join('groups', 'groups.id', '=', 'designs.group_id')
                ->join('lines', 'lines.group_id', '=', 'groups.id')
                ->join('campaigns', 'campaigns.id', '=', 'groups.campaign_id')
                ->join('colors', 'colors.id', '=', 'design_variants.color_id')
                ->whereRaw('colors.id = lines.default_color_id')
                ->whereRaw('lines.product_id = campaigns.default_product_id')
                ->where(function($q) use($campaignUrl) {
                    $q->where('campaigns.url', '=', $campaignUrl)
                        ->orWhere('campaigns.old_url', '=', $campaignUrl);
                });
            if ($position) {
                $query->where('designs.dec_position', '=', $position);
            } else {
                $query->whereRaw('designs.dec_position = campaigns.default_view_name');
            }
        } else if ($type === 'mockup') {
            $query = DB::table('design_variants')
                ->select('files.id', 'files.created_at', 'files.location')
                ->join('designs', 'designs.id', '=', 'design_variants.design_id')
                ->join('files', 'files.id', '=', 'design_variants.mockup_file_id')
                ->join('groups', 'groups.id', '=', 'designs.group_id')
                ->join('lines', 'lines.group_id', '=', 'groups.id')
                ->join('campaigns', 'campaigns.id', '=', 'groups.campaign_id')
                ->whereRaw('design_variants.color_id = lines.default_color_id')
                ->whereRaw('design_variants.line_id = lines.id')
                ->whereRaw('lines.product_id = campaigns.default_product_id')
                ->where(function($q) use($campaignUrl) {
                    $q->where('campaigns.url', '=', $campaignUrl)
                        ->orWhere('campaigns.old_url', '=', $campaignUrl);
                });
            if ($position) {
                $query->where('designs.dec_position', '=', $position);
            } else {
                $query->whereRaw('designs.dec_position = campaigns.default_view_name');
            }
        }
        if ($type === 'product' || ($type === 'mockup' && $query->count() < 1)) {
            $query = DB::table('view_files')
                ->select('files.id', 'files.created_at', 'files.location')
                ->join('views', 'views.id', '=', 'view_files.view_id')
                ->join('files', 'files.id', '=', 'view_files.file_id')
                ->join('lines', 'lines.product_id', '=', 'views.product_id')
                ->join('groups', 'groups.id', '=', 'lines.group_id')
                ->join('campaigns', 'campaigns.id', '=', 'groups.campaign_id')
                ->whereRaw('view_files.color_id = lines.default_color_id')
                ->whereRaw('views.product_id = lines.product_id')
                ->whereRaw('lines.product_id = campaigns.default_product_id')
                ->where(function($q) use($campaignUrl) {
                    $q->where('campaigns.url', '=', $campaignUrl)
                        ->orWhere('campaigns.old_url', '=', $campaignUrl);
                });
            if ($position) {
                $query->where('views.name', '=', $position);
            } else {
                $query->whereRaw('views.name = campaigns.default_view_name');
            }
        }
        $file = $query->orderBy('campaigns.id', 'desc')->first();
        if ($type === 'thumbnail' && $file) {
            $file = self::generateThumbnail($file);
        }

        return $file;
    }

    private static function generateThumbnail($file)
    {
        $thumbnailFileName = 'tmp/resized_thumbnail_' . $file['location'];
        $thumbnailUrl = (\MerchPlatform\Config\Main::get('brand')['urls']['userUploads'] . '/' . $fileName);
        $fileUrl = (\MerchPlatform\Config\Main::get('brand')['urls']['userUploads'] . '/' . $file['location']);
        $canvas = new \Imagick();
        if (file_get_contents($thumbnailUrl) === false) {
            $hexCode = $file['hex_prim'];
            $image = new \Imagick();
            $image->readImage($fileUrl);
            $image->trimImage(0);
            if ($image->getImageWidth() >= $image->getImageHeight()) {
                $image->scaleImage(250, 0);
                $compositeX = 20;
                $compositeY = ((290 - $image->getImageHeight()) / 2);
            } else {
                $image->scaleImage(0, 250);
                $compositeX = ((290 - $image->getImageWidth()) / 2);
                $compositeY = 20;
            }
            $bgColor = ($hexCode ? new \ImagickPixel('#'.$hexCode) : 'none');
            $canvas->newImage(290, 290, $bgColor);
            $canvas->setImageFormat('png');
            $canvas->compositeImage(
                $image,
                \Imagick::COMPOSITE_DEFAULT,
                $compositeX,
                $compositeY
            );
            $image->destroy();
        }

        $fileBlob = $canvas->getImageBlob();
        $canvas->destroy();

        return $fileBlob;
    }

    public function saveEstimatedCampaignCosts()
    {
        // Do not proceed if campaign_costs already exist
        if (!$this->campaignCosts->isEmpty()) {
            return false;
        }

        $numUnitsSold = $this->sold;
        $campaignCost = CampaignCost::create(array(
            'campaign_id' => $this->id,
            'name' => 'garments',
            'type' => 'supply',
            'units' => $numUnitsSold,
            'cost' => 0,
            'accuracy' => 'estimate'
        ));

        return $campaignCost;
    }

    public function previewImages(){
        $pics = array();

        foreach($this->group->designs as $design)
        {
            foreach($design->designVariants as $designVariant)
            {
                $pics[] = array('view' => $design->dec_position, 'fileName' => $designVariant->mockupFile->location, 'product_id' => $designVariant->line->product_id, 'color_id' => $designVariant->color_id);
            }
        }
        return $pics;
    }

    public function previewFrontImage()
    {
        $imgs = $this->previewImages();
        if (isset($imgs) && count($imgs) > 0) {
            return $imgs[0]['fileName'];
        }
    }

    public function getMockupImageName($view = null)
    {
        $imgs = $this->previewImages();
        foreach ($imgs as $img) {
            if (is_null($view)) {
                if ($img['view'] === $this->default_view_name && $img['product_id'] === $this->default_product_id) {
                    return $img['fileName'];
                }
            }
        }
    }

    public function getAuthIntegration($customerId)
    {
        $store = Integration::whereNotNull('integration_access_token')->where('customer_id',$customerId)->get();
        return $store;    
    }
}
