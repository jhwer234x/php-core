<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignItemExternal extends Eloquent
{
    protected $fillable = array('external_type', 'campaign_item_id', 'external_id', 'auth_integration_id');
    protected $appends = array();
    protected $hidden = array();

    public function campaignItem()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignItem');
    }

    public function integration()
    {
        return $this->belongsTo('MerchPlatform\Entities\Integration', 'auth_integration_id');
    }

}
