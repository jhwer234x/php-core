<?php namespace MerchPlatform\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;
use MerchPlatform\Entities\Setting;
use MerchPlatform\Entities\Order;
use MerchPlatform\Entities\CampaignOrder;

class CampaignOrder extends Eloquent
{
    protected $fillable = array(
        'order_id',
        'campaign_id',
        'stripe_card_token',
        'price_shipping',
        'price_items',
        'price_tax',
        'price_savings',
        'url_key',
        'status',
        'charge_attempts',
        'parent_id',
        'is_dtg',
        'currency_type',
        'conversion_rate'
    );
    protected $appends = array();
    protected $hidden = array();

    public function getDiscountCodeSavings()
    {
        $totalSavings = 0;
        foreach ($this->discountCodes as $discountCode) {
            if ($discountCode->free_shipping) {
                $totalSavings += $this->price_shipping;
            } elseif ($discountCode->discount_usd) {
                $totalSavings += $discountCode->discount_usd;
            } elseif ($discountCode->discount_pct) {
                $amount = $discountCode->discount_pct * $this->price_items;
                $totalSavings += $amount;
            }
        }
        $totalSavings = $totalSavings / 100;

        return $totalSavings;
    }

    public function getTotalAfterSavings()
    {
        return $this->price_total - $this->getDiscountCodeSavings();
    }

    public function getPriceTotalAttribute()
    {
        return $this->price_items + $this->price_shipping + $this->price_tax;
    }

    public function getPriceItemsAttribute($value)
    {
        return $value / 100;
    }

    public function getPriceSavingsAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceItemsAttribute($value)
    {
        $this->attributes['price_items'] = $value * 100;
    }

    public function setPriceSavingsAttribute($value)
    {
        $this->attributes['price_savings'] = $value * 100;
    }

    public function getPriceShippingAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceShippingAttribute($value)
    {
        $this->attributes['price_shipping'] = $value * 100;
    }

    public function getPriceTaxAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceTaxAttribute($value)
    {
        $this->attributes['price_tax'] = $value * 100;
    }

    public function campaignOrderExternals()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignOrderExternal');
    }

    public function orderReturn()
    {
        return $this->hasOne('MerchPlatform\Entities\OrderReturn');
    }

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order');
    }

    public function parent()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder', 'parent_id');
    }

    public function childCampaignOrders()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignOrder', 'parent_id');
    }

    public function campaignItems()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignItem');
    }

    public function packages()
    {
        return $this->hasMany('MerchPlatform\Entities\Package');
    }

    public function discountCodes()
    {
        return $this->belongsToMany('MerchPlatform\Entities\DiscountCode', 'discount_code_campaign_orders');
    }

    /*
     * If we have package that was delivered, return est_delivery_at
     * If we have package with estimated_delivery_at that are not delivered, and dates are already due, return estimated_delivery_at with 3 days increments
     * (e.g. estimated_delivery_at is Feb12, today is Feb20, it will return Feb12+3+3+3 = Feb 21)
     * If no package was created, return campaign end date + 12 days(with 3 days increments if already due)
    */
    public function soothing_estmated_delivery_date()
    {
        $est_delivery = $range = null;
        $delayed = false;
        $package = $this->packages()->whereNotNull('estimated_delivery_at')->first();
        $estimated_shipping_days = Setting::where('key', 'minimum_estimated_domestic_shipping_days')
            ->orWhere('key', 'maximum_estimated_domestic_shipping_days')
            ->orWhere('key', 'minimum_estimated_international_shipping_days')
            ->orWhere('key', 'maximum_estimated_international_shipping_days')
            ->lists('value','key');
        $fromOptimistic = $estimated_shipping_days['minimum_estimated_domestic_shipping_days'];
        $fromPessimistic = $estimated_shipping_days['maximum_estimated_domestic_shipping_days'];
        if ($this->order->address->country_id !== 233) {
            $fromOptimistic = $estimated_shipping_days['minimum_estimated_international_shipping_days'];
            $fromPessimistic = $estimated_shipping_days['maximum_estimated_international_shipping_days'];
        }
        if ($package) {
            $est_delivery = new Carbon($package->estimated_delivery_at);
            if ($package->tracking_status == 'delivered') {
                return (object)array('date' => $est_delivery, 'delay' => false);
            }
        } else {
            $est_delivery = $this->campaign->ends_at->addWeekDays($fromPessimistic);
        }
        while ($est_delivery->lt(Carbon::now())) {
            $delayed = true;
            $est_delivery = $est_delivery->addWeekDays(3);
        }
        if (!$delayed) {
            $range = array(
                'from' => $this->campaign->ends_at->addWeekDays($fromOptimistic),
                'to' => $this->campaign->ends_at->addWeekDays($fromPessimistic)
            );
        }
        return (object)array(
            'date' => $est_delivery,
            'delay' => $delayed,
            'range' => $range
        );
    }

    public function addresses()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\Order');
    }

    public function getOrderNumAttribute()
    {
        return 'M-'.str_pad($this->id, 4, 0, STR_PAD_LEFT);
    }

    public function getOrderTypeAttribute()
    {
        return 'Campaign Purchase';
    }

    public function getItemsStringAttribute()
    {
        $this->theString = '';
        $this->campaignItems->each(function($item){
            $this->theString .=  $item->variant->product->name.' '.$item->variant->size->name. ' '.$item->variant->color->name. ' x '.$item->qty.'<br><br>';
        });

        return $this->theString;
    }

    public function getHasPaidLabelsAttribute()
    {
        $this->returnVal = false;

        $this->packages->each(function($package){
            if($package->status == 'paid') {
                $this->returnVal = true;
            }
        });

        return $this->returnVal;
    }

    public function createWithOrder($campaignItems, $customer, $prices, $currency)
    {
        $order = Order::create([
            'buyer_id' => $customer->id,
            'seller_id' => $campaignItems->first()->campaign->customer_id,
            'url_key' => uniqid(),
        ]);

        $campaignOrder = CampaignOrder::create(array(
            'order_id' => $order->id,
            'campaign_id' => $campaignItems->first()->campaign_id,
            'price_shipping' => $prices['shipping'], // warning, you better not rely on this price values. primary information about prices are stored in individual campaignItems.
            'price_items' => $prices['items'],
            'price_tax' => $prices['tax'],
            'price_savings' => isset($prices['savings']) ? $prices['savings'] : 0,
            'url_key' => uniqid('mo_', true)
        ));

        $order->buyer_cost = $campaignOrder->getTotalAfterSavings();
        $order->save();

        if (isset($currency)) {
            $campaignOrder->currency_type = $currency['currency_type'];
            $campaignOrder->conversion_rate = $currency['conversion_rate'];

            $campaignOrder->save();
        }

        $campaignItems->each(function($campaignItem) use($campaignOrder) {
            $campaignItem->cart_id = null;
            $campaignItem->campaign_order_id = $campaignOrder->id;
            $campaignItem->save();
        });

        return $campaignOrder;

    }

    public function recalculatePrices()
    {
        $this->load('campaignItems.variant', 'campaign.group.lines');

        $shipping = 0; $items = 0;
        foreach($this->campaignItems as $campaignItem)
        {
            if($shipping == 0) {
                $shipping = 5.00;
            } else {
                $shipping += 1.00;
            }

            foreach($this->campaign->group->lines as $line) {
                if($campaignItem->variant->product_id == $line->product_id) {
                    $items += $line->campaign_price*$campaignItem->qty;
                }
            }

        }

        $this->price_items = $items;
        $this->price_shipping = $shipping;

        $this->save();
    }

}
