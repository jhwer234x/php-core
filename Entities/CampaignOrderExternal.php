<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignOrderExternal extends Eloquent
{
    protected $fillable = array('external_type', 'campaign_order_id', 'external_id', 'auth_integration_id');
    protected $appends = array();
    protected $hidden = array();

    public function campaignOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder');
    }

    public function integration()
    {
        return $this->belongsTo('MerchPlatform\Entities\Integration', 'auth_integration_id');
    }

}
