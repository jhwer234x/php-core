<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Cisession extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();

    public function cart()
    {
        return $this->hasOne('MerchPlatform\Entities\Cart', 'session_id');
    }
}