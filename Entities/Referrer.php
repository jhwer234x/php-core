<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Referrer extends Eloquent
{
    protected $fillable = array(
        'campaign_id', 'customer_id', 'cisession_id', 'url', 'campaign', 'ip_address', 'country_id', 'region', 'city', 'user_agent',
        'os', 'os_version', 'browser', 'browser_version', 'is_desktop', 'is_mobile', 'is_tablet'
    );

    public function scopeRange($query, $ranges)
    {
        return $query->whereBetween('created_at', $ranges);
    }

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function campaignOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder');
    }

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

}