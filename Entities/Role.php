<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Role extends Eloquent
{
    protected $fillable = [
        'name',
        'permissions',
    ];

    public function admins()
    {
        return $this->hasMany('MerchPlatform\Entities\Admin');
    }
}
