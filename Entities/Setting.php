<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Setting extends Eloquent
{
    public $timestamps = false;
    protected $fillable = array('key', 'value');

    public function scopeKey($query, $key, $default = 'false')
    {
        try {
            $result = $query->whereKey($key)->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return $default;
        }
        $arr = $result->toArray();
        return $arr['value'];
    }

}