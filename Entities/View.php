<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class View extends Eloquent
{
    protected $fillable = array('product_id', 'is_default', 'name', 'x_axis_1', 'x_axis_2', 'y_axis_1', 'y_axis_2', 'width_in', 'height_in', 'display_order', 'height_px', 'width_px', 'top_px', 'left_px', 'is_mirrored', 'config_file_id');
    protected $appends = array();
    protected $hidden = array();

    public function product()
    {
        return $this->belongsTo('MerchPlatform\Entities\Product');
    }

    public function file()
    {
        return $this->hasOne('MerchPlatform\Entities\File', 'id', 'config_file_id');
    }

    public function viewFiles()
    {
        return $this->hasMany('MerchPlatform\Entities\ViewFile');
    }
}