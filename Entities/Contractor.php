<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Contractor extends Eloquent
{
    protected $fillable = array(
        'address_id',
        'name',
        'phone_num',
        'emails',
        'settings',
    );
    protected $appends = array();
    protected $hidden = array();

    public function address()
    {
        return $this->belongsTo('MerchPlatform\Entities\Address');
    }

    public function contractorUsers()
    {
        return $this->hasMany('MerchPlatform\Entities\ContractorUser');
    }

    public function jobs()
    {
        return $this->hasMany('MerchPlatform\Entities\Job');
    }

    public function supplierOrders()
    {
        return $this->hasMany('MerchPlatform\Entities\SupplierOrder');
    }

    public function payouts()
    {
        return $this->hasMany('MerchPlatform\Entities\Payout');
    }

    public static function getSupplierPOsDataByContractorIdAndRange($startDate, $endDate, $supplierId)
    {
        $orderedOrders = \MerchPlatform\Entities\SupplierOrder::with('warehouse', 'items.variant', 'contractor')
            ->whereBetween('created_at', array($startDate, $endDate))->whereStatus('ordered');

        if ($supplierId) {
            $orderedOrders = $orderedOrders->whereContractorId($supplierId);
        };
        $orderedOrders = $orderedOrders->get();
        self::formatSupplierPOsData($orderedOrders);
        $orderedOrdersSums = self::calculateSupplierPOsSums($orderedOrders);
        // we dont show contractor breakdown if there is a specific contractor selected
        if (!$supplierId){
            $orderedOrdersContractorSums = self::calculateSupplierPOsContractorSums($orderedOrders);
        }
        return compact(array('orderedOrders','orderedOrdersSums', 'orderedOrdersContractorSums'));
    }

    private static function calculateSupplierPOsContractorSums(&$orderedOrders){
        $grouped = $orderedOrders->groupBy('contractor_id');
        $res = $grouped->map(function($orders, $contractor_id){
            return array('contractor'=>$orders[0]->contractor, 'breakdown'=>self::calculateSupplierPOsSums($orders));
        });
        return $res;
    }

    private static function calculateSupplierPOsSums(&$orderedOrders)
    {
        $sums = array(
            'styleColors' => array(),
            'total' => array(
                'not_arrived' => 0,
                'arrived' => 0,
                'issue' => 0
            )
        );
        foreach ($orderedOrders as $so) {
            foreach ($so->formattedBlanks['styleColors'] as $gtin => $gtinColors) {
                if (!isset($sums['styleColors'][$gtin])) {
                    $sums['styleColors'][$gtin] = array();
                }
                foreach ($gtinColors as $gtinColorName => $gtinColorNumbers) {
                    if (!isset($sums['styleColors'][$gtin][$gtinColorName])) {
                        $sums['styleColors'][$gtin][$gtinColorName] = array(
                            'sizes' => array(
                                'numberBySizes' => array(),
                                'total' => array(
                                    'not_arrived' => 0,
                                    'arrived' => 0,
                                    'issue' => 0
                                )
                            )
                        );
                    }
                    $sums['styleColors'][$gtin][$gtinColorName]['sizes']['total']['not_arrived'] +=  $gtinColorNumbers['sizes']['total']['not_arrived'];
                    $sums['styleColors'][$gtin][$gtinColorName]['sizes']['total']['arrived'] +=  $gtinColorNumbers['sizes']['total']['arrived'];
                    $sums['styleColors'][$gtin][$gtinColorName]['sizes']['total']['issue'] += $gtinColorNumbers['sizes']['total']['issue'];

                    $sums['total']['not_arrived'] += $gtinColorNumbers['sizes']['total']['not_arrived'];
                    $sums['total']['arrived'] += $gtinColorNumbers['sizes']['total']['arrived'];
                    $sums['total']['issue'] += $gtinColorNumbers['sizes']['total']['issue'];

                    foreach ($gtinColorNumbers['sizes']['numberBySizes'] as $gtinColorSize => $gtinColorSizeNumbers) {
                        if (!isset($sums['styleColors'][$gtin][$gtinColorName]['sizes']['numberBySizes'][$gtinColorSize])) {
                            $sums['styleColors'][$gtin][$gtinColorName]['sizes']['numberBySizes'][$gtinColorSize] = array(
                                'not_arrived' => 0,
                                'arrived' => 0,
                                'issue' => 0
                            );
                        }

                        $sums['styleColors'][$gtin][$gtinColorName]['sizes']['numberBySizes'][$gtinColorSize]['not_arrived'] += $gtinColorSizeNumbers['not_arrived'];
                        $sums['styleColors'][$gtin][$gtinColorName]['sizes']['numberBySizes'][$gtinColorSize]['arrived'] += $gtinColorSizeNumbers['arrived'];
                        $sums['styleColors'][$gtin][$gtinColorName]['sizes']['numberBySizes'][$gtinColorSize]['issue'] += $gtinColorSizeNumbers['issue'];

                    }
                }
            }
        }
        return $sums;

    }


    private static function formatSupplierPOsData(&$orderedOrders)
    {
        foreach ($orderedOrders as $so) {
            $gtinQtysOrdered = array();
            foreach ($so->items as $i) {
                if (!isset($gtinQtysOrdered[$i->variant->gtin])) {
                    $gtinQtysOrdered[$i->variant->gtin] = array('not_arrived' => 0, 'arrived' => 0, 'issue' => 0);
                }
                $gtinQtysOrdered[$i->variant->gtin][$i->status] += 1;
            }

            if ($gtinQtysOrdered) {
                $formattedBlanks = \MerchPlatform\Entities\Variant::gtinQtysToAdvancedFormat($gtinQtysOrdered);
            }

            $so->formattedBlanks = $formattedBlanks;
        }
    }

    public function getSupplierPOsDataByDate($date, $byCreatedAtOrArrivesAt = 'arrives_at')
    {
        $orderedOrders = \MerchPlatform\Entities\SupplierOrder::with('warehouse', 'items.variant', 'contractor')
            ->where($byCreatedAtOrArrivesAt, 'like', '%'.$date->format('Y-m-d').'%')
            ->whereContractorId($this->id)
            ->whereStatus('ordered')
            ->get();
        $supplierOrdersRemainder = \MerchPlatform\Entities\SupplierOrder::with('items.variant')
            ->where($byCreatedAtOrArrivesAt, 'like', '%'.$date->format('Y-m-d').'%')
            ->whereContractorId($this->id)
            ->whereStatus('failed')
            ->get();

        foreach ($orderedOrders as $po) {
            $jobNums = array();
            foreach ($po->items as $item) {
                if ($item->job_id) {
                    $jobNums[$item->job_id] = $item->job_id;
                }
            }
            $po->relatedJobNums = $jobNums;
        }

        self::formatSupplierPOsData($orderedOrders);

        $gtinQtysRemainder = array();
        foreach($supplierOrdersRemainder as $so)
        {
            foreach($so->items as $i)
            {
                if(!isset($gtinQtysRemainder[$i->variant->gtin])) {
                    $gtinQtysRemainder[$i->variant->gtin] = array('not_arrived' => 0, 'arrived' => 0, 'issue' => 0);
                }
                $gtinQtysRemainder[$i->variant->gtin]['not_arrived'] += 1;
            }
        }

        if($gtinQtysRemainder) {
            $remainder = \MerchPlatform\Entities\Variant::gtinQtysToAdvancedFormat($gtinQtysRemainder);
        }

        return compact('orderedOrders', 'remainder');
    }

    public function getJobsPOBreakdown($date, $byCreatedAtOrArrivesAt = 'arrives_at')
    {
        // This eloquent call is for getting data in "By Job" format
        $jobs = \MerchPlatform\Entities\Job::whereContractorId($this->id)
            ->with(array(
                'order.campaign',
                'supplierOrderItems' => function($q) use($date, $byCreatedAtOrArrivesAt) {
                    $q->with(array(
                        'variant.product',
                        'variant.size',
                        'variant.color',
                        'supplierOrder.warehouse'
                    ))->whereHas('supplierOrder', function($soq) use($date, $byCreatedAtOrArrivesAt) {
                        $soq->where($byCreatedAtOrArrivesAt, 'like', '%'.$date->format('Y-m-d').'%');
                    });
                },
            ))
            ->has('supplierOrderItems', '>', '0')
            ->whereHas('supplierOrderItems', function($q) use($date, $byCreatedAtOrArrivesAt) {
                $q->whereHas('supplierOrder', function($qo) use($date, $byCreatedAtOrArrivesAt) {
                    $qo->where($byCreatedAtOrArrivesAt, 'like', '%'.$date->format('Y-m-d').'%');
                });
            })
            ->get();

        return $this->formatBlanksByJob($jobs);
    }

    private function formatBlanksByJob($jobs)
    {
        $dataByJob = array();

        foreach($jobs as $job)
        {

            $dataByJob[$job->job_num]['admin_status'] = $job->order->campaign->admin_status;
            $dataByJob[$job->job_num]['total_units'] = 0;
            $items = array();

            foreach($job->supplierOrderItems as $item)
            {
                $arrayKey = implode(',', array(
                    $item->supplierOrder->id, $item->variant->product->id, $item->variant->color->id
                ));
                if(!isset($items[$arrayKey]))
                {
                    if ($item->supplierOrder->status !== 'ordered') {
                        continue;
                    }
                    $poNumString = ($item->supplierOrder->internal_order_num . ' - ' . $item->supplierOrder->warehouse->name);
                    $items[$arrayKey] = array(
                        'po' => $poNumString,
                        'style' => $item->variant->product->code,
                        'color' => $item->variant->color->name,
                        'sizes' => array_fill(0, 10, 0)
                    );
                }

                // Add sizes data
                switch ($item->variant->size->name) {
                    case 'XS':
                        $items[$arrayKey]['sizes'][0]++;
                        break;
                    case 'S':
                        $items[$arrayKey]['sizes'][1]++;
                        break;
                    case 'M':
                        $items[$arrayKey]['sizes'][2]++;
                        break;
                    case 'L':
                        $items[$arrayKey]['sizes'][3]++;
                        break;
                    case 'XL':
                        $items[$arrayKey]['sizes'][4]++;
                        break;
                    case '2XL':
                        $items[$arrayKey]['sizes'][5]++;
                        break;
                    case '3XL':
                        $items[$arrayKey]['sizes'][6]++;
                        break;
                    case '4XL':
                        $items[$arrayKey]['sizes'][7]++;
                        break;
                    case '5XL':
                        $items[$arrayKey]['sizes'][8]++;
                        break;
                    case '6XL':
                        $items[$arrayKey]['sizes'][9]++;
                        break;
                }

                // Increment total pieces for this style color pair in this purchase order by 1
                $items[$arrayKey]['sizes'][10]++;

                // Increment total pieces for this purchase order by 1
                $dataByJob[$job->job_num]['total_units']++;
            }

            $dataByJob[$job->job_num]['items'] = $items;
        }

        return $dataByJob;
    }

    public function numJobsInReceiving()
    {
        $count = Job::whereContractorId($this->id)
            ->inReceiving()
            ->count();

        return $count;
    }

    public function numPiecesInReceiving()
    {
        $count = DB::table('supplier_order_items')
            ->join('jobs', 'jobs.id', '=', 'supplier_order_items.job_id')
            ->join('supplier_orders', 'supplier_orders.id', '=', 'supplier_order_items.supplier_order_id')
            ->where('jobs.contractor_id', '=', $this->id)
            ->where('supplier_order_items.status', '=', 'not_arrived')
            ->where('supplier_orders.status', '=', 'ordered')
            ->where('jobs.has_blanks_arrived', '=', '0')
            ->where('jobs.job_status', '=', 'Open')
            ->where('jobs.fulfillment_status', '=', 'Open')
            ->count();

        return $count;
    }

    public function numJobsInProduction()
    {
        $count = Job::whereContractorId($this->id)
            ->inProduction()
            ->count();

        return $count;
    }

    public function numPiecesInProduction()
    {
        $count = DB::table('supplier_order_items')
            ->join('jobs', 'jobs.id', '=', 'supplier_order_items.job_id')
            ->where('jobs.contractor_id', '=', $this->id)
            ->where('supplier_order_items.status', '=', 'arrived')
            ->where('jobs.has_blanks_arrived', '=', '1')
            ->where('jobs.job_status', '=', 'Open')
            ->where('jobs.fulfillment_status', '=', 'Open')
            ->count();

        return $count;
    }

    public function numJobsInFulfillment()
    {
        $count = Job::whereContractorId($this->id)
            ->inFulfillment()
            ->count();

        return $count;
    }

    public function numPiecesInFulfillment()
    {
        $count = DB::table('supplier_order_items')
            ->join('jobs', 'jobs.id', '=', 'supplier_order_items.job_id')
            ->where('jobs.contractor_id', '=', $this->id)
            ->where('supplier_order_items.status', '=', 'arrived')
            ->where('jobs.has_blanks_arrived', '=', '1')
            ->where('jobs.job_status', '=', 'Completed')
            ->where('jobs.fulfillment_status', '=', 'Open')
            ->count();

        return $count;
    }

    public function numJobsOpen()
    {
        $count = Job::whereContractorId($this->id)
            ->where(function($q) {
                $q->where('has_blanks_arrived', '=', '0')
                    ->orWhere('job_status', '=', 'Open')
                    ->orWhere('fulfillment_status', '=', 'Open');
            })
            ->count();

        return $count;
    }

    public function numPiecesOpen()
    {
        $count = DB::table('supplier_order_items')
            ->join('jobs', 'jobs.id', '=', 'supplier_order_items.job_id')
            ->join('supplier_orders', 'supplier_orders.id', '=', 'supplier_order_items.supplier_order_id')
            ->where('jobs.contractor_id', '=', $this->id)
            ->where('supplier_order_items.status', '!=', 'issue')
            ->where(function($q) {
                $q->where('jobs.has_blanks_arrived', '=', '0')
                    ->orWhere('jobs.job_status', '=', 'Open')
                    ->orWhere('jobs.fulfillment_status', '=', 'Open');
            })
            ->count();

        return $count;
    }

    public function getFulfillCostPackageAttribute($value)
    {
        return $value / 100;
    }

    public function setFulfillCostPackageAttribute($value)
    {
        $this->attributes['fulfill_cost_package'] = ($value * 100);
    }

    public function getFulfillCostUnitAttribute($value)
    {
        return $value / 100;
    }

    public function setFulfillCostUnitAttribute($value)
    {
        $this->attributes['fulfill_cost_unit'] = ($value * 100);
    }
    public function jobsInFulfillment()
    {
        Job::whereContractorId($this->id)
            ->inFulfillment()->get();
    }
}
