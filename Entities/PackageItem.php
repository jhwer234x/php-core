<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PackageItem extends Eloquent
{
    protected $fillable = array('qty', 'campaign_item_id', 'package_id', 'line_variant_id', 'weight_oz', 'weight_lb');
    protected $appends = array();
    protected $hidden = array();

    public function package()
    {
        return $this->belongsTo('MerchPlatform\Entities\Package');
    }

    public function campaignItem()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignItem');
    }

    public function lineVariant()
    {
        return $this->belongsTo('MerchPlatform\Entities\LineVariant');
    }

}