<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Design extends Eloquent
{
    protected $fillable = array('dec_position', 'num_colors', 'top_in', 'left_in', 'width_in', 'height_in', 'dec_method', 'file_id');
    protected $appends = array();
    protected $hidden = array();
    
    public function group()
    {
        return $this->belongsTo('MerchPlatform\Entities\Group');
    }

    public function designVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignVariant');
    }

    public function designPositions()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignPosition');
    }

    public function designObjects()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignObject');
    }

    public function decColors()
    {
        return $this->belongsToMany('MerchPlatform\Entities\DecColor', 'dec_color_design', 'design_id', 'dec_color_id');
    }

    public function file()
    {
        return $this->belongsTo('MerchPlatform\Entities\File');
    }

    public function deleteCascade()
    {
        $this->decColors()->detach();
        
        // Delete all child designs and their children
        $this->designObjects->each(function($designObject){
            $designObject->decColors()->detach();
            $designObject->delete();
        });

        // Delete all child designs and their children
        $this->designVariants->each(function($designVariant){
            $designVariant->decColors()->detach();
            $designVariant->delete();
        });

        $this->delete();
    }

}
