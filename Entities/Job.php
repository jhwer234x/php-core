<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use MerchPlatform\Pricing\Quote;

class Job extends Eloquent
{
    protected $fillable = [
        'contractor_id',
        'order_id',
        'ref_num',
    ];

    protected $appends = array();

    // Job's parent relationships

    public function contractor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Contractor');
    }

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order');
    }

    public function supplierOrderItems()
    {
        return $this->hasMany('MerchPlatform\Entities\SupplierOrderItem');
    }

    // Job's child relationships

    public function groups()
    {
        return $this->hasMany('MerchPlatform\Entities\Group');
    }

    public function payoutItem()
    {
        return $this->hasOne('MerchPlatform\Entities\PayoutItem');
    }

    public function getJobNumAttribute()
    {
        return str_pad($this->id, 4, '0', STR_PAD_LEFT);
    }

    public function scopeFulfillment($query, $status = null)
    {
        if($status)
        {
            $query->whereFulfillmentStatus($status);
        }

        return $query->whereNotNull('fulfillment_status');
    }

    public function scopeContractor($query, $contractorId)
    {
        return $query->whereContractorId($contractorId);
    }

    public function getOldJobNumAttribute()
    {
        return $this->attributes['job_num'];
    }

    public function getDates()
    {
        return array('shippingdue_at', 'created_at', 'updated_at');
    }

    // Sets custom "job_status_label" attribute (bootstrap label color class name).. Based on the Order's 'prod_status' value
    public function getJobStatusLabelAttribute()
    {
        if($this->attributes['job_status'] == 'Open')
        {
            return 'default';
        }
        elseif($this->attributes['job_status'] == 'Issue')
        {
            return 'danger';
        }
        elseif($this->attributes['job_status'] == 'Supply Arrived')
        {
            return 'info';
        }
        elseif($this->attributes['job_status'] == 'Completed')
        {
            return 'success';
        }

        return 'default';
    }

    // Sets custom "fulfillment_status_label" attribute (bootstrap label color class name).. Based on the Order's 'prod_status' value
    public function getFulfillmentStatusLabelAttribute()
    {
        if($this->attributes['fulfillment_status'] == 'Open')
        {
            return 'default';
        }
        elseif($this->attributes['fulfillment_status'] == 'Issue')
        {
            return 'danger';
        }
        elseif($this->attributes['fulfillment_status'] == 'Completed')
        {
            return 'success';
        }

        return 'default';
    }

    public function getPayoutDataAttribute()
    {
        $this->payoutData['imprint'] = array();
        $this->payoutData['supplies'] = array();
        $this->payoutData['shipping'] = array();
        $this->payoutData['other'] = array();

        $payoutDetails = json_decode($this->payout_details);
        foreach($payoutDetails as $detail) {
            if($detail->type == 'group') {
                foreach($detail->costs->decorations as $cost) {
                    foreach($cost as $singleCost) {
                        if($singleCost->type == 'Screen Charge') {
                            $this->payoutData['supplies']['screen']['name'] = $singleCost->type;
                            $this->payoutData['supplies']['screen']['description'] = $singleCost->type;
                            $this->payoutData['supplies']['screen']['num_units'] += $singleCost->numUnits;
                            $this->payoutData['supplies']['screen']['per_unit'] = money_format('%i', $singleCost->costPer);
                            $this->payoutData['supplies']['screen']['total'] += $singleCost->total;
                        } elseif($singleCost->type == 'Light Product Print' || $singleCost->type == 'Dark Product Print') {
                            array_push($this->payoutData['imprint'], array('name' => $singleCost->type, 'description' => $singleCost->type.' '.$singleCost->numColors.' colors '.$cost->printLocation. ' ('.$cost->type.')', 'num_units' => $singleCost->numUnits, 'per_unit' => money_format('%i', $singleCost->costPer), 'total' => money_format('%i', $singleCost->total)));
                        }
                    }
                }
            }
            elseif($detail->type == 'ship') {
                $dropshipCharge = 0;
                foreach($detail->costs->shippingHandling as $cost){
                    if(strpos($cost->type, 'Dropship') !== false) {
                        $dropshipCharge += $cost->total;
                        // exit('wtf');
                    } else {

                        array_push($this->payoutData['shipping'], array('name' => $cost->type, 'description' => $cost->type, 'num_units' => 1, 'per_unit' => money_format('%i', $cost->total), 'total' => money_format('%i', $cost->total)));

                    }
                }
                if($dropshipCharge > 0) {
                    array_push($this->payoutData['shipping'], array('name' => 'Dropship Charge', 'description' => 'Dropship Charge', 'num_units' => 1, 'per_unit' => money_format('%i', $dropshipCharge), 'total' => money_format('%i', $dropshipCharge)));
                }
                
            }
        }

        $this->payoutData['calculatedTotal'] = 0.00;
        foreach($this->payoutData as $datum) {
            foreach($datum as $cost) {
                $this->payoutData['calculatedTotal'] += $cost['total'];
            }
        }

        $this->payoutData['total'] = money_format('%i', $this->order->payout_contractor);
        $this->payoutData['remainder'] = $this->payoutData['total']-$this->payoutData['calculatedTotal'];
        if($this->payoutData['remainder'] > 0) {
            $this->payoutData['other']['misc'] = array('name' => 'Misc', 'description' => 'Miscellaneous Fees', 'num_units' => 1, 'per_unit' => $this->payoutData['remainder'], 'total' => $this->payoutData['remainder']);
        }
        
        return $this->payoutData;
    }

    public function getPayoutDetails()
    {
        $this->load('order.groups.lines.lineVariants.variant.color');
        $this->load('order.groups.lines.lineVariants.variant.product');
        $this->load('order.campaign.campaignOrders.packages');
        
        // Get $originalCampaign, because if this is a relaunch, the original Campaign's designs data must be used
        $originalCampaignId = ($this->order->campaign->original_id ?: $this->order->campaign->id);
        $originalCampaign = \MerchPlatform\Entities\Campaign::with('group.designs.designVariants.decColors')->find($originalCampaignId);
        foreach($originalCampaign->group->designs as $design) {
            $design->actual_num_colors = $design->designVariants->first()->decColors->count();
        }

        // Build $quoteGroups (used to get Quote)
        $quoteLines = $quoteDesigns = $quoteGroups = array();
        foreach($this->order->groups->first()->lines as $line) {
            $quoteVariants = array();
            foreach($line->lineVariants as $lineVariant)
            {
                if($lineVariant->qty < 1) {
                    continue;
                }

                $quoteVariants[] = array(
                    "color_id" => $lineVariant->variant->color_id,
                    "size_id" => $lineVariant->variant->size_id,
                    "qty" => $lineVariant->qty,
                    "light_dark" => $lineVariant->variant->color->light_dark_prim,
                    "min_supply_cost" => $lineVariant->variant->min_cost*100,
                    "max_supply_cost" => $lineVariant->variant->max_cost*100,
                    "item_key" => $lineVariant->variant->product->item_key
                );
            }

            if($quoteVariants) {
                $quoteLines[] = array('product_id' => $line->product_id, 'variants' => $quoteVariants);
            }
        }
        foreach($originalCampaign->group->designs as $design) {
            $quoteDesigns[] = array(
                'num_colors' => $design->actual_num_colors,
                'position' => $design->dec_position
            );
        }
        $quoteGroups[] = array(
            'lines' => $quoteLines,
            'designs' => $quoteDesigns
        );

        // Build $numPackages (used to get Quote)
        $numPackages = 0;
        foreach($this->order->campaign->campaignOrders as $campaignOrder) {
            if($campaignOrder->status == 'paid') {
                $numPackages += $campaignOrder->packages->count();
            }
        }

        $quote = new Quote($quoteGroups, $options = array('num_packages' => $numPackages));
        $quote->calculateExactQuote();

        return $quote->getCostsLog(array('printer' => true, 'remove_empties' => true));
    }

    public function getPayoutDetailsAttribute($value)
    {
        if(!$value) {
            return false;
        }
        $details = json_decode($value);

        $screen_charges = array('name' => 'Screen Charge', 'cost' => 0, 'units' => 0, 'total' => 0); 
        $imprints = array(); $additional_costs = array();
        $fulfillment_costs = array(); $print_additionals = array();
        foreach($details->costs as $group) {
            if(isset($group->fulfillment)) {
                $fulfillment_costs = array('name' => ucfirst($group->fulfillment->key), 'cost' => $group->fulfillment->cost, 'units' => $group->fulfillment->units, 'total' => $group->fulfillment->total);
            }
            if(isset($group->print_additionals)) {
                foreach($group->print_additionals as $print_additional) {
                    $nameParts = explode('_', $print_additional->key);
                    $name = '';
                    foreach($nameParts as $part) {
                        $name .= ucfirst($part.' ');
                    }
                    $additional_costs[] = array('name' => trim($name), 'cost' => $print_additional->cost, 'units' => $print_additional->units, 'total' => $print_additional->total);
                }
            }
            if(isset($group->decorations)) {
                foreach($group->decorations as $dec) {
                    foreach($dec->additionals as $cost) {
                        if(!isset($additional_costs[$cost->key])) {
                            $additional_costs[$cost->key] = array('name' => $cost->key, 'cost' => $cost->cost, 'units' => $cost->units, 'total' => $cost->total);
                            continue;
                        }

                        $additional_costs[$cost->key]['units'] += $cost->units;
                        $additional_costs[$cost->key]['total'] += $cost->cost;
                    }

                    foreach($dec->imprints as $cost) {
                        $name = 'Dark Imprint';
                        if($cost->key == 'imprints_light') {
                            $name = 'Light Imprint';
                        }

                        $imprints[] = array('name' => $name, 'cost' => $cost->cost, 'units' => $cost->units, 'total' => $cost->total);
                    }

                    if(isset($dec->screen_charges)) {
                        $screen_charges['cost'] = $dec->screen_charges->cost;
                        $screen_charges['units'] += $dec->screen_charges->units;
                        $screen_charges['total'] += $dec->screen_charges->total;
                    }
                }
            }
            
        }

        $details->costs = array();
        $details->costs[] = $screen_charges;
        foreach($imprints as $imprint) {
            $details->costs[] = $imprint;
        }
        if(!empty($additional_costs)) { 
            foreach($additional_costs as $additional_cost) {
                $details->costs[] = $additional_cost;
            }
        }
        if(!empty($fulfillment_costs)) { $details->costs[] = $fulfillment_costs; }
        return $details;
    }

    public function scopeInReceiving($query)
    {
        return $query->whereHasBlanksArrived('0')
            ->whereJobStatus('Open')
            ->whereFulfillmentStatus('Open');
    }

    public function scopeInProduction($query)
    {
        return $query->whereHasBlanksArrived('1')
            ->whereJobStatus('Open')
            ->whereFulfillmentStatus('Open');
    }

    public function scopeInFulfillment($query)
    {
        return $query->whereHasBlanksArrived('1')
            ->whereJobStatus('Completed')
            ->whereFulfillmentStatus('Open');
    }
}
