<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ViewFile extends Eloquent
{
    protected $fillable = array('color_id', 'view_id', 'file_id', 'type');

    public function color()
    {
        return $this->belongsTo('MerchPlatform\Entities\Color');
    }

    public function view()
    {
        return $this->belongsTo('MerchPlatform\Entities\View');
    }

    public function file()
    {
        return $this->belongsTo('MerchPlatform\Entities\File');
    }
}