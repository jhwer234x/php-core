<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PackageBatch extends Eloquent
{
    protected $fillable = array('batch_id', 'scan_form_url', 'carrier', 'contractor_id');
    protected $appends = array();
    protected $hidden = array();

    public function packages()
    {
        return $this->hasMany('MerchPlatform\Entities\Package');
    }
}