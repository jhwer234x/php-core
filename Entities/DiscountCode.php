<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon as Carbon;

class DiscountCode extends Eloquent
{
    protected $fillable = array(
        'customer_id', 'code', 'limit', 'uses',
        'discount_pct', 'discount_usd', 'is_active',
        'expires_at', 'free_shipping', 'for_all_campaigns'
    );
    protected $appends = array();
    protected $hidden = array();

    public $dates = array('created_at', 'updated_at', 'expires_at');

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function campaigns()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Campaign', 'discount_code_campaigns');
    }

    public function getCampaigns()
    {
        return $this->campaigns;
    }

    public function campaignOrders()
    {
        return $this->belongsToMany('MerchPlatform\Entities\CampaignOrder', 'discount_code_campaign_orders');
    }

    public function isExpired()
    {
        if(!isset($this->expires_at)){
            return false;
        }

        if(Carbon::now() < $this->expires_at){
            return false;
        }

        return true;
    }

    public function haveUses()
    {
        return $this->limit === null ? true : ($this->uses < $this->limit);
    }

    public function isValid()
    {
        if ($this->is_active != true) {
            return 'Discount inactive';
        }

        if (!$this->haveUses()) {
            return 'Discount has reached maximum uses';
        }

        if ($this->isExpired()) {
            return 'Discount has expired';
        }

        return true;
    }
}
