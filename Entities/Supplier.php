<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Supplier extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();
    
    public function address()
    {
        return $this->hasOne('MerchPlatform\Entities\Address');
    }

    public function payouts()
    {
        return $this->hasMany('MerchPlatform\Entities\Payout');
    }

    public function skus()
    {
        return $this->hasMany('MerchPlatform\Entities\Sku');
    }

    public function warehouses()
    {
        return $this->hasMany('MerchPlatform\Entities\Warehouse');
    }

}