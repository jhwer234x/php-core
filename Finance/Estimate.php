<?php namespace MerchPlatform\Finance;

class Estimate {

    public $gtinQtys;
    public $styleColors;
    public $variants;
    public $numUnits;
    public $numSweats;
    public $numSleeves;
    public $log;
    public $screenCharge;
    public $underbaseCharge;
    public $sweatsCharge;
    public $fulfillmentCharge;
    public $labelGenCharge;
    public $avgShippingCost;
    public $avgOrderTotal;
    public $payGatewayPercent;
    public $payGatewayFlat;
    public $polyMailerCharge;
    public $costSettings;
    public $sweatsExist;
    public $printMethod;

    public function __construct($gtinQtys = array(), $designs = array(), $orders = null, $packages = null)
    {
        $this->numUnits = $this->numSweats = 0;
        $this->log = array();
        $this->gtinQtys = $gtinQtys;
        if(is_array($gtinQtys) && !empty($gtinQtys)) {
            $variants = \MerchPlatform\Entities\Variant::with('product')
                ->whereIn('gtin', array_keys($gtinQtys))
                ->get();
            $variants->each(function($variant){
                $this->variants[$variant->gtin] = array(
                    'cost' => $variant->max_cost,
                    'id' => $variant->id,
                    'product_id' => $variant->product_id,
                    'color_id' => $variant->color_id,
                    'product' => $variant->product->name,
                    'markup_pct' => $variant->product->markup_pct,
                    'item_key' => $variant->product->item_key,
                    'flat_cost' => $variant->product->flat_cost,
                    'qty' => 0
                );
            });

            foreach($gtinQtys as $gtin => $qty) {
                $this->variants[$gtin]['qty'] += $qty;
                $this->numUnits += $qty;
            }
            foreach($this->variants as $variant) {
                if ($variant['item_key'] === 'sweats') {
                    $this->numSweats += $variant['qty'];
                    $this->sweatsExist = true;
                }
            }
        }
        
        $this->designs = $designs;
        $this->orders = $orders;
        $this->packages = $packages;

        $this->costSettings = json_decode(\MerchPlatform\Entities\Setting::whereKey('costs')->first()->value);
        $this->dtgRanges = json_decode(\MerchPlatform\Entities\Setting::whereKey('dtg_ranges')->first()->value);
    }

    public function getTotalCost()
    {
        $totalCost = $this->getSupplierCost() +
                $this->getProductionCost($this->designs, $this->numUnits, $this->numSweats) +
                $this->getFulfillmentCost($this->packages) +
                $this->getLabelGenCost($this->packages) +
                $this->getShippingCost($this->packages) +
                $this->getPayGatewayCost($this->orders) +
                $this->getPolyMailerCost($this->packages);

        return $totalCost;
    }

    public function getSupplierCost($productId = null, $qty = null)
    {
        $totalCost = 0;
        foreach($this->variants as $gtin => $data) {
            if($productId && $productId != $data['product_id']) {
                continue;
            }
            if($productId && $qty) {
                $data['qty'] = $qty;
            }
            $this->log[] = array(
                'name' => $gtin,
                'cost' => $data['cost'],
                'units' => $data['qty'],
                'type' => 'supplier',
                'accuracy' => 'estimate'
            );
            $totalCost += $data['cost'] * $data['qty'];
        }
        return $totalCost;
    }

    // Get markup pct (based on style)
    public function getMarkupPct($productId)
    {
        $markupPct = null;
        foreach($this->variants as $gtin => $data) {
            if($productId && $productId != $data['product_id']) {
                continue;
            }
            $markupPct = $data['markup_pct'];
            break;
        }
        return $markupPct;
    }

    public function getProductFlatCost($productId)
    {
        $flat = null;
        foreach($this->variants as $gtin => $data) {
            if($productId && $productId != $data['product_id']) {
                continue;
            }
            $flat = $data['flat_cost'] / 100;
            break;
        }
        return $flat;
    }

    public function getProductionCost($designs = null, $numUnits = null)
    {
        $totalCost = $numSweatsToQuote = 0;
        if(isset($this->numUnits) && isset($this->designs)) {
            $designs = $this->designs;
            $numUnits = $this->numUnits;
            $numSweatsToQuote = $this->numSweats;
        } elseif(is_array($designs) && is_int($numUnits)) {
            if($this->sweatsExist) {
                $numSweatsToQuote = $numUnits;
            }
        }
        
        $printMethod = $this->getPrintMethod($designs, $numUnits);
        if ($printMethod === 'scr') {
            foreach($designs as $design) {
                $totalCost += $this->getScreenImprintCost($design['colors'], $numUnits, $design['position']);
            }
            $totalCost += $this->getScreenAdditionalCharge($numSweatsToQuote);
            $totalCost += $this->getScreenCharge($designs);
        } else {
            foreach($designs as $design) {
                $totalCost += $this->getDtgCost($numUnits);
            }
        }
        
        return array(
            'cost' => $totalCost,
            'printMethod' => $printMethod
        );
    }

    public function getScreenCharge($designs)
    {
        $numScreens = 0;
        foreach($designs as $design) {
            $numScreens += ($design['colors'] + 1);
        }
        $screenCharge = $this->costSettings->cost_per_printer_screen * $numScreens;
        $this->log[] = array(
            'name' => 'screen_screens',
            'cost' => $this->costSettings->cost_per_printer_screen,
            'units' => $numScreens,
            'type' => 'printer',
            'accuracy' => 'actual'
        );
        return $screenCharge;
    }

    public function getScreenAdditionalCharge($numSweats)
    {
        $additionalCharge = $this->costSettings->cost_per_printer_sweatshirt_screen*$numSweats;
        $this->log[] = array(
            'name' => 'screen_sweats',
            'cost' => $this->costSettings->cost_per_printer_sweatshirt_screen,
            'units' => $this->numSweats,
            'type' => 'printer',
            'accuracy' => 'actual'
        );
        return $additionalCharge;
    }

    public function getPrintMethod($designs, $numUnits)
    {
        if (!is_null($this->printMethod)) {
            return $this->printMethod;
        }
        foreach($designs as $design) {
            foreach($this->dtgRanges as $dtgRange) {
                if($numUnits >= $dtgRange->from && $numUnits <= $dtgRange->to
                    && $design['colors'] >= $dtgRange->min_colors) {
                    return 'dtg';
                }
            }
        }
        return 'scr';
    }

    public function setPrintMethod($printMethod)
    {
        $this->printMethod = $printMethod;
        return $this->printMethod;
    }

    public function getScreenImprintCost($numColors, $numUnits, $area)
    {
        $scrRanges = \MerchPlatform\Entities\ScrRange::all();
        $totalCost = $underbase = 0;
        if($numUnits >= $this->costSettings->num_units_underbase_cutoff) {
            $numColors++;
        } else {
            $underbase = $this->costSettings->cost_per_printer_underbase_screen;
        }
        $numUnitsToQuote = $numUnits;
        if($numUnits < $this->costSettings->min_qty_screen_print) {
            $numUnitsToQuote = $this->costSettings->min_qty_screen_print;
        }
        foreach($scrRanges as $range) {
            if($numUnitsToQuote >= $range->units_from && $numUnitsToQuote <= $range->units_to) {
                $costPer = $range->{'colors_'.$numColors};
                $costPer += $underbase;
                break;
            }
        }
        
        $imprintCost = $costPer * $numUnits;
        $this->log[] = array(
            'name' => 'screen_imprint_dark',
            'cost' => $costPer,
            'units' => $numUnits,
            'type' => 'printer',
            'accuracy' => 'actual'
        );
        return $imprintCost;
    }

    public function getDtgCost($numUnits)
    {
        return ($this->costSettings->cost_per_dtg_print * $numUnits);
    }

    public function getFulfillmentCost($packages = null)
    {
        $totalCost = 0;
        if(is_int($packages)) {
            $numPackages = $packages;
            $estimateOrActual = 'actual';
        } elseif(is_array($packages)) {
            $numPackages = $packages->count();
            $estimateOrActual = 'actual';
        } else {
            $numPackages = $this->numUnits;
            $estimateOrActual = 'estimate';
        }
        $totalCost = ($numPackages * $this->costSettings->cost_per_printer_package_shipped);
        $this->log[] = array(
            'name' => 'fulfillment',
            'cost' => $this->costSettings->cost_per_printer_package_shipped,
            'units' => $numPackages,
            'type' => 'printer',
            'accuracy' => $estimateOrActual
        );
        return $totalCost;
    }

    public function getLabelGenCost($packages = null)
    {
        $totalCost = 0;
        if(is_null($packages)) {
            $numPackages = $this->numUnits;
            $estimateOrActual = 'estimate';
        } else {
            $numPackages = $packages->count();
            $estimateOrActual = 'actual';
        }
        $totalCost = $numPackages * $this->costSettings->cost_per_label_generated;
        $this->log[] = array(
            'name' => 'labels',
            'cost' => $this->costSettings->cost_per_label_generated,
            'units' => $numPackages,
            'type' => 'shipping',
            'accuracy' => $estimateOrActual
        );
        return $totalCost;
    }

    public function getShippingCost($packages = null)
    {
        $totalCost = 0;
        if(is_null($packages)) {
            $numPackages = $this->numUnits;
            $totalCost = $numPackages * $this->costSettings->cost_avg_package_shipped;
            $estimateOrActual = 'estimate';
            $this->log[] = array(
                'name' => 'shipping',
                'cost' => $this->costSettings->cost_avg_package_shipped,
                'units' => $numPackages,
                'type' => 'shipping',
                'accuracy' => $estimateOrActual
            );
        } else {
            foreach($packages as $package) {
                $totalCost += $package->cost;
            }
            $estimateOrActual = 'actual';
            $this->log[] = array(
                'name' => 'shipping',
                'cost' => $totalCost,
                'units' => 1,
                'type' => 'shipping',
                'accuracy' => $estimateOrActual
            );
        }
        
        return $totalCost;
    }

    public function getPayGatewayCost($orders = null)
    {
        $totalCost = 0;
        if(is_null($orders)) {
            $numOrders = $this->numUnits;
            $totalCost = $numOrders * (
                ($this->costSettings->cost_avg_order_total * ($this->costSettings->cost_per_transaction_pct / 100))
                + $this->costSettings->cost_per_transaction_flat
            );
            $estimateOrActual = 'estimate';
        } else {
            foreach($orders as $order) {
                $totalCost += (
                    ($order->price_total * ($this->costSettings->cost_per_transaction_pct / 100))
                    + $this->costSettings->cost_per_transaction_flat
                );
            }
            $estimateOrActual = 'actual';
        }
        
        $this->log[] = array(
            'name' => 'payments',
            'cost' => $totalCost,
            'units' => 1,
            'type' => 'payments',
            'accuracy' => $estimateOrActual
        );
        return $totalCost;
    }

    public function getPolyMailerCost($packages = null)
    {
        $totalCost = 0;
        if(is_null($packages)) {
            $numPackages = $this->numUnits;
            $estimateOrActual = 'estimate';
        } else {
            $numPackages = $packages->count();
            $estimateOrActual = 'actual';
        }
        $totalCost = $numPackages * $this->costSettings->cost_per_poly_mailer;
        $this->log[] = array(
            'name' => 'mailers',
            'cost' => $this->costSettings->cost_per_poly_mailer,
            'units' => $numPackages,
            'type' => 'materials',
            'accuracy' => $estimateOrActual
        );
        return $totalCost;
    }

    public function getLog()
    {
        foreach($this->log as $idx => $cost) {
            if($cost['units'] == 0 || $cost['cost'] == 0) {
                unset($this->log[$idx]);
            }
        }
        return $this->log;
    }
}
