<?php namespace MerchPlatform\Finance;

use Illuminate\Database\Capsule\Manager as DB;
use MerchPlatform\Entities\Setting;
use MerchPlatform\Entities\CampaignItem;
use MerchPlatform\Entities\CampaignOrder;
use MerchPlatform\Entities\Campaign;
use MerchPlatform\Entities\Design;
use MerchPlatform\Entities\Package;
use MerchPlatform\Entities\Variant;
use MerchPlatform\Finance\Estimate;
define('DEFAULT_MARKUP_PCT', 96);

class Quote {

    /**
     * Calculate raw costs of a campaign (live processing)
     * @param  int $campaignId - the campaign record's ID
     * @return array - with "total", and "log" keys
     */
    public static function getCampaignCosts($campaignId = null)
    {
        $campaign = Campaign::with(array('campaignOrders' => function($q){
            $q->whereStatus('paid');
        }))->find($campaignId);
        if(!$campaign) {
            return null;
        }
        $packages = Package::whereHas('campaignOrder', function($q) use($campaignId) {
            $q->whereStatus('paid')->whereHas('campaign', function($mq) use($campaignId) {
                $mq->whereId($campaignId);
            });
        })->get();
        $originalCampaignId = $campaign->id;
        if($campaign->original_id) {
            $originalCampaignId = $campaign->original_id;
        }
        $designs = Design::with('designVariants.decColors')->whereHas('group', function($q) use($originalCampaignId) {
            $q->whereCampaignId($originalCampaignId);
        })->get();
        
        $sendDesigns = array();
        foreach($designs as $design) {
            $numColors = $design->designVariants->first()->decColors->count();
            if($design->designVariants->first()->decColors->isEmpty()) {
                $numColors = $design->num_colors;
            }
            $sendDesigns[] = array(
                'position' => $design->dec_position,
                'colors' => $numColors
            );
        }
        $gtinQtys = array();
        foreach($campaign->campaignOrders as $mo) {
            foreach($mo->campaignItems as $mi) {
                if(!isset($gtinQtys[$mi->variant->gtin])) {
                    $gtinQtys[$mi->variant->gtin] = 0;
                }
                $gtinQtys[$mi->variant->gtin] += $mi->qty;
            }
        }
        $estimate = new Estimate(
            $gtinQtys,
            $sendDesigns,
            $campaign->campaignOrders,
            $packages
        );
        
        return array(
            'total' => $estimate->getTotalCost(),
            'log' => $costs = $estimate->getLog()
        );
    }

    /**
     * getBestCaseSellerProfit 
     * 
     * @param array $styleColorsQty (keys are product_id, values are arrays with keys color_id + values of quantity)
     * @param array $designs (values are arrays, with a key of position + value of string and a key of designs + value of int)
     * @param array $resalePrices (keys are product_id, values are seller-chosen resale prices)
     * @param bool $estimate 
     * @param int $goal 
     * @param string $priceMethod (enum of pricing method for flat-rate customers)
     * @static
     * @access public
     * @return integer
     */
    public static function getBestCaseSellerProfit($styleColorsQty, $designs, $resalePrices, $estimate = false, $goal = null, $priceMethod = null)
    {
        $printMethods = ['scr', 'dtg'];
        $responses = [];
        $methodName = 'getCampaignSellerQuote';
        $methodName .= ($estimate ? 'Studio' : '');
        if ($priceMethod) {
            $printMethods = [$priceMethod];
        }
        foreach ($printMethods as $printMethod) {
            $responses[] = self::{$methodName}(
                $styleColorsQty,
                $designs,
                $resalePrices,
                $printMethod,
                $goal
            );
        }
        
        $finalResponse = $responses[0];
        foreach ($responses as $response) {
            if ($estimate) {
                if ($response['minProfit'] > $finalResponse['minProfit']) {
                    $finalResponse = $response;
                }
            } else {
                if ($response['profit'] > $finalResponse['profit']) {
                    $finalResponse = $response;
                }
            }
        }

        return $finalResponse;
    }

    /**
     * Get a seller's quoted base costs and profits for a specific Campaign
     * @param  int $campaignId - The ID of the campaign record
     * @param  boolean $saveToDb - Save the costs and profit to the database?
     * @return array - with "lines", "raw", "markedup", "revenue", and "profit" keys
     */
    public static function getSellerQuoteByCampaignId($campaignId, $saveToDb = false)
    {
        $designs = $gtinQtys = $resalePrices = $styleColorsQty = array();

        $campaign = Campaign::with(array(
            'campaignOrders.campaignItems.variant',
            'group.lines.lineVariants' => function($q) {
                $q->with('variant')->whereHas('variant', function($qv) {
                    $qv->whereSizeId(6);
                });
            }
        ))->find($campaignId);
        $originalCampaignId = $campaign->id;
        if($campaign->original_id) {
            $originalCampaignId = $campaign->original_id;
        }
        $originalCampaign = Campaign::with('group.designs')
            ->find($originalCampaignId);

        // Format designs for quoter
        foreach($originalCampaign->group->designs as $design) {
            array_push($designs, array(
                'position' =>   $design->dec_position,
                'colors' =>     ($design->num_colors ?: 3)
            ));
        }
        // Set resale prices, initialize style & color sold qtys for quoter
        foreach($campaign->group->lines as $line) {
            $productId = $line->product_id;
            $resalePrices[$productId] = $line->campaign_price;
            foreach($line->lineVariants as $lv) {
                $colorId = $lv->variant->color_id;
                $styleColorsQty[$productId][$colorId] = 0;
            }
        }
        // Populate style & color sold qtys for quoter
        foreach($campaign->campaignOrders as $mo) {
            if($mo->status != 'paid') {
                continue;
            }
            foreach($mo->campaignItems as $mi) {
                $productId = $mi->variant->product_id;
                $colorId = $mi->variant->color_id;
                $styleColorsQty[$productId][$colorId] += $mi->qty;
            }
        }

        // Get the quote and save & return
        $response = self::getBestCaseSellerProfit(
            $styleColorsQty,
            $designs,
            $resalePrices,
            false,
            null,
            $campaign->price_method
        );
        if ($campaign->price_method) {
            $bestCaseResponse = self::getBestCaseSellerProfit(
                $styleColorsQty,
                $designs,
                $resalePrices,
                false,
                null
            );
            $response['printMethod'] = $bestCaseResponse['printMethod'];
        }
        $upsellDiscountTotal = self::getUpsellDiscountTotal($campaignId);
        $discountCodeDiscountTotal = self::getDiscountCodeDiscountTotal($campaignId);
        $response['profit'] = self::getFinalPlatformProfit(
            $response['lines'],
            $campaignId,
            $upsellDiscountTotal,
            $discountCodeDiscountTotal
        );
        if ($saveToDb) {
            foreach($response['lines'] as $line) {
                foreach($campaign->group->lines as $lineDb) {
                    if($line['productId'] == $lineDb->product_id) {
                        $lineDb->campaign_base_cost = self::getCurrency($line['baseCostGoal']);
                        $lineDb->save();
                    }
                }
            }
            $campaign->profit = self::getCurrency($response['profit']);
            $campaign->save();
        }
        
        return $response;
    }

    public static function getUpsellDiscountTotal($campaignId)
    {
        $totalUpsellCents = DB::table('campaign_items')
            ->join('campaign_orders', 'campaign_items.campaign_order_id', '=', 'campaign_orders.id')
            ->where('campaign_orders.status', '=', 'paid')
            ->where('campaign_orders.campaign_id', '=', $campaignId)
            ->sum('upsell_discount_amount');
        $totalUpsellDollars = $totalUpsellCents / 100;

        return $totalUpsellDollars;
    }

    public static function getDiscountCodeDiscountTotal($campaignId)
    {
        $campaignOrders = CampaignOrder::whereCampaignId($campaignId)
            ->where('price_savings', '>', '0')
            ->whereStatus('paid')
            ->get();
        $totalDiscountCodeDollars = 0;
        foreach ($campaignOrders as $campaignOrder) {
            $totalDiscountCodeDollars += $campaignOrder->price_savings;
        }

        return $totalDiscountCodeDollars;
    }

    public static function getFinalPlatformProfit($lines, $campaignId, $upsellDiscountTotal, $discountCodeDiscountTotal)
    {
        $campaignItems = CampaignItem::whereCampaignId($campaignId)
            ->select('id', 'qty', 'variant_id')
            ->whereHas('campaignOrder', function($q) {
                $q->whereStatus('paid')
                    ->has('campaignOrderExternals', 0);
            })
            ->with('variant')
            ->get();
        $profitOnPlatform = 0;
        foreach ($lines as $line) {
            $soldOnPlatform = 0;
            foreach ($campaignItems as $campaignItem) {
                if ($line['productId'] == $campaignItem->variant->product_id) {
                    $soldOnPlatform += $campaignItem->qty;
                }
            }
            $profitPer = ($line['resalePrice'] - $line['baseCostGoal']);
            $profitOnPlatform += ($soldOnPlatform * $profitPer);
        }
        $realProfitOnPlatform = ($profitOnPlatform - $upsellDiscountTotal - $discountCodeDiscountTotal);
        return $realProfitOnPlatform;
    }

    /**
     * Get a seller's quoted base costs and profits based on user inputted data (post-launch)
     * @param  array $styleColorsQty - Array of styles, their chosen colors => qty sold
     * @param  array $designs - Array of designs, their position => num of colors
     * @param  array $resalePrices - Array of styles, their product ID => resale price
     * @return array - with "lines", "raw", "markedup", "revenue", and "profit" keys
     */
    public static function getCampaignSellerQuote($styleColorsQty, $designs, $resalePrices, $printMethod)
    {
        $markupMods = json_decode(Setting::whereKey('markup_modifiers')
            ->first()->value);
        $dtgCostDeduction = Setting::whereKey('dtg_cost_deduction')
            ->first()->value;
        $styleColors = $costLogs = $lines = $totalResales = array();
        $splitCost = $totalBaseCost = $totalRevenue = $rawCost = $totalQty = 0;

        $gtinQtys = self::getGtinQtys($styleColorsQty);
        $estimate = new Estimate($gtinQtys, $designs);
        $estimate->setPrintMethod($printMethod);
        $fulfillmentCost = $estimate->getFulfillmentCost();
        $prodData = $estimate->getProductionCost();
        $productionCost = $prodData['cost'];
        $splitCost = $fulfillmentCost + $productionCost;
        $splitCostPerUnit = $splitCost / $estimate->numUnits;
        $rawCost += $fulfillmentCost;
        $rawCost += $productionCost;
        foreach($styleColorsQty as $productId => $colorsQty) {
            $totalQty += array_sum($colorsQty);
        }
        foreach($styleColorsQty as $productId => $colorsQty) {
            $totalCost = $qty = $totalCostPer = $totalProfit = $flatCost = 0;
            $qty = array_sum($colorsQty);
            if ($qty == 0) {
                continue;
            }
            $flatCost = $estimate->getProductFlatCost($productId);
            if ($printMethod == 'flat' && !is_null($flatCost)) {
                $totalCost += $flatCost * $qty;
                foreach($designs as $design) {
                    $totalCost += $estimate->getDtgCost($qty);
                }
            } else {
                $markupPct = $estimate->getMarkupPct($productId);
                if (is_null($markupPct)) {
                    $markupPct = DEFAULT_MARKUP_PCT;
                }

                $totalCost += $estimate->getSupplierCost($productId);
                $totalCost += $splitCostPerUnit * $qty;
                $totalCost += $totalCost * ($markupPct / 100);
                $totalCost = self::applyMarkupModifications(
                        $printMethod, $totalCost, $markupMods,
                        $dtgCostDeduction, $qty, count($designs));
            }
            
            $rawCost += $totalCost;
            $totalCostPer = $totalCost / $qty;
            $totalResales[$productId] = $resalePrices[$productId] * $qty;
            $totalRevenue += $totalResales[$productId];
            $totalBaseCost += $totalCost;
            $profitPer = $resalePrices[$productId] - $totalCostPer;
            $totalProfit = $profitPer * $qty;
            $lines[$productId] = array(
                'productId' =>          $productId,
                'baseCostGoal' =>       self::getCurrency($totalCostPer),
                'profitPer' =>          self::getCurrency($profitPer),
                'totalProfit' =>        self::getCurrency($totalProfit),
                'resalePrice' =>        self::getCurrency($resalePrices[$productId])
            );
        }

        $rawCost = self::getCurrency($rawCost);
        $totalBaseCost = self::getCurrency($totalBaseCost);
        $totalRevenue = self::getCurrency($totalRevenue);
        $profit = ($totalRevenue - $totalBaseCost);
        $profit = self::getCurrency($profit);
        return array(
            'lines' => $lines,
            'raw' => $rawCost,
            'markedup' => $totalBaseCost,
            'revenue' => $totalRevenue,
            'profit' => $profit,
            'printMethod' => $printMethod
        );
    }

    /**
     * Get a seller's quoted base costs and profits based on inputted data (in Designer, pre-launch)
     * @param  array $styleColorsQty - Array of styles, their chosen colors => qty sold
     * @param  array $designs - Array of designs, their position => num of colors
     * @param  array $resalePrices - Array of styles, their product ID => resale price
     * @param  string $printMethod - The print method ('dtg' or 'scr' or 'flat')
     * @param  int $goal - Number of pieces to assume Campaign will sell
     * @return array - with "lines", "goal", "tippingPoint", "minProfit", and "maxProfit" keys
     */
    public static function getCampaignSellerQuoteStudio($styleColorsQty, $designs, $resalePrices, $printMethod, $goal = null)
    {
        $markupMods = json_decode(Setting::whereKey('markup_modifiers')
            ->first()->value);
        if (is_null($goal)) {
            $tippingPoint = Setting::whereKey('tipping_point')
                ->first()->value;
            $goal = $tippingPoint;
        }
        $dtgCostDeduction = Setting::whereKey('dtg_cost_deduction')
            ->first()->value;
        $splitCost = $totalBaseCost = $totalRevenue = 0;
        $styleColors = $costLogs = $lines = $totalResales = array();

        $gtinQtys = self::getGtinQtys($styleColorsQty);
        $estimate = new Estimate($gtinQtys, $designs);
        $estimate->setPrintMethod($printMethod);
        
        foreach($styleColorsQty as $productId => $colorsQty) {
            $totalCost = $flatCost = $totalProfit = $profitPer = $totalCostPer = $markupPct = 0;
            $estimate->numUnits = $goal;
            $flatCost = $estimate->getProductFlatCost($productId);
            if ($printMethod == 'flat' && !is_null($flatCost)) {
                $totalCost += $flatCost * $goal;
                foreach($designs as $design) {
                    $totalCost += $estimate->getDtgCost($goal);
                }
            } else {
                $markupPct = $estimate->getMarkupPct($productId);
                if(is_null($markupPct)) {
                    $markupPct = DEFAULT_MARKUP_PCT;
                }
                $totalCost += $estimate->getFulfillmentCost($goal);
                $prodData = $estimate->getProductionCost($designs, $goal);
                $totalCost += $prodData['cost'];
                $printMethod = $prodData['printMethod'];
                $totalCost += $estimate->getSupplierCost($productId, $goal);
                $totalCost += ($totalCost * ($markupPct / 100));
                $totalCost = self::applyMarkupModifications(
                    $printMethod, $totalCost, $markupMods,
                    $dtgCostDeduction, $goal, count($designs));
            }
            $totalCostPer = $totalCost / $goal;
            $profitPer = $resalePrices[$productId] - $totalCostPer;
            $totalProfit = $profitPer * $goal;
            $lines[$productId] = array(
                'productId' =>          $productId,
                'baseCostGoal' =>       self::getCurrency($totalCostPer),
                'profitPer' =>          self::getCurrency($profitPer),
                'totalProfit' =>        self::getCurrency($totalProfit),
                'resalePrice' =>        self::getCurrency($resalePrices[$productId])
            );
        }

        $minProfit = $maxProfit = null;
        foreach($lines as $productId => $line) {
            if(is_null($minProfit) && is_null($maxProfit)) {
                $minProfit = $line['totalProfit'];
                $maxProfit = $line['totalProfit'];
            } else {
                if($line['totalProfit'] > $maxProfit) {
                    $maxProfit = $line['totalProfit'];
                }
                if($line['totalProfit'] < $minProfit) {
                    $minProfit = $line['totalProfit'];
                }
            }
        }
        
        return array(
            'lines' => $lines,
            'goal' => intval($goal),
            'tippingPoint' => intval($tippingPoint),
            'minProfit' => self::getCurrency($minProfit),
            'maxProfit' => self::getCurrency($maxProfit),
            'printMethod' => $printMethod
        );
    }

    /**
     * Get array of gtinQtys (gtin => qty), get most expensive color of the chosen (per style)
     * @param  array $styleColorsQty - Array of styles, their chosen colors => qty sold
     * @return array - Array of gtinQtys with the gtin as key, qty as value
     */
    private static function getGtinQtys($styleColorsQty)
    {
        $productIds = array_keys($styleColorsQty);
        $colorIds = array();
        foreach($styleColorsQty as $productId => $colorsQty) {
            foreach($colorsQty as $colorId => $qty) {
                array_push($colorIds, $colorId);
            }
        }
        $variants = Variant::whereIn('color_id', $colorIds)
            ->whereIn('product_id', $productIds)
            ->whereSizeId(6)
            ->get();
        $gtinQtys = array(); $totalQty = 0;
        foreach($styleColorsQty as $productId => $colorsQty) {
            $variantToQuote = null;
            foreach($variants as $v) {
                if($v->product_id != $productId) {
                    continue;
                }
                if(!in_array($v->color_id, array_keys($colorsQty))) {
                    continue;
                }
                if(is_null($variantToQuote)) {
                    $variantToQuote = $v;
                    continue;
                }
                if($v->max_cost > $variantToQuote->max_cost) {
                    $variantToQuote = $v;
                    continue;
                }
            }
            $qty = array_sum($colorsQty);
            $gtinQtys[$variantToQuote->gtin] = $qty;
        }

        return $gtinQtys;
    }

    private static function getCurrency($float)
    {
        return round($float, 2, PHP_ROUND_HALF_DOWN);
    }

    private static function applyMarkupModifications($printMethod, $totalCost, $markupMods, $dtgCostDeduction, $qty, $numViews)
    {
        if ($printMethod === 'scr') {
            foreach($markupMods as $mod) {
                if(($mod[0] <= $qty) && ($mod[1] >= $qty)) {
                    $totalCost += ($qty * $mod[2]);
                    break;
                }
            }
        } elseif ($printMethod === 'dtg') {
            $totalCost -= (($qty * $dtgCostDeduction) * $numViews);
        }
        return $totalCost;
    }
}
