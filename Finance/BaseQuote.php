<?php namespace MerchPlatform\Finance;

use MerchPlatform\Entities\Setting;

class BaseQuote
{
    protected $costSettings;

    public function __construct()
    {
        $costSettings = Setting::whereKey('costs')->first()->value;
        $this->costSettings = json_decode($costSettings);
    }

    protected function getCostArray($name, $type, $units, $cost, $meta = null)
    {
        $total = ($units * $cost);
        return compact(
            'name', 'type', 'units',
            'cost', 'meta', 'total'
        );
    }

    protected function getCostsArray($costs)
    {
        $total = 0;
        foreach ($costs as $cost) {
            $total += $cost['total'];
        }
        return compact('costs', 'total');
    }
}
