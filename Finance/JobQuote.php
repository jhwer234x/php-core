<?php namespace MerchPlatform\Finance;

use MerchPlatform\Finance\BaseQuote;
use MerchPlatform\Entities\Setting;
use MerchPlatform\Entities\Campaign;
use MerchPlatform\Entities\SupplierOrderItem;
use MerchPlatform\Entities\Job;
use MerchPlatform\Entities\ScrRange;
use MerchPlatform\Entities\Design;
use Illuminate\Database\Capsule\Manager as DB;

class JobQuote extends BaseQuote
{
    protected $dtgRanges;
    protected $scrRanges;
    protected $numUnits;
    protected $numSweats;
    protected $numTees;
    protected $decMethod;
    protected $numDesignedViews;
    protected $positionNumColors;
    protected $numPackages;
    protected $fulfillCostUnit;
    protected $fulfillCostPackage;

    public function __construct()
    {
        parent::__construct();
        $dtgRanges = Setting::whereKey('dtg_ranges')->first()->value;
        $this->dtgRanges = json_decode($dtgRanges);
        $this->scrRanges = ScrRange::all();
    }

    public function setJob($jobId)
    {
        $job = Job::find($jobId);
        $contractor = $job->contractor;
        $campaign = Campaign::whereOrderId($job->order_id)
            ->first();
        $originalCampaignId = $campaign->getOriginalCampaignId();
        $this->units = DB::table('supplier_order_items')
            ->join('variants', 'supplier_order_items.variant_id', '=', 'variants.id')
            ->whereJobId($jobId)
            ->whereStatus('arrived')
            ->groupBy('variant_id')
            ->get([
                'variants.gtin',
                'variants.max_cost',
                DB::raw('count(variant_id) as qty')
            ]);
        $numUnits = array_sum(array_column($this->units, 'qty'));
        $numSweats = SupplierOrderItem::whereJobId($job->id)
            ->whereStatus('arrived')
            ->whereHas('variant', function($q) {
                $q->whereHas('product', function($q) {
                    $q->where('item_key', 'sweats');
                });
            })
            ->count();
        $numTees = ($numUnits - $numSweats);

        $decMethod = $job->dec_method;
        $designs = Design::with('designVariants.decColors')
            ->whereHas('group', function($q) use($originalCampaignId) {
                $q->whereCampaignId($originalCampaignId);
            })->get();
        $positionNumColors = array();
        foreach ($designs as $design) {
            $numColors = 'full';
            if ($decMethod === 'scr') {
                $numColors = $design->designVariants->first()
                    ->decColors->count();
            }
            $positionNumColors[$design->dec_position] = $numColors;
        }

        $packages = DB::table('supplier_order_items')
            ->select('package_id', 'cost')
            ->join('packages', 'packages.id', '=', 'supplier_order_items.package_id')
            ->distinct()
            ->whereNotNull('package_id')
            ->where('job_id', '=', $job->id)
            ->get();
        $numPackages = count($packages);

        $fulfillCostUnit = $contractor->fulfill_cost_unit;
        $fulfillCostPackage = $contractor->fulfill_cost_package;

        $supply = compact(
            'numSweats',
            'numTees',
            'gtinQtys'
        );
        $decoration = compact(
            'decMethod',
            'numDesignedViews',
            'positionNumColors'
        );
        $shipping = compact(
            'numPackages',
            'packages'
        );
        $costs = compact(
            'fulfillCostUnit',
            'fulfillCostPackage'
        );

        $this->initialize($decoration, $supply, $shipping, $costs);
    }

    public function initialize($decoration, $supply, $shipping, $costs)
    {
        $this->positionNumColors = $decoration['positionNumColors'];
        $this->numTees = $supply['numTees'];
        $this->numSweats = $supply['numSweats'];
        $this->numPackages = $this->fulfillCostUnit =
            $this->fulfillCostPackage = 0;

        $this->numUnits = ($this->numTees + $this->numSweats);
        $this->numDesignedViews = count($this->positionNumColors);
        
        if (isset($decoration['decMethod'])) {
            $this->decMethod = $decoration['decMethod'];
        } else {
            $this->decMethod = $this->getPrintMethod();
        }

        if (isset($shipping['numPackages'])) {
            $this->numPackages = $shipping['numPackages'];
        }
        if (isset($shipping['packages'])) {
            $this->packages = $shipping['packages'];
        }
        if (isset($costs['fulfillCostUnit'])) {
            $this->fulfillCostUnit = $costs['fulfillCostUnit'];
        }
        if (isset($costs['fulfillCostPackage'])) {
            $this->fulfillCostPackage = $costs['fulfillCostPackage'];
        }
    }

    public function getSupplyCosts()
    {
        $costs = array();
        if ($this->units) {
            foreach ($this->units as $unit) {
                $costs[] = $this->getSupplyCost($unit);
            }
        }
        return $this->getCostsArray($costs);
    }

    public function getSupplyCost($unit)
    {
        $costPer = ($unit['max_cost'] / 100);
        $costArray = $this->getCostArray(
            'garments',
            'supply',
            $unit['qty'],
            $costPer
        );

        return $costArray;
    }

    public function getContractorCosts()
    {
        if ($this->decMethod === 'dtg') {
            $prodCosts = $this->getDtgPrintCosts(
                $this->numDesignedViews,
                $this->numTees,
                $this->numSweats
            );
        } else {
            $prodCosts = $this->getScreenPrintCosts(
                $this->positionNumColors,
                $this->numUnits,
                $this->numSweats
            );
        }

        $fulfillCosts = $this->getFulfillmentCosts(
            $this->numUnits,
            $this->numPackages,
            $this->fulfillCostUnit,
            $this->fulfillCostPackage
        );

        $costs = array_merge($prodCosts, $fulfillCosts);
        return $this->getCostsArray($costs);
    }

    public function getShippingCosts()
    {
        $costs = array();
        if ($this->numPackages) {
            $costs[] = $this->getShippingCost();
            $costs[] = $this->getLabelsCost();
            if ($this->decMethod === 'scr') {
                $costs[] = $this->getMailersCost();
            }
        }
        return $this->getCostsArray($costs);
    }

    public function getScreenPrintCosts($positionNumColors, $numUnits, $numSweats)
    {
        $costs = array();
        if (!$numUnits) {
            return $costs;
        }

        foreach ($positionNumColors as $position => $numColors) {
            $costs[] = $this->getScreenImprintCost($numColors, $numUnits);
        }
        
        $costs[] = $this->getScreenScreenCost($positionNumColors);

        if ($numSweats) {
            $costs[] = $this->getScreenSweatsCost($numSweats);
        }
        return $costs;
    }

    public function getDtgPrintCosts($numDesignedViews, $numTees, $numSweats)
    {
        $costs = array();
        if ($numTees) {
            $costs[] = $this->getDtgImprintCost($numTees, $numDesignedViews, 'tee');
        }
        if ($numSweats) {
            $costs[] = $this->getDtgImprintCost($numSweats, $numDesignedViews, 'sweat');
        }
        return $costs;
    }

    public function getFulfillmentCosts($numUnits, $numPackages, $fulfillCostUnit, $fulfillCostPackage)
    {
        $costs = array();
        if ($fulfillCostUnit && $numUnits) {
            $costs[] = $this->getFulfillmentCost($numUnits, 'units', $fulfillCostUnit);
        }

        if ($fulfillCostPackage && $numPackages) {
            $costs[] = $this->getFulfillmentCost($numPackages, 'packages', $fulfillCostPackage);
        }

        return $costs;
    }

    public function getLabelsCost()
    {
        $costPer = $this->costSettings->cost_per_label_generated;
        $costArray = $this->getCostArray(
            'labels',
            'ship',
            $this->numPackages,
            $costPer
        );

        return $costArray;
    }

    public function getMailersCost()
    {
        $costPer = $this->costSettings->cost_per_poly_mailer;
        $costArray = $this->getCostArray(
            'mailers',
            'ship',
            $this->numPackages,
            $costPer
         );

        return $costArray;
    }

    public function getShippingCost()
    {
        $totalCost = $costPer = 0;
        if ($this->numPackages) {
            foreach ($this->packages as $package) {
                $totalCost += $package['cost'];
            }
            $totalCost = ($totalCost / 100);
            $costPer = ($totalCost / $this->numPackages);
        }

        $costArray = $this->getCostArray(
            'carriers',
            'ship',
            $this->numPackages,
            $costPer
        );

        return $costArray;
    }

    public function getFulfillmentCost($numUnits, $unitType, $costPer)
    {
        $costArray = $this->getCostArray(
            $unitType,
            'fulfill',
            $numUnits,
            $costPer
        );
 
        return $costArray;
    }

    public function getDtgImprintCost($numPieces, $numDesignedViews, $unitType)
    {
        $fieldName = ('cost_per_dtg_' . $unitType);
        $costPer = $this->costSettings->{$fieldName};
        $units = ($numPieces * $numDesignedViews);

        $costArray = $this->getCostArray(
            'dtg_imprints',
            'produce',
            $units,
            $costPer,
            $unitType
        );

        return $costArray;
    }

    public function getScreenImprintCost($numColors, $numUnits)
    {
        $underbase = 0;
        $numColorsToQuote = $numColors;

        // Handle underbase charge depending on number of units
        if ($numUnits >= $this->costSettings->num_units_underbase_cutoff) {
            $numColorsToQuote++;
        } else {
            $underbase = $this->costSettings->cost_per_printer_underbase_screen;
        }

        // Get the cost per each unit needing this imprint. Get total.
        $costPer = $this->getScreenRangeCostPer($numColorsToQuote, $numUnits);
        $costPer += $underbase;

        $costArray = $this->getCostArray(
            'scr_imprints',
            'produce',
            $numUnits,
            $costPer,
            'dark'
        );

        return $costArray;
    }

    public function getScreenSweatsCost($numSweats)
    {
        $costPer = $this->costSettings->cost_per_printer_sweatshirt_screen;

        $costArray = $this->getCostArray(
            'scr_sweats',
            'produce',
            $numSweats,
            $costPer
        );

        return $costArray;
    }

    public function getScreenScreenCost($positionNumColors)
    {
        // Get the number of screens
        $totalNumColors = 0;
        foreach ($positionNumColors as $position => $numColors) {
            $totalNumColors += $numColors;
        }
        $numDesignedViews = count($positionNumColors);
        $numScreens = ($numDesignedViews + $totalNumColors);

        // Get the cost per screen. Get the total.
        $costPer = $this->costSettings->cost_per_printer_screen;
        
        $costArray = $this->getCostArray(
            'scr_screens',
            'produce',
            $numScreens,
            $costPer
        );

        return $costArray;
    }

    public function getScreenRangeCostPer($numColorsToQuote, $numUnits)
    {
        $selectedRange = $this->scrRanges->first();
        foreach($this->scrRanges as $range) {
            if ($numUnits >= $range->units_from && $numUnits <= $range->units_to) {
                $selectedRange = $range;
                break;
            }
        }
        $fieldName = ('colors_' . $numColorsToQuote);
        $costPer = $selectedRange->{$fieldName};
        return $costPer;
    }

    public function getPrintMethod($positionNumColors, $numUnits)
    {
        foreach ($positionNumColors as $position => $numColors) {
            if ($this->checkIfDtg($numColors, $numUnits)) {
                return 'dtg';
            }
        }

        return 'scr';
    }

    public function checkIfDtg($numColors, $numUnits)
    {
        foreach($this->dtgRanges as $dtgRange) {
            if($numUnits >= $dtgRange->from && $numUnits <= $dtgRange->to
                && $numColors >= $dtgRange->min_colors) {
                return true;
            }
        }

        return false;
    }
}
