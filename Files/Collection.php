<?php namespace MerchPlatform\Files;

use Illuminate\Support\Collection as IlluminateCollection;

class Collection extends IlluminateCollection
{
    public function __construct($filesystem)
    {
        return parent::__construct(array_map(function($item) use($filesystem)
        {
            return new Details($filesystem, $item);
        }, $filesystem->listContents()));
    }
}