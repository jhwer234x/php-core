<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Gif implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'gif';
    }

    public function getMimeTypes()
    {
        return array('image/gif');
    }
}