<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Pdf implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'pdf';
    }

    public function getMimeTypes()
    {
        return array('application/pdf', 'application/x-download');
    }
}