<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Svg implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'svg';
    }

    public function getMimeTypes()
    {
        return array('image/svg+xml');
    }
}