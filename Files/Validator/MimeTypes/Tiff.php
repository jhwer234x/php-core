<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Tiff implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'tiff';
    }

    public function getMimeTypes()
    {
        return array('image/tiff');
    }
}