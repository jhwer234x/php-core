<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Jpe implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'jpe';
    }

    public function getMimeTypes()
    {
        return array('image/jpeg', 'image/pjpeg');
    }
}