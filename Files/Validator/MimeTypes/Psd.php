<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Psd implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'psd';
    }

    public function getMimeTypes()
    {
        return array('application/x-photoshop', 'image/vnd.adobe.photoshop', 'image/photoshop', 'image/x-photoshop', 'image/psd', 'application/photoshop', 'application/psd', 'zz-application/zz-winassoc-psd');
    }
}