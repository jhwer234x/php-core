<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Eps implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'eps';
    }

    public function getMimeTypes()
    {
        return array('application/postscript', 'application/octet-stream', 'application/eps', 'application/x-eps', 'image/eps', 'image/x-eps');
    }
}