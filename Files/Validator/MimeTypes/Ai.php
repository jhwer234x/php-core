<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Ai implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'ai';
    }

    public function getMimeTypes()
    {
        return array('application/postscript', 'application/pdf', 'application/illustrator');
    }
}