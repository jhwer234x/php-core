<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Tif implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'tif';
    }

    public function getMimeTypes()
    {
        return array('image/tiff');
    }
}