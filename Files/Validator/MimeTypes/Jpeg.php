<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Jpeg implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'jpeg';
    }

    public function getMimeTypes()
    {
        return array('image/jpeg', 'image/pjpeg');
    }
}