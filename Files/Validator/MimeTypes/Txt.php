<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Txt implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'txt';
    }

    public function getMimeTypes()
    {
        return array('text/plain');
    }
}