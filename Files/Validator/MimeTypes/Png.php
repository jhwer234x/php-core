<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Png implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'png';
    }

    public function getMimeTypes()
    {
        return array('image/png',  'image/x-png');
    }
}