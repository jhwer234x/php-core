<?php namespace MerchPlatform\Files\Storage;

class Storage
{
    const RACKSPACE = 'Rackspace';
    const LOCAL = 'local';
    const AWSS3 = 'AwsS3';
    const DROPBOX = 'dropbox';
    const FTP = 'ftp';
    const SFTP = 'sftp';
    const ZIP = 'zip';
    const WEBDAV = 'webdav';
}