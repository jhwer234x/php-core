<?php namespace MerchPlatform\Files\Storage;

use League\Flysystem\Adapter\AwsS3 as AwsS3Adapter;
use Aws\S3\S3Client;

class AwsS3 extends AbstractStorage implements StorageInterface
{
    public function storage()
    {
        $client = S3Client::factory(array(
            'key' => $this->getOption('key'),
            'secret' => $this->getOption('secret'),
            'region' => $this->getOption('region')
        ));
       return new AwsS3Adapter($client, $this->getOption('bucket'), $this->getOption('prefix'));
    }
}