<?php namespace MerchPlatform\Files;

use Imagick;
use MerchPlatform\Files\Exceptions\ConvertFormatNotFound;
use MerchPlatform\Files\Exceptions\ConvertMethodNotFound;

class Converter {

    private $file;
    private $convertable;
    private $convertedFile;
    private $filePath;
    private $fileType = 'image';
    private $convertTypes = array(
        'image' => array('jpg', 'gif', 'png', 'pdf', 'psd', 'eps', 'ai', 'tif', 'tiff', 'jpeg', 'jpe', 'bmp')
    );

    public function __construct($file, $convertable)
    {
        $this->file = $file;
        $this->convertable = $this->parseConvertable($convertable);
        $this->decideOnFileType();
    }

    private function decideOnFileType()
    {
        $this->filePath = pathinfo($this->file['name']);
        if (in_array($this->filePath['extension'], $this->convertTypes['image'])) {
            $this->fileType = 'image';
        }
        return $this;
    }

    private function parseConvertable($convertable)
    {
        return is_array($convertable) ? $convertable : explode('|', $convertable);
    }

    public function convert($convertTo = null)
    {
        if (is_null($convertTo)) throw new ConvertFormatNotFound("Converter needs a format to convert to.");
        $method = 'convertTo'.ucwords($this->fileType);
        if (!method_exists($this, $method)) throw new ConvertMethodNotFound("Cannot convert file ({$this->fileType}) to {$convertTo}.");
        return $this->{$method}($convertTo);
    }

    public function convertToImage($convertTo)
    {
        if (!in_array($this->filePath['extension'], $this->convertable)) return false;
        $image = new Imagick($this->file['tmp_name']);
        $image->setImageFormat($convertTo);
        $this->convertedFile = $image->getImageBlob();
        return true;
    }

    public function file()
    {
        return $this->convertedFile;
    }
}