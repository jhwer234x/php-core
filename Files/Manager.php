<?php namespace MerchPlatform\Files;

use Imagick;
use ImagickPixel;
use League\Flysystem\Filesystem;
use MerchPlatform\Files\Storage\Storage;
use MerchPlatform\Files\Exceptions\StorageAdapterNotFoundException;
use MerchPlatform\Files\Exceptions\NoFileSentException;
use MerchPlatform\Files\Exceptions\FileSizeExceededException;
use MerchPlatform\Files\Exceptions\UnknownErrorException;
use MerchPlatform\Files\Exceptions\InvalidFileTypeException;

class Manager {

    private $configuration = array();
    private $storage = Storage::AWSS3;
    private $directory;

    public function __construct(array $configuration = array())
    {
        $this->configuration = $configuration;
    }

    public function __call($method, $arguments)
    {
        $this->setStorage($method);
        return $this;
    }

    public function upload($file, $options)
    {
        $allowed = isset($options['allowed']) ? $options['allowed'] : null;
        $convert = isset($options['convert']) ? $options['convert'] : null;
        $maxSize = isset($options['maxSize']) ? $options['maxSize'] : null;
        $filename = isset($options['filename']) ? $options['filename'] : false;

        if (isset($options['directory'])) $this->setDirectory($options['directory']);
        if (isset($options['storage'])) $this->setStorage($options['storage']);
        $validator = new Validator($_FILES[$file], $allowed, $maxSize);
        if (!$validator->passes()) {
            switch($validator->getError()) {
                case 'no_file_sent': throw new NoFileSentException;
                case 'file_size_exceeded': throw new FileSizeExceededException;
                case 'invalid_file_type': throw new InvalidFileTypeException;
                case 'unknown_error': throw new UnknownErrorException;
                default: throw new UnknownErrorException;
            }
        }
        $converter = new Converter($_FILES[$file], $convert);
        $response = array();
        if ($converter->convert('png'))
        {
            $convertFileName = $this->nameFileForUpload();
            if ($this->create($convertFileName, $converter->file())) {
                $response['converted'] = $convertFileName;
            }
        }
        $originalPath = pathinfo($_FILES[$file]['name']);
        $originalFileName = $this->nameFileForUpload($originalPath['extension'], $filename);
        $this->create($originalFileName, file_get_contents($_FILES[$file]['tmp_name']));
        $response['original'] = $originalFileName;
        return $response;
    }

    public function uploadMultiple($files, $options)
    {
        $response = array();
        $filename = isset($options['filename']) ? $options['filename'] : false;
        if (isset($options['directory'])) $this->setDirectory($options['directory']);
        if (isset($options['storage'])) $this->setStorage($options['storage']);
        for($i=0; $i<count($_FILES[$files]['name']); $i++){
            $originalPath = pathinfo($_FILES[$files]['name'][$i]);
            $originalFileName = $this->nameFileForUpload($originalPath['extension'], $filename);
            $this->create($originalFileName, file_get_contents($_FILES[$files]['tmp_name'][$i]));
            $response['images'][$i] = $originalFileName;
        }
        return $response;
    }    

    public function combineWithDir($filename)
    {
        if ($this->getDirectory() == null) {
            return $filename;
        } else {
            $fullPath = $this->getDirectory();
            if (substr($fullPath, -1) != "/") {
                $fullPath .= "/";
            };
            return $fullPath . $filename;
        }
    }

    // expect array: [{name: "uploaded-file1.txt", tmp_name: "/tmp/phpa3Za", directory: "massUpload"}]
    public function uploadKeyValues($kv){
        foreach($kv as &$eachFile){
            if (isset($eachFile['directory'])) $this->setDirectory($eachFile['directory']);
            if (isset($eachFile['storage'])) $this->setStorage($eachFile['storage']);
            $pathInfo = pathinfo($eachFile['name']);
            $remoteFileName = $this->nameFileForUpload($pathInfo['extension']);
            $this->create($this->combineWithDir($remoteFileName), file_get_contents($eachFile['tmp_name']));
            $eachFile['remote_file'] = isset($eachFile['directory']) ? $eachFile['directory']."/" : "";
            $eachFile['remote_file'] .= $remoteFileName;
        }
        return $kv;
    }

    // @altmind: This function cannot be used to handle multiple files upload(like when we upload files with field name: files[])
    public function uploadStraight($file, $options)
    {
        $filename = isset($options['filename']) ? $options['filename'] : false;
        if (isset($options['directory'])) $this->setDirectory($options['directory']);
        if (isset($options['storage'])) $this->setStorage($options['storage']);
        $originalPath = pathinfo($_FILES[$file]['name']);
        $originalFileName = $this->nameFileForUpload($originalPath['extension'], $filename);
        $this->create($originalFileName, file_get_contents($_FILES[$file]['tmp_name']));
        $response['original'] = $originalFileName;
        return $response;
    }

    public function uploadFromBase64($string, $filename)
    {
        $image = new Imagick;
        $image->setBackgroundColor(new ImagickPixel('transparent'));
        $image->readImageBlob(base64_decode(str_replace('data:image/png;base64,', '', $string)));
        $image->setImageColorspace(Imagick::COLORSPACE_RGB);
        $image->setImageFormat('png');
        $this->create($filename, $image->getImageBlob());
        return true;
    }

    public function uploadFromFilePath($filePath, $fileName)
    {
        $this->create($fileName, file_get_contents($filePath));
    }

    public function nameFileForUpload($ext = 'png', $filename = false)
    {
        
        if (is_null($ext)) {
            $ext = 'png';
        }
        
        // Return the filename if it was explicitly specified (not guaranteed to be unique or random in this case)
        if ($filename) {
            return $filename.'.'.$ext;
        }

        // Generate a unique and random file name
        $uniqueFileName = uniqid('file_', true);
        $uniqueFileName = str_replace('.', '_', $uniqueFileName);
        return $uniqueFileName.'.'.$ext;
    }

    public function setDirectory($directory = null)
    {
        $this->directory = $directory;
        return $this;
    }

    public function getDirectory()
    {
        return $this->directory;
    }

    public function setStorage($storage = null)
    {
        $this->storage = $storage;
        return $this;
    }

    public function getStorage()
    {
        return $this->storage;
    }

    public function cd($directory = null)
    {
        return $this->setDirectory($directory);
    }

    public function ls($directory = null)
    {
        if (!is_null($directory)) $this->setDirectory($directory);
        return new Collection($this->resolveFilesystem());
    }

    public function get($filename)
    {
        return new File($this->resolveFilesystem(), $filename);
    }

    public function has($filename)
    {
        return $this->resolveFilesystem()->has($filename);
    }

    public function create($file, $contents)
    {
        $config = array(
            'headers' => array(
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET'
            ),
            'visibility' => 'public'
        );
        if ($this->has($file)) {
            return $this->resolveFilesystem()->update($file, $contents, $config);
        }
        return $this->resolveFilesystem()->write($file, $contents, $config);
    }

    private function resolveFilesystem()
    {
        return new Filesystem($this->buildAdapter());
    }

    private function buildAdapter()
    {
        $storageAdapter = $this->getStorage();
        $method = "MerchPlatform\\Files\\Storage\\".$storageAdapter;
        if (!class_exists($method)) throw new StorageAdapterNotFoundException("Storage adapter ({$storageAdapter}) cannot be found.");
        $adapter = new $method;
        $adapter->setOptions(array_merge(array('directory' => $this->getDirectory()), $this->configuration[$this->getStorage()]));
        return $adapter->storage();
    }
}
