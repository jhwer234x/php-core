<?php namespace MerchPlatform\Shipping;

use EasyPost\EasyPost as EasyPost_Base;
use EasyPost\Address, EasyPost\Parcel, EasyPost\Shipment, EasyPost\CustomsInfo;
use EasyPost\CustomsItem, EasyPost\Event;

use MerchPlatform\Shipping\Exceptions\APIKeyNotSetException;

class EasyPost implements ShippingInterface {

    private $apiKey = null;
    
    public function __construct($apiKey = null)
    {
        if (!is_null($apiKey)) $this->setApiKey($apiKey);
        if (is_null($this->getApiKey())) throw new APIKeyNotSetException;
        EasyPost_Base::setApiKey($this->getApiKey());
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function processWebhook()
    {
        $inputJSON = file_get_contents('php://input');
        return Event::receive($inputJSON);
    }

    public function createAddress(array $options = array())
    {
        return Address::create($options);
    }

    public function verifyAddress(array $options = array())
    {
        $address = $this->createAddress($options);
        return $address->verify();
    }

    public function createParcel(array $options = array())
    {
        return Parcel::create($options);
    }

    public function createShipment($to, $from, $parcel, array $options = array(), $withRates = true)
    {
        $shipment = Shipment::create(array(
            'to_address' => $to,
            'from_address' => $from,
            'parcel' => $parcel,
            'options' => $options
        ));

        if($withRates)
        {
            $shipment->get_rates();
        }

        return $shipment;
    }

    public function getShipment($shipmentId)
    {
        return Shipment::retrieve($shipmentId);
    }

    public function getRates($shipment)
    {
        if(isset($shipment->rates))
        {
            return $shipment->rates;
        }
        else
        {
            $shipment->get_rates();
        }
        
        return $shipment->rates;
    }

    public function getLabel($shipmentId, array $options = array())
    {
        $shipment = $this->getShipment($shipmentId);
        return $shipment->label($options);
    }

    public function createCustomsItem(array $options = array())
    {
        return CustomsItem::create($options);
    }

    public function createRefund($shipmentId)
    {
        $shipment = $this->getShipment($shipmentId);
        return $shipment->refund();
    }

    public function createScanForm($from, array $trackingCodes = array())
    {
        return ScanForm::create(array(
            'from_address' => $from,
            'tracking_codes' => implode(',', $trackingCodes)
        ));
    }
}