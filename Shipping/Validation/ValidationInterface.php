<?php namespace MerchPlatform\Shipping\Validation;

interface ValidationInterface {

    public function __construct($apiKey = null);
    public function setApiKey($apiKey);
    public function getApiKey();
    public function setAddress($address);
    public function getAddress();
    public function validate();
}