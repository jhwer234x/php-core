<?php namespace MerchPlatform\Shipping\Validation;

use GuzzleHttp\Client;

use MerchPlatform\Shipping\Exceptions\APIKeyNotSetException;
use MerchPlatform\Shipping\Exceptions\AddressNotSetException;
use MerchPlatform\Shipping\Exceptions\InvalidAddressException;

class GoogleGeocode implements ValidationInterface {

    const ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json';
    const NO_RESULTS = 'NO_RESULTS';
    const LOCATION_APPROXIMATE = 'APPROXIMATE';
    const LOCATION_GEOMETRIC_CENTER = 'GEOMETRIC_CENTER';
    const LOCATION_RANGE_INTERPOLATED = 'RANGE_INTERPOLATED';
    const LOCATION_ROOFTOP = 'ROOFTOP';

    private $apiKey;
    private $address;
    private $formattedAddress;
    private $addressComponents;
    private $location;
    private $client;

    public function __construct($apiKey = null, $address = null)
    {
        if (!is_null($apiKey)) $this->setApiKey($apiKey);
        if (!is_null($address)) $this->setAddress($address);
        $this->client = new Client;
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setFormattedAddress($formattedAddress)
    {
        $this->formattedAddress = $formattedAddress;
        return $this;
    }

    public function getFormattedAddress()
    {
        return $this->formattedAddress;
    }

    public function setAddressComponents($addressComponents)
    {
        $this->addressComponents = $addressComponents;
        return $this;
    }

    public function getAddressComponents()
    {
        return $this->addressComponents;
    }

    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function validate()
    {
        if (is_null($this->getApiKey())) throw new APIKeyNotSetException;
        if (is_null($this->getAddress())) throw new AddressNotSetException;
        $request = $this->client->get(self::ENDPOINT.'?'.http_build_query(array('key' => $this->getApiKey(), 'address' => $this->getAddress())));
        $response = $request->json();
        if ($response['status'] === self::NO_RESULTS) throw new InvalidAddressException;
        $locationType = $response['results'][0]['geometry']['location_type'];
        $this->setFormattedAddress($response['results'][0]['formatted_address']);
        $this->setAddressComponents($response['results'][0]['address_components']);
        $this->setLocation($response['results'][0]['geometry']['location']);
        if ($locationType === self::LOCATION_APPROXIMATE) throw new InvalidAddressException;
        elseif ($locationType === self::LOCATION_GEOMETRIC_CENTER) throw new InvalidAddressException;
        elseif ($locationType === self::LOCATION_RANGE_INTERPOLATED) throw new InvalidAddressException;
        elseif ($locationType === self::LOCATION_ROOFTOP) return true;
    }
}