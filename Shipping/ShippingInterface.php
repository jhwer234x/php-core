<?php namespace MerchPlatform\Shipping;

interface ShippingInterface {

    public function __construct($apiKey = null);
    public function setApiKey($apiKey);
    public function getApiKey();
    public function createAddress(array $options = array());
    public function verifyAddress(array $options = array());
    public function createParcel(array $options = array());
    public function createShipment($to, $from, $parcel, array $options = array(), $withRates = true);
    public function getShipment($shipmentId);
    public function getRates($shipment);
    public function getLabel($shipmentId, array $options = array());
    public function createCustomsItem(array $options = array());
    public function createRefund($shipmentId);
    public function createScanForm($from, array $trackingCodes = array());
}