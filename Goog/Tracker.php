<?php namespace MerchPlatform\Goog;

/**
* Track events in Google Analytics via HTTP API
*/
class Tracker
{
    private $profile_id;
    private $client_profile_id;
    private $host = 'www.example.com';
    private $base_url = 'http://www.google-analytics.com/collect?';

    function __construct($env = 'development')
    {
        $this->profile_id = \MerchPlatform\Config\Main::get('googleAnalytics')['profileId'];
        $this->client_profile_id = \MerchPlatform\Config\Main::get('googleAnalytics')['clientProfileId'];
        $this->host = $env . ".example.com";
    }

    // Handle GA response for HTTP request
    // @param HTTPStatus $resp
    // @return Bool
    private function handle_response($resp)
    {
        return true;
    }

    // Send a PV to Google Analytics
    // @param associative_array $pv = array(
    //  "ip" => <IP Address>,
    //  "path" => <URL path of pv>,
    //  "title" => <Title of page>,
    // );
    // @return handle_response
    public function send_pageview($pv)
    {
        // Available params: https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters
        $params = array(
            'v=1',
            ("tid=" . $this->profile_id),
            ("cid=" . $pv['ip']),
            ("uip=" . $pv['ip']),
            "t=pageview",
            "ds=server",        // data source to designate in GA
            ("p=" . rawurlencode($pv['path'])),
            ("dh=" . $this->host),
            ("dt=" . urlencode($pv['title'])),
            ("dl=" . rawurlencode($this->host . $pv['path']))
        );
        $resp = $this->get_json( $this->base_url . implode('&', $params) );
        return $this->handle_response($resp);
    }

    // Send an event to  Google Analytics
    // @param associative_array $event= array(
    //  "ip" => <IP Address>,
    //  "category" => <Event Category string>,
    //  "action" => <Event Action string>,
    //  "label" => <Event Label string>,
    //  "value" => <Event Value float>,
    // );
    // @return handle_response
    public function send_event($event)
    {
        // http://www.google-analytics.com/collect?v=1&tid=UA-43250043-1&cid=555&t=event&ec=Email&ea=Col&el=foo&ev=1
        $params = array(
            'v=1',
            ("tid=" . $this->profile_id),
            ("cid=" . $event['ip']),
            "t=event",
            "ds=server",        // data source to designate in GA
            ("ec=" . urlencode($event['category'])),
            ("ea=" . urlencode($event['action'])),
            ("el=" . urlencode($event['label'])),
            ("ev=" . urlencode($event['value']))
        );
        $resp = $this->get_json( $this->base_url . implode('&', $params) );
        return $this->handle_response($resp);
    }

    /**
     * send_ecommerce_campaign_order 
     * 
     * @param mixed $campaignOrder 
     * @access public
     * @return void
     */
    public function send_ecommerce_campaign_order($campaignOrder)
    {
        $params = array(
            'v=1',
            ("tid=" . $this->client_profile_id),
            ("cid=" . uniqid()),
            "ds=server",        // data source to designate in GA
            "t=transaction",
            ("ti=" . $campaignOrder->order_num),                // transaction ID
            ("ta=" . strval($campaignOrder->campaign->customer_id)),    // affiliation
            ("tr=" . strval(
                    $campaignOrder->price_items + 
                    $campaignOrder->price_shipping + 
                    $campaignOrder->price_tax
                )),                                             // total revenue
            ("ts=" . strval($campaignOrder->price_shipping)),   // shipping
            ("tt=" . strval($campaignOrder->price_tax))         // tax
        );
        $resp = $this->get_json( $this->base_url . implode('&', $params) );
        foreach($campaignOrder->campaignItems as $item){
            $this->send_ecommerce_campaign_item($item, $campaignOrder);
        }
    }

    /**
     * send_ecommerce_campaign_item 
     * 
     * @param mixed $item 
     * @param mixed $campaignOrder 
     * @access public
     * @return void
     */
    public function send_ecommerce_campaign_item( $item, $campaignOrder )
    {
        $params = array(
            'v=1',
            ("tid=" . $this->client_profile_id),
            ("cid=" . uniqid()),
            "ds=server",        // data source to designate in GA
            "t=item",
            ("ti=" . $campaignOrder->order_num),        // transaction ID
            ("in=" . urlencode(implode(' - ', array(
                    $item->variant->size->name_long,
                    $item->variant->color->name,
                    $item->variant->product->name
                )))),                                   // item name
            ("ip=" . strval($item->price_items)),       // total price
            ("iq=" . strval($item->qty)),               // quantity
            ("ic=" . $item->variant->gtin),             // item code/SKU/GTIN
            ("iv=" . urlencode('Campaign Garment')),    // item category
        );
        $resp = $this->get_json( $this->base_url . implode('&', $params) );
        return $this->handle_response($resp);
    }

    /**
     * get_json 
     * 
     * @param mixed $url 
     * @access private
     * @return void
     */
    private function get_json($url)
    {
        $resp = $this->special_get_url( $url );
        return json_decode($resp, true);
    }

    private function special_get_url( $url,  $javascript_loop = 0, $timeout = 5 )
    {
    //    $url = str_replace( "&amp;", "&", urldecode(trim($url)) );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $ch, CURLOPT_ENCODING, "" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
        curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
        curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
        $content = curl_exec( $ch );
        $response = curl_getinfo( $ch );
        curl_close ( $ch );

        if ($response['http_code'] == 301 || $response['http_code'] == 302)
        {
            ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

            if ( $headers = get_headers($response['url']) )
            {
                foreach( $headers as $value )
                {
                    if ( substr( strtolower($value), 0, 9 ) == "location:" )
                        return get_url( trim( substr( $value, 9, strlen($value) ) ) );
                }
            }
        }
        return $content;
    }
}
