<?php namespace MerchPlatform\Billing;

interface BillingInterface {

    public function __construct($apiKey = null);
    public function setApiKey($apiKey);
    public function getApiKey();
    public function account();
    public function balance();
    public function getTransaction($transaction);
    public function allTransactions(array $filters = array());
    public function createCustomer(array $options = array());
    public function getCustomer($customer);
    public function allCustomers(array $filters = array());
    public function createCharge(array $options = array());
    public function getCharge($charge);
    public function allCharges(array $filters = array());
    public function createRefund($charge, $amount, array $metadata = array());
    public function getRefund($charge, $refund);
    public function allRefunds($charge, array $filters = array());
    public function createRecipient(array $options = array());
    public function getRecipient($recipient);
    public function allRecipients(array $filters = array());
    public function createPayout($recipient, $amount, $description);
    public function getPayout($transfer);
    public function cancelPayout($transfer);
    public function allPayouts(array $filters = array());
}