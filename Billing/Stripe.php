<?php namespace MerchPlatform\Billing;

use Closure;

use Stripe as Stripe_Base, Stripe_Charge, Stripe_Recipient, Stripe_BalanceTransaction;
use Stripe_Customer, Stripe_Transfer, Stripe_Balance, Stripe_Account;
use Stripe_InvalidRequestError, Stripe_CardError;

use MerchPlatform\Billing\Exceptions\GeneralErrorException;
use MerchPlatform\Billing\Exceptions\RefundCompletedException;
use MerchPlatform\Billing\Exceptions\InvalidMoneyFormatException;
use MerchPlatform\Billing\Exceptions\CardDeclinedException;
use MerchPlatform\Billing\Exceptions\RefundToHighException;
use MerchPlatform\Billing\Exceptions\APIKeyNotSetException;
use MerchPlatform\Billing\Exceptions\IncorrectNumberException;
use MerchPlatform\Billing\Exceptions\InvalidNumberException;
use MerchPlatform\Billing\Exceptions\InvalidExpiryMonthException;
use MerchPlatform\Billing\Exceptions\InvalidExpiryYearException;
use MerchPlatform\Billing\Exceptions\InvalidCVCException;
use MerchPlatform\Billing\Exceptions\ExpiredCardException;
use MerchPlatform\Billing\Exceptions\IncorrectCVCException;
use MerchPlatform\Billing\Exceptions\IncorrectZipException;
use MerchPlatform\Billing\Exceptions\MissingCardNumberException;
use MerchPlatform\Billing\Exceptions\ProcessingErrorException;
use MerchPlatform\Billing\Exceptions\APIRateLimitException;

class Stripe implements BillingInterface {

    private $currency = 'usd';
    private $apiKey = null;
    private $apiVersion = null;

    public function __construct($apiKey = null, $apiVersion = null)
    {
        if (!is_null($apiKey)) $this->setApiKey($apiKey);
        if (!is_null($apiVersion)) $this->setApiVersion($apiVersion);
        if (is_null($this->getApiKey())) throw new APIKeyNotSetException;
        Stripe_Base::setApiKey($this->getApiKey());
        if (!is_null($this->getApiVersion())) {
            Stripe_Base::setApiVersion($this->getApiVersion());
        }
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiVersion($apiVersion = null)
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }

    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    public function account()
    {
        return $this->tryCatch(function() {
            return Stripe_Account::retrieve();
        });
    }

    public function balance()
    {
        return $this->tryCatch(function() {
            return Stripe_Balance::retrieve();
        });
    }

    public function getTransaction($transaction)
    {
        return $this->tryCatch(function() use($transaction) {
            return Stripe_BalanceTransaction::retrieve($transaction);
        });
    }

    public function allTransactions(array $filters = array())
    {
        return $this->tryCatch(function() use($filters) {
            return Stripe_BalanceTransaction::all($filters);
        });
    }

    public function createCustomer(array $options = array())
    {
        return $this->tryCatch(function() use($options)
        {
            return Stripe_Customer::create($options);
        });
    }

    public function getCustomer($customer)
    {
        return $this->tryCatch(function() use($customer) {
            return Stripe_Customer::retrieve($customer);
        });
    }

    public function allCustomers(array $filters = array())
    {
        return $this->tryCatch(function() use($filters) {
            return Stripe_Customer::all($filters);
        });
    }

    public function createCharge(array $options = array())
    {
        return $this->tryCatch(function() use($options)
        {
            return Stripe_Charge::create($options);
        });
    }

    public function getCharge($charge)
    {
        return $this->tryCatch(function() use($charge) {
            return Stripe_Charge::retrieve($charge);
        });
    }

    public function allCharges(array $filters = array())
    {
        return $this->tryCatch(function() use($filters) {
            return Stripe_Charge::all($filters);
        });
    }

    public function createRefund($charge, $amount, array $metadata = array())
    {
        return $this->tryCatch(function() use($charge, $amount, $metadata)
        {
            $charge = Stripe_Charge::retrieve($charge);
            return $charge->refund(array(
                'amount' => $amount,
                'metadata' => $metadata
            ));
        });
    }

    public function getRefund($charge, $refund)
    {
        return $this->tryCatch(function() use($charge, $refund) {
            $charge = Stripe_Charge::retrieve($charge);
            return $charge->refunds->retrieve($refund);
        });
    }

    public function allRefunds($charge, array $filters = array())
    {
        return $this->tryCatch(function() use($charge, $filters) {
            $charge = Stripe_Charge::retrieve($charge);
            return $charge->refunds->all($filters);
        });
    }

    public function createRecipient(array $options = array())
    {
        return $this->tryCatch(function() use($options)
        {
            return Stripe_Recipient::create($options);
        });
    }

    public function getRecipient($recipient)
    {
        return $this->tryCatch(function() use($recipient) {
            return Stripe_Recipient::retrieve($recipient);
        });
    }

    public function allRecipients(array $filters = array())
    {
        return $this->tryCatch(function() use($filters) {
            return Stripe_Recipient::all($filters);
        });
    }

    public function createPayout($recipient, $amount, $description)
    {
        return $this->tryCatch(function() use($recipient, $amount, $description)
        {
            return Stripe_Transfer::create(array(
                'amount' => $amount,
                'currency' => $this->currency,
                'recipient' => $recipient,
                'description' => $description
            ));
        });
    }

    public function getPayout($transfer)
    {
        return $this->tryCatch(function() use($transfer) {
            return Stripe_Transfer::retrieve($transfer);
        });
    }

    public function cancelPayout($transfer)
    {
        return $this->tryCatch(function() use($transfer) {
            $transfer = Stripe_Transfer::retrieve($transfer);
            return $transfer->cancel();
        });
    }

    public function allPayouts(array $filters = array())
    {
        return $this->tryCatch(function() use($filters) {
            return Stripe_Transfer::all($filters);
        });
    }

    private function tryCatch(Closure $callback)
    {
        try {
            return $callback();
        }
        catch(Stripe_CardError $exception)
        {
            $body = $exception->getJsonBody();
            $this->throwException($body['error']['code'], $body['error']['message']);
        }
        catch(Stripe_InvalidRequestError $exception)
        {
            $message = $exception->getMessage();
            // dd($message);
            if ($this->exceptionMessageHas('is greater than unrefunded amount on charge', $message))
            {
                $this->throwException('refund_to_high', $exception->getMessage());
            }
            elseif ($this->exceptionMessageHas('has already been refunded', $message))
            {
                $this->throwException('refund_completed', $exception->getMessage());
            }
            elseif ($this->exceptionMessageHas('Invalid integer', $message))
            {
                $this->throwException('invalid_integer', $exception->getMessage());
            } else {
                $this->throwException(null, $exception->getMessage());
            }
        }
    }

    private function exceptionMessageHas($needle, $haystack)
    {
        return strpos($haystack, $needle) !== false;
    }

    private function throwException($code, $message = null)
    {
        switch($code)
        {
            case 'refund_completed': throw new RefundCompletedException($message); break;
            case 'invalid_integer': throw new InvalidMoneyFormatException($message); break;
            case 'refund_to_high': throw new RefundToHighException($message); break;
            case 'card_declined': throw new CardDeclinedException($message); break;
            case 'incorrect_number': throw new IncorrectNumberException($message); break;
            case 'invalid_number': throw new InvalidNumberException($message); break;
            case 'invalid_expiry_month': throw new InvalidExpiryMonthException($message); break;
            case 'invalid_expiry_year': throw new InvalidExpiryYearException($message); break;
            case 'invalid_cvc': throw new InvalidCVCException($message); break;
            case 'expired_card': throw new ExpiredCardException($message); break;
            case 'incorrect_cvc': throw new IncorrectCVCException($message); break;
            case 'incorrect_zip': throw new IncorrectZipException($message); break;
            case 'missing': throw new MissingCardNumberException($message); break;
            case 'processing_error': throw new ProcessingErrorException($message); break;
            case 'rate_limit': throw new APIRateLimitException($message); break;
            default: throw new GeneralErrorException($message); break;
        }
    }
}
